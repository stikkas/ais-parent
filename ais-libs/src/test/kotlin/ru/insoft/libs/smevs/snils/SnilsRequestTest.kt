package ru.insoft.libs.smevs.snils

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.io.StringWriter
import java.time.LocalDate
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller

class SnilsRequestTest {
    @Disabled
    @Test
    fun testCreateRequest() {
        val data = SnilsRequest().apply {
            familyName = "Кузнецов"
            firstName = "Андрей"
            patronymic = "Иванович"
            birthDate = LocalDate.now().minusYears(18)
            sex = 0
            birthPlace = BirthPlaceType().apply {
                placeType = "село"
                settlement = "XP"
                district = "Курчевое"
                region = "Вакхинокио"
                country = "Россия"
            }
            identity = PassportRF().apply {
                series = "2800"
                number = "247238"
                issueDate = LocalDate.now().minusYears(4)
                issuer = "Иван Иванович Иванов"
            }
        }

        val writer = StringWriter()
        JAXBContext.newInstance(data.javaClass).createMarshaller().apply {
            setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true)
        }.marshal(data, writer)
        println(writer.toString())
    }

    @Disabled
    @Test
    fun testManyFlats() {
        val flats = { f: String ->
            f.split(",").flatMap {
                if (it.contains("-")) {
                    val (min, max) = it.split("-").map { it.trim().toInt() }
                    (min..max).map { it.toString() }
                } else {
                    listOf(it.trim())
                }
            }.filterNot { it.isNullOrBlank() }
        }
        println(flats("15"))
        println(flats("15, 18, 32"))
        println(flats("15-18, 32-33, 17, 19"))
        println(flats("15, 14, 20-22,"))
    }
}
