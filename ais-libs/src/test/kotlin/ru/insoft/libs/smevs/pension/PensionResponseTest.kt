package ru.insoft.libs.smevs.pension

import com.sun.xml.internal.messaging.saaj.util.JAXMStreamSource
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.io.StringReader
import javax.xml.bind.JAXBContext

class PensionResponseTest {
    @Disabled
    @Test
    fun testCreateRequest() {
        val data = """<?xml version="1.0" encoding="UTF-8"?>
<tns:ReceiptPensionFactResponse xmlns:tns="http://kvs.pfr.com/receipt-pension-fact/1.0.1" xmlns:smev="urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1">
        <smev:FamilyName>Тестовый</smev:FamilyName>
        <smev:FirstName>Тест</smev:FirstName>
        <smev:Patronymic>Тестович</smev:Patronymic>
        <tns:Snils>00000055500</tns:Snils>
        <tns:BirthDate>1967-08-13</tns:BirthDate>
        <tns:PensionSettingFact>Застрахованное лицо является получателем пенсии</tns:PensionSettingFact>
        <tns:PensionsCount>2</tns:PensionsCount>
        <tns:PensionsData>
                <tns:PensionData>
                        <tns:PensionCause>Основание получения пенсии 1</tns:PensionCause>
                        <tns:StartDate>2013-08-01</tns:StartDate>
                        <tns:EndDate>2999-12-31</tns:EndDate>
                </tns:PensionData>
                <tns:PensionData>
                        <tns:PensionCause>Основание получения пенсии 2</tns:PensionCause>
                        <tns:StartDate>2013-10-01</tns:StartDate>
                        <tns:EndDate>2999-12-31</tns:EndDate>
                </tns:PensionData>
        </tns:PensionsData>
</tns:ReceiptPensionFactResponse>
"""

        val writer = StringReader(data)
        val res = JAXBContext.newInstance(PensResponse::class.java).createUnmarshaller()
            .unmarshal(JAXMStreamSource(writer), PensResponse::class.java)?.value
        println(res?.toString())
    }
}
