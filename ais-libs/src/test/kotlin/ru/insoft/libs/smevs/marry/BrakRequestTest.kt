package ru.insoft.libs.smevs.marry

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.io.StringWriter
import java.time.LocalDate
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller

class BrakRequestTest {
    @Disabled
    @Test
    fun testCreateRequest() {
        val data = BrakRequest().apply {
            id = "145"
            details = ReqDetails().apply {
                agsInfo = AgsDetails().apply {
                    date = LocalDate.now()
                    organ = AgsType().apply {
                        id = "203923902jaf;jdsfoieajfaefeaf"
                        name = "Загс №1"
                        code = "X41-42"
                    }
                    nomer = "234a"
                }
                fizInfo = FizDetails().apply {
                    fio = FioType().apply {
                        lastName = "Иванов"
                        firstName = "Иван"
                        middleName = "Иванович"
                    }
                    birthDate = LocalDate.now().minusYears(45)
                    ausweis = Personalausweis().apply {
                        taxCode = "0023"
                        series = "24080"
                        number = "8420 2-42"
                        date = LocalDate.now().minusYears(25)
                        whogive = "Unknown"
                        whoCode = "23-41"
                    }
                    snils = "234-341-234-00"
                }
            }
        }

        val writer = StringWriter()
        JAXBContext.newInstance(data.javaClass).createMarshaller().apply {
            setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true)
        }.marshal(data, writer)
        println(writer.toString())
    }
}
