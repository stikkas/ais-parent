package ru.insoft.libs.controllers

import mu.KotlinLogging
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.validation.BindingResult
import org.springframework.validation.DataBinder
import org.springframework.validation.Validator
import ru.insoft.libs.models.Response

private val logger = KotlinLogging.logger {}

abstract class AbstractController {
    protected fun handle(block: () -> Any?) = try {
        val res = block()
        if (res is Response) {
            res
        } else {
            Response.ok(res)
        }
    } catch (ex: Exception) {
        logger.error(ex) {}
        val message = (ex as? DataIntegrityViolationException)?.let {
            if (ex.cause?.message?.contains(Regex("UPDATE.*?DELETE")) == true) {
                "Невозможно удалить запись. На нее ссылаются из других мест."
            } else {
                null
            }
        } ?: ex.cause?.let { it.message } ?: ex.message ?: "Произошла ошибка"
        Response.fail(message)
    }

    protected fun check(target: Any, validator: Validator, name: String = "info", block: () -> Any?): Response {
        val errors = DataBinder(target, name).bindingResult
        validator.validate(target, errors)
        return withValidation(errors, block)
    }

    protected fun withValidation(errors: BindingResult, block: () -> Any?) = if (errors.hasErrors()) {
        Response.fail(errors.allErrors)
    } else {
        handle(block)
    }
}

