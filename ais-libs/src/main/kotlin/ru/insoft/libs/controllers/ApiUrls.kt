package ru.insoft.libs.controllers

import com.fasterxml.jackson.annotation.JsonProperty

open class ApiUrls {
    @JsonProperty("files")
    fun getFiles() = files

    @JsonProperty("rptrs")
    fun getReports() = reports

    companion object {
        const val root = "/api"
        const val files = "$root/files"
        const val reports = "$root/reports"
    }
}
