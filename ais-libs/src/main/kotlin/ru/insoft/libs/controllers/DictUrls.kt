package ru.insoft.libs.controllers

import com.fasterxml.jackson.annotation.JsonProperty

open class DictUrls {

    @JsonProperty("base")
    fun getBase() = base

    @JsonProperty("ocat")
    fun getOrgCategories() = orgCategories

    @JsonProperty("fios")
    fun getFios() = fios

    @JsonProperty("aorgs")
    fun getAllOrgs() = allOrgs

    @JsonProperty("otps")
    fun getOrgTypes() = orgTypes

    @JsonProperty("cntrs")
    fun getCountries() = countries

    @JsonProperty("lvls")
    fun getLevels() = levels

    @JsonProperty("lgs")
    fun getLogin() = login

    @JsonProperty("lns")
    fun getLastName() = lastName

    @JsonProperty("ais")
    fun getAis() = ais

    @JsonProperty("pos")
    fun getPos() = pos

    @JsonProperty("rls")
    fun getRoles() = roles

    @JsonProperty("smns")
    fun getSmevNames() = smevNames

    @JsonProperty("stets")
    fun getTargetSmevETypes() = targetSmevETypes

    @JsonProperty("ffm")
    fun getFullFamilyMembers() = fullFamilyMembers

    @JsonProperty("fm")
    fun getFamilyMembers() = familyMembers

    @JsonProperty("rt")
    fun getRegTypes() = regTypes

    @JsonProperty("tdt")
    fun getTypesDocument() = typesDocument

    @JsonProperty("zcert")
    fun getZagsCerts() = zagsCerts

    @JsonProperty("okids")
    fun getOpeakFamilyKids() = opekaFamilyKids

    @JsonProperty("hg")
    fun getHealthGroup() = healthGroup

    @JsonProperty("ig")
    fun getInvalidGroup() = invalidGroup

    @JsonProperty("hl")
    fun getHealth() = health

    @JsonProperty("ctzn")
    fun getCitizen() = citizen

    @JsonProperty("whogiv")
    fun getWhogive() = whogive

    @JsonProperty("jobp")
    fun getJobPlace() = jobPlace

    @JsonProperty("wgo")
    fun getWgo() = wgo

    @JsonProperty("zags")
    fun getZags() = zags

    @JsonProperty("morgs")
    fun getMyOrgs() = myOrgs

    @JsonProperty("ntns")
    fun getNationalities() = nationalities

    @JsonProperty("dt")
    fun getDocTypes() = docTypes

    @JsonProperty("lta")
    fun getLiveTypesArrange() = ltArrange

    @JsonProperty("dda")
    fun getDecDocAppeal() = decDocAppeal

    @JsonProperty("adrts")
    fun getAddressTypes() = addressTypes

    @JsonProperty("ddia")
    fun getDecDocIncAppeal() = decDocIncAppeal

    @JsonProperty("ddpa")
    fun getDecDocPatronAppeal() = decDocPatronAppeal

    @JsonProperty("users")
    fun getUsers() = users

    @JsonProperty("sorg")
    fun getSameOrgs() = sameOrgs

    @JsonProperty("atpt")
    fun getApplicantType() = applicantType

    @JsonProperty("alpt")
    fun getApplicantAllTypes() = applicantAllTypes

    @JsonProperty("oop")
    fun getRegOOP() = regOOP

    companion object {
        const val base = "${ApiUrls.root}/dicts"
        const val ltArrange = "/live-types-arrange"
        const val decDocAppeal = "/decision-document"
        const val addressTypes = "/address-types"
        const val decDocIncAppeal = "/decision-document-inc"
        const val decDocPatronAppeal = "/decision-document-patron"
        const val users = "/users"
        const val sameOrgs = "/same-organisations"
        const val applicantType = "/applicant-type"
        const val applicantAllTypes = "/applicant-all-types"
        const val levels = "/levels"
        const val countries = "/countries"
        const val orgTypes = "/org-types"
        const val jobPlace = "/job-places"
        const val allOrgs = "/all-orgs"
        const val fios = "/fios"
        const val orgCategories = "/org-categories"
        const val citizen = "/citizenships"
        const val whogive = "/whogives"
        const val wgo = "/whogives-other"
        const val zags = "/zags"
        const val myOrgs = "/my-orgs"
        const val nationalities = "/nationalities"
        const val docTypes = "/doc-types"
        const val opekaFamilyKids = "/opeka-family-kids"
        const val healthGroup = "/health-group"
        const val invalidGroup = "/invalid-group"
        const val health = "/health"
        const val targetSmevETypes = "/target-smev-etypes"
        const val fullFamilyMembers = "/full-family-members"
        const val familyMembers = "/family-members"
        const val regOOP = "/register-oop"
        const val regTypes = "/reg-types"
        const val typesDocument = "/types-document"
        const val zagsCerts = "/zags-certs"
        const val login = "/logins"
        const val lastName = "/last-names"
        const val ais = "/ais"
        const val pos = "/pos"
        const val roles = "/roles"
        const val smevNames = "/smev-names"
    }
}
