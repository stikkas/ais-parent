package ru.insoft.libs.controllers

import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import ru.insoft.libs.AisId
import ru.insoft.libs.models.Classifiers
import ru.insoft.libs.models.Dict
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.models.User
import ru.insoft.libs.services.SharedDictService

abstract class DictController(protected val sds: SharedDictService) {
    @GetMapping(DictUrls.orgCategories)
    fun orgCategories() = sds.orgCategories()

    @GetMapping(DictUrls.countries)
    fun countries(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ) = sds.countries(query, limit)

    @GetMapping(DictUrls.levels)
    fun addressLevels(
        @RequestParam(required = false) query: String?,
        @RequestParam(required = false) paid: String?,
        @RequestParam(required = false) level: String?,
        @RequestParam(defaultValue = "false") all: Boolean,
        @RequestParam(defaultValue = "10") limit: Int
    ) = if (all)
        sds.childAddresses(query, paid)
    else
        sds.addressLevels(query, paid, level, limit)

    @GetMapping(DictUrls.login)
    fun logins(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ) = sds.logins(query, limit)

    @GetMapping(DictUrls.lastName)
    fun lastNames(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ) = sds.lastNames(query, limit)

    @GetMapping(DictUrls.smevNames)
    fun smevNames(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ) = sds.smevNames(query, limit)

    @GetMapping(DictUrls.ais)
    fun ais() = sds.ais()

    @GetMapping(DictUrls.pos)
    fun pos(@RequestParam login: String, @RequestParam aisId: Int) = sds.positions(login, aisId)

    @GetMapping(DictUrls.roles)
    fun roles(query: Int) = sds.roles(query)

    @GetMapping(DictUrls.fios)
    fun executiveNames(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int,
        auth: Authentication
    ) = withUser(auth) { sds.execNames(query, limit, it.orgId) }

    @GetMapping(DictUrls.allOrgs)
    fun allOrgs(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ) = sds.allOrgs(query, limit)

    @GetMapping(DictUrls.orgTypes)
    fun orgTypes(@RequestParam(required = false) query: Int?) = sds.orgTypes(query)

    @GetMapping(DictUrls.targetSmevETypes)
    fun electricSmevOfAppeal(@RequestParam(required = false) query: Long?) =
        sds.smevOfAppeal(query, DictValue.ELEC_INTER)

    @GetMapping(DictUrls.fullFamilyMembers)
    fun fullFamilyMembers(auth: Authentication) = withUser(auth) { user ->
        val full = dict(DictValue.FAMILY_MEMBERS)
        if (user.aisId == AisId.RUG.id) {
            full.filter {
                it.value == DictValue.MOTHER.id ||
                        it.value == DictValue.FATHER.id ||
                        it.value == DictValue.FOSTER_CAREE.id ||
                        it.value == DictValue.GUARDIAN.id ||
                        it.value == DictValue.POPECHITEL.id ||
                        it.value == DictValue.FOSTER_PARENT.id
            }
        } else {
            full.filter { it.value != null && it.value != DictValue.OWNER.id && it.value != DictValue.EMPLOYER.id }
        }
    }

    @GetMapping(DictUrls.familyMembers)
    fun familyMembers(auth: Authentication) = withUser(auth) {
        sds.familyMembers(DictValue.FAMILY_MEMBERS, it.aisId).filter { it.value != null }
    }

    @GetMapping(DictUrls.regOOP)
    fun regOOP(@RequestParam(required = false) query: String?) = sds.registerOOP(query)

    @GetMapping(DictUrls.ltArrange)
    fun liveTypeArranges() = sds.classifier(DictValue.LIVE_TYPE_ARRANGE)

    @GetMapping(DictUrls.regTypes)
    fun regTypes() = sds.classifier(DictValue.REGISTRATION_TYPE).filter { it.value != null }

    @GetMapping(DictUrls.zagsCerts)
    fun zagsCerts() = sds.dictUs(DictValue.MARRY_CERT, DictValue.DEATH_CERT, DictValue.BREAK_MARRY_CERT)

    @GetMapping(DictUrls.opekaFamilyKids)
    fun opekaFamilyKids(@RequestParam query: String) = sds.opekaFamilyKids(query)

    /**
     * Документы для принятия решения для опеки несовершеннолетних
     */
    @GetMapping(DictUrls.decDocAppeal)
    fun decDocAppeal() = sds.dictUs(DictValue.DOC_OOP_ALLOW, DictValue.DOC_OOP_PROHIBIT)

    /**
     * Адрес регистрации и фактический адрес
     */
    @GetMapping(DictUrls.addressTypes)
    fun addressTypes() = sds.dictUs(DictValue.REG_ADDR, DictValue.FACT_ADDR)

    /**
     * Документы для принятия решения для опеки недееспособных
     */
    @GetMapping(DictUrls.decDocIncAppeal)
    fun decDocIncAppeal() = sds.dictUs(DictValue.DOC_OOPA_ALLOW, DictValue.DOC_OOPA_PROHIBIT)

    /**
     * Документы для принятия решения для нуждающихся в патронаже
     */
    @GetMapping(DictUrls.decDocPatronAppeal)
    fun decDocPatronAppeal() = sds.dictUs(Classifiers.docAllowAssistant, Classifiers.docNoallowAssistant)

    @GetMapping(DictUrls.applicantType)
    fun applicantTypes(@RequestParam(required = false) query: Int?) =
        query?.let { sds.applicantTypes(query) } ?: emptyList()

    @GetMapping(DictUrls.applicantAllTypes)
    fun applicantListTypes() = dict(DictValue.APPLICANT_TYPE)

    @GetMapping(DictUrls.users)
    fun users(@RequestParam(required = false) query: String?, auth: Authentication) = withUser(auth) {
        sds.users(query, it.orgId, it.aisId)
    }

    /**
     * Организации с категорией и типом учреждения как у организации текущего пользователя
     */
    @GetMapping(DictUrls.sameOrgs)
    fun sameOrganisations(@RequestParam(required = false) query: String?, auth: Authentication) =
        withUser(auth) { sds.sameOrganisations(it.orgId, query) }

    @GetMapping(DictUrls.citizen)
    fun citizenShips(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ) = sds.citizenShips(query, limit)

    @GetMapping(DictUrls.whogive)
    fun whogives(
        @RequestParam(required = false) query: String?,
        @RequestParam(required = false) typeId: String?,
        @RequestParam(defaultValue = "10") limit: Int
        // 5 - Свидетельство о рождении
    ) = forTypeAhead(query) { if (typeId == "5") sds.zags(query) else sds.whogives(query, limit) }

    @GetMapping(DictUrls.jobPlace)
    fun jobPlaces(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ) = forTypeAhead(query) { sds.jobPlaces(query, limit) }

    @GetMapping(DictUrls.wgo)
    fun whogivesOther(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ) = forTypeAhead(query) { sds.whogivesOther(query, limit) }

    @GetMapping(DictUrls.zags)
    fun zagsOrgs() = sds.whogivesZags()

    /**
     * Возвращает список организаций текущего пользователя (саму организацию + дочерние)
     */
    @GetMapping(DictUrls.myOrgs)
    fun myOrgs(auth: Authentication) = withUser(auth) { sds.myOrgs(it.orgId) }

    @GetMapping(DictUrls.nationalities)
    fun nationalities(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "10") limit: Int
    ) = sds.nationalities(query, limit)

    @GetMapping(DictUrls.docTypes)
    fun docTypes() = sds.docTypes()

    @GetMapping(DictUrls.healthGroup)
    fun healthGroup() = dict(DictValue.HEALTH_GROUP)

    @GetMapping(DictUrls.invalidGroup)
    fun invalidGroup() = dict(DictValue.INVALID_GROUP)

    @GetMapping(DictUrls.health)
    fun health() = dict(DictValue.HEALTH)

    @GetMapping(DictUrls.typesDocument)
    fun typesDocument() = dict(DictValue.DOC_TYPES)

    @GetMapping("/{name}")
    fun dictUrl(@PathVariable name: String) = DictValue.toDictValue(name)?.let {
        dict(it)
    } ?: sds.dict(name)

    protected fun dict(dict: DictValue) = when (dict) {
        DictValue.BREAK_CUMS -> sds.allTree(dict)
        else -> sds.dict(dict)
    }

    protected fun forTypeAhead(default: String?, block: () -> List<Dict<String>?>) = block().filterNotNull().let {
        if (it.isEmpty() && !default.isNullOrBlank()) {
            listOf(Dict<String>().apply {
                value = default
                label = default
            });
        } else {
            it
        }
    }

    private fun <T> withUser(auth: Authentication, block: (user: User) -> T): T? =
        ((auth.details as? User) ?: (auth.principal as? User))?.let {
            block(it)
        }
}


