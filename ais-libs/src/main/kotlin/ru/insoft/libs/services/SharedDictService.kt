package ru.insoft.libs.services

import ru.insoft.libs.models.*


interface SharedDictService {
    fun childAddresses(query: String?, paid: String?): List<List<AddressDict>>
    fun addressLevels(query: String?, paid: String?, levelId: String?, limit: Int): List<AddressDict>
    fun docTypes(): List<DictDoc>
    fun classifier(cls: DictValue): List<Dict<DictValue>>
    fun familyMembers(cls: DictValue, aisId: Int?): List<Dict<DictValue>>
    fun classifierUs(vararg cls: DictValue): List<Dict<DictValue>>

    /**
     * Возвращает значения и названия, справочников, чьими родителями являются перечисленные справочники
     */
    fun dict(vararg cls: DictValue): List<Dict<String>>
    fun dict(vararg cls: String): List<Dict<String>>

    /**
     * Возвращает значения и названия запрашиваемых справочников
     */
    fun dictUs(vararg cls: DictValue): List<Dict<String>>
    fun dictUs(vararg cls: String): List<Dict<String>>
    fun allTree(parent: DictValue): List<Dict<String>>

    fun citizenShips(query: String?, limit: Int): List<Dict<String>>
    fun nationalities(query: String?, limit: Int): List<DictShort<String>>
    fun countries(query: String?, limit: Int): List<Dict<String>>

    fun whogives(query: String?, limit: Int): List<Dict<String>>
    fun jobPlaces(query: String?, limit: Int): List<Dict<String>>
    fun zags(query: String?): List<Dict<String>>
    fun whogivesOther(query: String?, limit: Int): List<Dict<String>?>
    fun whogivesZags(): List<Dict<String>?>
    fun myOrgs(orgId: String): List<DictShort<String>>
    fun users(userId: String?, orgId: String, aisId: Int?): List<DictUser>
    fun smevNames(query: String?, limit: Int): List<Dict<String>>

    fun opekaFamilyKids(familyId: String): List<DictShort<String>>
    fun orgCategories(): List<Dict<Int>>
    fun lastNames(query: String?, limit: Int): List<Dict<String>>
    fun logins(query: String?, limit: Int): List<Dict<String>>
    fun execNames(query: String?, limit: Int, orgId: String): List<Dict<String>>
    fun allOrgs(query: String?, limit: Int): List<Dict<String>>
    fun ais(): List<Dict<Int>>
    fun roles(aisId: Int): List<Dict<String>>
    fun positions(login: String, aisId: Int): List<Dict<String>>
    fun orgTypes(catId: Int?): List<Dict<String>>
    fun sameOrganisations(orgId: String, newOrgId: String?): List<Dict<String>>
    fun smevOfAppeal(appealId: Long?, interaction: DictValue = DictValue.PAPER_INTER): List<DictSmev>
    fun applicantTypes(targetId: Int): List<Dict<String>>?
    fun registerOOP(orgId: String?): List<DictShort<String>>
}
