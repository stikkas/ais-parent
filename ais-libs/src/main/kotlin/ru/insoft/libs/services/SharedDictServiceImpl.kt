package ru.insoft.libs.services

import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.libs.*
import ru.insoft.libs.models.AddressDict
import ru.insoft.libs.models.CacheNames.Companion.AddressLevels
import ru.insoft.libs.models.CacheNames.Companion.Ais
import ru.insoft.libs.models.CacheNames.Companion.AllAddressLevels
import ru.insoft.libs.models.CacheNames.Companion.AllTree
import ru.insoft.libs.models.CacheNames.Companion.AppealRelations
import ru.insoft.libs.models.CacheNames.Companion.Citizenships
import ru.insoft.libs.models.CacheNames.Companion.Classifiers
import ru.insoft.libs.models.CacheNames.Companion.ClassifiersUs
import ru.insoft.libs.models.CacheNames.Companion.Countries
import ru.insoft.libs.models.CacheNames.Companion.Dicts
import ru.insoft.libs.models.CacheNames.Companion.DictsAndFlag
import ru.insoft.libs.models.CacheNames.Companion.DictsStr
import ru.insoft.libs.models.CacheNames.Companion.DictsUs
import ru.insoft.libs.models.CacheNames.Companion.DictsUsStr
import ru.insoft.libs.models.CacheNames.Companion.DocTypes
import ru.insoft.libs.models.CacheNames.Companion.FamilyMembers
import ru.insoft.libs.models.CacheNames.Companion.Nationalities
import ru.insoft.libs.models.CacheNames.Companion.OrgCategories
import ru.insoft.libs.models.CacheNames.Companion.Roles
import ru.insoft.libs.models.CacheNames.Companion.TwoLevels
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.repo.SharedDictMapper


@Service
@Transactional(readOnly = true)
class SharedDictServiceImpl(private val dm: SharedDictMapper) : SharedDictService {
    private val countryLevelId = "002"

    @Scheduled(fixedRate = 86400000, initialDelay = 86400000) // Раз в сутки
    @CacheEvict(
        cacheNames = [Roles, Ais, OrgCategories, Nationalities, Countries, AllAddressLevels, AddressLevels, DocTypes,
            Classifiers, FamilyMembers, ClassifiersUs, Dicts, DictsStr, Citizenships, DictsUs, DictsUsStr, AllTree, TwoLevels,
            DictsAndFlag, AppealRelations],
        allEntries = true
    )
    fun evictAllcachesAtIntervals() {
    }

    @Cacheable(AllAddressLevels)
    override fun childAddresses(query: String?, paid: String?) =
        dm.addressIds(paid, padAddressId(paid), query, if (paid.isNullOrEmpty()) 300 else null).map { id ->
            dm.levels(id, paid).also { setChilds(it) }
        }

    @Cacheable(AddressLevels)
    override fun addressLevels(query: String?, paid: String?, levelId: String?, limit: Int): List<AddressDict> {
        val emptyPaid = paid.isNullOrEmpty()
        val patternId = if (!emptyPaid) {
            val m = zeroEx.matcher(paid)
            var lastNumber = 0
            while (m.find()) {
                lastNumber = m.start()
            }
            val d = lastNumber / fieldWidth
            val indexStart = (d + 1) * fieldWidth
            var patternId = paid!!.substring(0, indexStart) + "_".repeat(fieldWidth)
            patternId + "0".repeat(addressIdLength - patternId.length)
        } else null

        return if (levelId == "007") dm.levels(null, parentId = paid, pattern = patternId, limit = 1000)
            .let { l ->
                setChilds(l)
                // Родительский уровень - улица
                // Поиск идет по дому, предполагаем что больше 1000 домов на одной улице нет
                val res = l.filter { a ->
                    query == null || a.label?.contains(query, true) == true
                }.sortedBy {
                    // Сортируем по номеру дома, затем номеру корпуса или строения
                    // Может быть до третьего уровня вложенности
                    it.label?.padStart(6, '0') + (it.child?.let { c -> c.levelId + c.label } ?: "") +
                            (it.child?.child?.let { c -> c.levelId + c.label } ?: "")
                }
                if (res.isEmpty()) {
                    res
                } else {
                    // Обрезаем до limit или немного дальше, если на границе оказался дом с несколькими копрусами (строениями)
                    // нужно оставить все его корпуса (строения)
                    var lastName = res[0].label
                    val result = mutableListOf<AddressDict>()
                    for (it in res) {
                        if (result.size < limit || it.label == lastName) {
                            result.add(it)
                            lastName = it.label
                        } else {
                            break
                        }
                    }
                    result
                }
            }
        else
            dm.levels(
                null, null, paid, patternId, limit,
                if (emptyPaid) countryLevelId else null, query, true
            )
    }

    @Cacheable(DocTypes)
    override fun docTypes() = dm.docTypes()

    @Cacheable(Classifiers)
    override fun classifier(cls: DictValue) = dm.classifiers(cls)

    @Cacheable(FamilyMembers)
    override fun familyMembers(cls: DictValue, aisId: Int?) = dm.familyMembers(cls, aisId)

    @Cacheable(ClassifiersUs)
    override fun classifierUs(vararg cls: DictValue) = cls.toList().let {
        if (it.isNotEmpty()) {
            dm.classifiersUs(it)
        } else {
            emptyList()
        }
    }

    @Cacheable(Dicts)
    override fun dict(vararg cls: DictValue) = cls.toList().let {
        if (it.isNotEmpty()) {
            dm.dict(it)
        } else {
            emptyList()
        }
    }

    @Cacheable(DictsStr)
    override fun dict(vararg cls: String) = cls.toList().let {
        if (it.isNotEmpty()) {
            dm.dictStr(it)
        } else {
            emptyList()
        }
    }

    @Cacheable(DictsUs)
    override fun dictUs(vararg cls: DictValue) = cls.toList().let {
        if (it.isNotEmpty()) {
            dm.dictUs(it)
        } else {
            emptyList()
        }
    }

    @Cacheable(DictsUsStr)
    override fun dictUs(vararg cls: String) = cls.toList().let {
        if (it.isNotEmpty()) {
            dm.dictUsStr(it)
        } else {
            emptyList()
        }
    }

    @Cacheable(AllTree)
    override fun allTree(parent: DictValue) = dm.allTree(parent)

    @Cacheable(Citizenships)
    override fun citizenShips(query: String?, limit: Int) = dm.citizenShips(query, limit)
    override fun whogives(query: String?, limit: Int) = dm.whogives(query, limit)
    override fun jobPlaces(query: String?, limit: Int) = dm.jobPlaces(query, limit)
    override fun zags(query: String?) = dm.zags(query)
    override fun whogivesOther(query: String?, limit: Int) = dm.whogivesOther(query, limit)
    override fun whogivesZags() = dm.whogivesZags()

    /**
     * Организация и дочернии текущего пользователя, одним списком для выбора в селекторе
     */
    override fun myOrgs(orgId: String) = dm.myOrgs(orgId)

    /**
     * сотрудники организации текущего пользователя
     */
    override fun users(userId: String?, orgId: String, aisId: Int?) = dm.users(userId, orgId, aisId ?: -1)
    override fun sameOrganisations(orgId: String, newOrgId: String?) = dm.sameOrgs(orgId, newOrgId)

    override fun orgTypes(catId: Int?) = dm.orgTypes(catId)

    override fun opekaFamilyKids(familyId: String) = dm.opekaFamilyKids(familyId)

    @Cacheable(Nationalities)
    override fun nationalities(query: String?, limit: Int) = dm.nationalities(query, limit)

    @Cacheable(Countries)
    override fun countries(query: String?, limit: Int) = dm.countries(query, limit)

    @Cacheable(OrgCategories)
    override fun orgCategories() = dm.orgCategories()

    @Cacheable(Ais)
    override fun ais() = dm.ais()

    @Cacheable(Roles)
    override fun roles(aisId: Int) = dm.roles(aisId)

    override fun registerOOP(orgId: String?) = dm.registerOOP(orgId)
    override fun positions(login: String, aisId: Int) = dm.positions(login.toLowerCase(), aisId)
    override fun lastNames(query: String?, limit: Int) = dm.lastNames(query, limit)
    override fun logins(query: String?, limit: Int) = dm.logins(query, limit)
    override fun smevNames(query: String?, limit: Int) = dm.smevNames(query, limit)
    override fun execNames(query: String?, limit: Int, orgId: String) = dm.execNames(query, limit, orgId)
    override fun allOrgs(query: String?, limit: Int) = dm.orgs(query, limit)
    override fun smevOfAppeal(appealId: Long?, interaction: DictValue) = dm.smevOfAppeal(appealId, interaction)
    override fun applicantTypes(targetId: Int) = dm.applicantTypes(targetId)
}
