package ru.insoft.libs.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Timestamp
import java.time.LocalDateTime

// Используем это так как у заказчика
// по какой-то причине вываливается ошибка при получении даты из базы
class LocalDateTimeHandler : BaseTypeHandler<LocalDateTime>() {
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: LocalDateTime, jdbcType: JdbcType?) {
        ps.setTimestamp(i, Timestamp.valueOf(parameter))
    }

    override fun getNullableResult(rs: ResultSet?, columnName: String?): LocalDateTime? {
        return rs?.getTimestamp(columnName)?.toLocalDateTime()
    }

    override fun getNullableResult(rs: ResultSet?, columnIndex: Int): LocalDateTime? {
        return rs?.getTimestamp(columnIndex)?.toLocalDateTime()
    }

    override fun getNullableResult(cs: CallableStatement?, columnIndex: Int): LocalDateTime? {
        return cs?.getTimestamp(columnIndex)?.toLocalDateTime()
    }
}
