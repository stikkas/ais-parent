package ru.insoft.libs.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Time
import java.time.LocalTime

@MappedTypes(LocalTime::class)
class LocalTimeHandler : BaseTypeHandler<LocalTime>() {
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: LocalTime, jdbcType: JdbcType?) {
        ps.setTime(i, Time.valueOf(parameter))
    }

    override fun getNullableResult(rs: ResultSet?, columnName: String?): LocalTime? {
        return rs?.getTime(columnName)?.toLocalTime()
    }

    override fun getNullableResult(rs: ResultSet?, columnIndex: Int): LocalTime? {
        return rs?.getTime(columnIndex)?.toLocalTime()
    }

    override fun getNullableResult(cs: CallableStatement?, columnIndex: Int): LocalTime? {
        return cs?.getTime(columnIndex)?.toLocalTime()
    }
}
