package ru.insoft.libs.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import java.sql.CallableStatement
import java.sql.Date
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.time.LocalDate

// Используем это так как у заказчика
// по какой-то причине вываливается ошибка при получении даты из базы
class LocalDateHandler : BaseTypeHandler<LocalDate>() {
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: LocalDate, jdbcType: JdbcType?) {
        ps.setDate(i, Date.valueOf(parameter))
    }

    override fun getNullableResult(rs: ResultSet?, columnName: String?): LocalDate? {
        return rs?.getDate(columnName)?.toLocalDate()
    }

    override fun getNullableResult(rs: ResultSet?, columnIndex: Int): LocalDate? {
        return rs?.getDate(columnIndex)?.toLocalDate()
    }

    override fun getNullableResult(cs: CallableStatement?, columnIndex: Int): LocalDate? {
        return cs?.getDate(columnIndex)?.toLocalDate()
    }
}
