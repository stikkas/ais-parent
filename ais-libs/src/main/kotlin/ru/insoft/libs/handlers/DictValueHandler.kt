package ru.insoft.libs.handlers

import org.apache.ibatis.type.EnumTypeHandler
import org.apache.ibatis.type.JdbcType
import ru.insoft.libs.models.DictValue
import java.sql.PreparedStatement
import java.sql.ResultSet

class DictValueHandler : EnumTypeHandler<DictValue>(DictValue::class.java) {
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: DictValue, jdbcType: JdbcType?) {
        ps.setString(i, parameter.id);
    }

    override fun getNullableResult(rs: ResultSet, columnName: String): DictValue? {
        val s = rs.getString(columnName)
        return if (s == null) null else DictValue.toDictValue(s)
    }
}
