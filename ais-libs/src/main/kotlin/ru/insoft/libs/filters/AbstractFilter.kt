package ru.insoft.libs.filters

import java.net.URLDecoder

/**
 * Общий функционал фильтров для поиска по регистрам
 */
abstract class AbstractFilter {
    /**
     * Поле для сортировки
     */
    var sortField: String? = null
        @MySetter("sortField") set

    /**
     * Направление сортировки
     */
    var desc: Boolean = false
        @MySetter("desc", Boolean::class) set

    fun decodeAndTrim(value: String?) = value?.let { URLDecoder.decode(it, "UTF-8")?.trim() }
}
