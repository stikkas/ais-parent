package ru.insoft.libs.filters

import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY_SETTER)
annotation class MySetter(val value: String, val type: KClass<*> = String::class)
