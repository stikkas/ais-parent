package ru.insoft.libs.models

interface ContainsFile<T> {
    var id: T?
    var fileName: String?
}
