package ru.insoft.libs.models

open class Dict<T> {
    var value: T? = null
    var label: String? = null
}
