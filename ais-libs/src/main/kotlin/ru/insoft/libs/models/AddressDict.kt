package ru.insoft.libs.models

import com.fasterxml.jackson.annotation.JsonIgnore

/**
 * Объект уровня адреса
 * value - идентификатор адреса (addressid)
 * label - название адреса (addressname)
 * short - сокращенное название типа (shortname)
 */
class AddressDict : DictShort<String>() {
    /**
     * Название адресного объекта (типа)
     */
    var type: String? = null

    /**
     * Сокращенное название типа
     */
    override var short: String = ""
        set(value) {
            field = when (value) {
                "двлд.", "влд.", "влд", "зд.", "зд" -> "д."
                else -> value
            }
        }

    /**
     * Идентификатор уровня
     */
    var levelId: String? = null

    /**
     * Название уровня
     */
    var level: String? = null

    /**
     *  Порядковый номер типа
     */
    var order: Int = 0

    /**
     * Дочерний ардес
     * используется дла дома (корпус, строение)
     */
    var child: AddressDict? = null

    /**
     * Поля для корпуса
     */
    @JsonIgnore
    var korpLabel: String? = null

    @JsonIgnore
    var korpShort: String = ""

    @JsonIgnore
    var korpType: String? = null

    @JsonIgnore
    var korpLevelId: String? = null

    @JsonIgnore
    var korpLevel: String? = null

    @JsonIgnore
    var korpOrder: Int = 0

    /**
     * Поля для строения/сооружения/литера
     */
    @JsonIgnore
    var strLabel: String? = null

    @JsonIgnore
    var strShort: String = ""

    @JsonIgnore
    var strType: String? = null

    @JsonIgnore
    var strLevelId: String? = null

    @JsonIgnore
    var strLevel: String? = null

    @JsonIgnore
    var strOrder: Int = 0
}

