package ru.insoft.libs.models

/**
 * Справочник сотрудников
 */
class DictUser : Dict<String>() {
    /**
     * ФИО
     */
    lateinit var fio: String

    /**
     * Организация
     */
    lateinit var org: String

    /**
     * Должность
     */
    lateinit var pos: String
}
