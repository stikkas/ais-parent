package ru.insoft.libs.models

open class Address {
    var id: String? = null

    /**
     * Уровни адреса, например 'Страна' - 'Россия', 'Область' - 'Земляничная'
     */
    var levels: List<AddressDict> = emptyList()

    /**
     * идентификатор страны
     */
    var countryId: String? = null

    /**
     * название страны для отображения
     */
    var country: String? = null

    /**
     * Страна для неклассифицированного ардеса
     */
    var countryText: String? = null

    /**
     * Неклассифицированный адрес
     */
    var addressText: String? = null
}


