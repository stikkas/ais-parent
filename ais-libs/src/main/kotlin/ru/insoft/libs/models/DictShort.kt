package ru.insoft.libs.models

open class DictShort<T> : Dict<T>() {
    open var short: String = ""
}
