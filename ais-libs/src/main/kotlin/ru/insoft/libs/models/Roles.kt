package ru.insoft.libs.models

/**
 * Используемые роли
 */
object Roles {
    val specialist = "8da02404-413a-11eb-8b60-3c970e170448" // Специалист Опека
    val bossSpecialist = "f11ef35e-889c-11ea-b0df-c0b6f966a800" // Главный Специалист Опека
    val admin = "54ddd008-0d00-11eb-ac5b-c8d9d2854ef7" // Администратор Опека
    val specialistBddsop = "SPECIALIST:bddsop" // Специалист БДДСОП
}
