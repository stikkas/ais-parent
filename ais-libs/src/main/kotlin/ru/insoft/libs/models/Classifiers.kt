package ru.insoft.libs.models

/**
 * Константы для справочников
 */
object Classifiers {

    // @formatter:off
    // Типы документов
    val opudt = "d87fac08-2b22-11ea-9ce7-00155d00050e" // Документы опекуна, попечителя, усыновителя
    val sdt = "d87fca26-2b22-11ea-9ce7-00155d00050e" // Документы судебные
    val zgsdt = "8a68e544-30b6-11eb-b6ab-00155d02221d" // Документы органов ЗАГС
    val opdt = "a6020c54-33d1-11eb-821f-00155d02221d" // Документы Опеки
    val gdt = "07130a56-6e71-11ea-a3fc-00155d02221d" // Общие документы
    val regdt = DictValue.REG_DTYPE.id // Документы постановка на учет
    val opcdt = "b6913db6-6e87-11ea-bdb2-00155d02221d" // Документы, устанавливающие основания для передачи ребенка на воспитание в семью
    val brdt = "1e289af4-c019-11ea-8ef2-00155d02221d" // Документы, подтверждающие расходы на приобретение путевки
    val fdt = "8ee11942-c019-11ea-8ef2-00155d02221d" // Проездные документы
    val tfdt = "cde3859a-cfd2-11e9-8689-ff248ef910c1" // Документ, подтверждающий факт смерти
    val meddt = "d87fbf0e-2b22-11ea-9ce7-00155d00050e" // Медецинские документы
    val cncl = "db5b569a-797c-11ea-8774-00155d02221d" // Статус решения Прекращено
    val declined = DictValue.DECLINE_PLEA_STATUS.id // Статус решения Отклонено
    val incrt = "c0695db0-87d0-11eb-afe6-00155d02221d" // Статус решения В суде
    val umzg = "913d965c-daea-11ea-9555-00155d02221d" // Причина прекращения Семейного устройства "Изменение места жительства, переезд"
    val iumzg = "64612746-f991-11ea-9d9a-00155d02221d" // Причина прекращения устройства недееспособного "Изменение места жительства, переезд"
    val moop = "3943b3a6-862a-11ea-a919-00155d02221d" // "Идентификатор Департамент социального развития Ханты-Мансийского автономного округа - Югры"
    val cacc = "9ac23728-5d3b-11ea-a380-00155d02221d" // Результат посещения согласие.
    val agrr = "f0263d2c-50b1-4225-a523-7dddf6fc4962" // Имущество сельскохозяйственного значения
    val realr = "5a676706-8722-4754-b348-6cfcc89b7caa" //  Недвижимое имущество
    val autor = "c4b0b919-765d-43ef-939b-4065114bac78" // Транспортные средства
    val domr = "2d429222-4d98-11ea-a424-00155d02221d" // Бытовое имущество
    var cpro = "a349f381-2538-4a4e-b6f8-169387ac1fc9" // Причины о продлении заявления
    var socst = "17ab3925-172d-4260-9158-1a90286e8fc1" // Социальные статусы
    var fmst = "4667b8ec-934c-11e9-acb0-2fabac323758" // Семейное положение
    var statef = "a1f88e24-8efe-11ec-8277-00155d02221d" // Семейное положение для кандидатов (таблица applicant)
    var trbl = "f97c9ac0-6700-46cd-91be-92aaaecb9ba4" // Статусы неблагополучия
    var lsrs = "15275b88-bb86-45f4-98a8-32d19cb882e8" // Вид досуга
    var dthc = DictValue.DEATH_CERT.id // Свидетельство о смерти
    var urdt = DictValue.UREG_DTYPE.id // Документы для снятия с учета
    var radr = DictValue.REG_ADDR.id // Адрес регистрации
    var ladr = DictValue.FACT_ADDR.id // Фактический адрес проживания
    // Типы постановки на учет
    var erreg = DictValue.REG_EVENT_TYPE.id // Первичная постановка учет
    var secreg = DictValue.SEC_EVENT_TYPE.id // Повторная постановка на учет
    var dangr = DictValue.DANG_EVENT_TYPE.id // Группа риска
    var monst = DictValue.MONITOR_START.id // Начало социального мониторинга
    var monen = DictValue.MONITOR_END.id // Окончание социального мониторинга
    var unreg = DictValue.UNREG_TUTEV.id
    var bunreg = DictValue.UNREG_TUTEV_BD.id

    var mpun = "38573bd7-abcd-4a58-bcff-1bfbe275a7d4" // Меры наказания
    var lawd = "0e4ffc28-ed30-4150-a643-911e4d402990" // Решения по делу
    val hardc = "64daec73-807a-42ad-864a-64a0f4b414af" // Отягчающие обстоятельства
    val softc = "862bf537-1f3a-458b-951d-d6a93f06161f" // Смягчающие обстоятельства
    var lowa = "8982c5f7-05c4-4cbd-880b-7ba3794f2ce1" // Законодательство
    var lowdm = "42d622dd-1466-4e27-b2c5-d312d04f447f" // Административное право
    var lowsc = "676a07a0-e128-4997-8909-ff9cc761b41c" // Социальное право
    var lowv = "a0e6054f-5294-48d0-bc4a-695dd572f339" // Уголовное право
    val comq = "f4b0b177-ccad-44c3-8da5-8aa9d00dc040" // Информация о деле - Общие вопросы
    val adl = "ff7bd371-0e78-415e-9e25-a57aa37db90f" // Информация о деле - Административное дело
    var prar = "42975383-f433-4e6f-a26c-2822d3e59c3a" // Содержание профилактической работы с семьей
    val need = DictValue.NEED.id  // Нуждаемость
    val dkdpz = "34d5a3a5-394f-4777-8a6b-c8caac81e53a" // Документы КДНиЗП
    val cout = "dcba2ea3-ce5b-4748-8762-55da3e18630f" // Причина статуса "Изменение места жительства (выбыл/уехал)"
    val ofadt = "f332ad8c-87d0-11eb-9b78-00155d02221d" // Причина "Оформление усыновления"
    val readt = "0a249f1e-87d1-11eb-9915-00155d02221d" // Причина "Отказано в усыновлении"

    val fcat = DictValue.FAMILY_CAT.id // Признаки учета семьи
    val armc = DictValue.ARM_CRIT.id // Критерии неблагополучия
    val orgts = DictValue.ORG_TYPE.id // Типы учереждений
    val lacc = DictValue.LIC_ACCOUNT_PEREVOD.id // Перечисление на лицевой счёт
    val alorg = DictValue.APPLICANT_ORG_TYPE.id // Тип заявителя - Организация
    val albaby = DictValue.APPLICANT_CHILD_TYPE.id // Тип заявителя - Ребенок
    val tdt = DictValue.DOC_TYPES.id // Тип документов
    val mcrt = DictValue.MARRY_CERT.id // Свидетельство о браке
    val wife = DictValue.WIFE.id // Жена
    val husband = DictValue.HUSBAND.id // Муж
    val incts = DictValue.INCOME_TYPE.id // Тип дохода
    val oopdt = DictValue.OOP_DOCS.id // Документы органа опеки и попечительства
    val danlv = "7cd96396-0474-11ea-8224-00155d00050e" // Непосредственная угроза жизни или здоровью ребенка
    val rtnprn = "dda1160a-048e-11ea-92bb-00155d00050e" // Возвращены родителям
    val dnodt = "e8b997f1-c060-4fe2-8fd2-4b99060af880" // Договор найма жилого помещения для детей-сирот ..., оставшихся без попечения родителей
    val outao = "1bd96252-a8cf-11eb-860f-00155d02221d" // Переезд за пределы автономного округа на постоянное место жительства
    val cfout = "24051e30-a8cf-11eb-9a11-00155d02221d" // "Переезд за пределы автономного округа на постоянное место жительства"
    // Резделы программ для ИПР
    val psyipr = "642275b8-6490-11eb-873f-00155d02221d" // Психологическое сопровождение
    val medipr = "6ffb4a2c-6490-11eb-873f-00155d02221d" // Медицинское сопровождение
    val socipr = "7c2c63ee-6490-11eb-873f-00155d02221d" // Социальное сопровождение
    val pedipr = "88869f74-6490-11eb-873f-00155d02221d" // Педагогическое сопровождение
    val eduipr = "939c8b3a-6490-11eb-873f-00155d02221d" // Воспитательное сопровождение

    val opdog = "d87f9f2e-2b22-11ea-9ce7-00155d00050e" // Договоры на опеку
    val ldt = "b1729a6f-39e9-4709-9b89-febe7374705f" // документы о жилье
    var prsdk = DictValue.PEREEZD_KINDER.id  // Причина В связи с переменой места жительства, переезд для детей
    var prsda = "7043f5fe-499a-11ea-9f81-00155d02221d" // Причина В связи с переменой места жительства, переезд для недееспособных
    var phps = "74f36746-769a-11eb-9349-00155d02221d" // Вид опекуна - Физическое лицо
    var bsorg = "6e5a68da-769a-11eb-9349-00155d02221d" // Вид опекуна - Руководитель учреждения
    var ooip = "7cae705c-769a-11eb-9349-00155d02221d" // Вид опекуна - ООиП
    var reg = DictValue.REG_TUTEV.id
    var snyat = DictValue.LIST_UNREGISTERED.id // Снят с учета (статус для "Реестр списков")
    var posu = DictValue.LIST_REGISTERED.id // Поставлен на учет (статус для "Реестр списков")
    var socna = DictValue.GP_SOC_NAYM.id // Принять на учет в качестве нуждающегося в жил.помещении по договорам соц.найма
    var othop = DictValue.NEW_ORT_OOP.id // Заявление о передаче учетных дел детей-сирот в орган местного самоуправления по новому месту жительства
    var apcan = DictValue.CANCEL_APPEAL.id // Прекратить заявление
    var plus = DictValue.PLUS_PLEA_DECISION.id // Положительное решение по обращению
    val gpt = DictValue.LIST_GP.id //  Список для обеспечения ж.п. (тип цели обращения)
    val alorph = DictValue.APPLICANT_ORPHAN_TYPE.id // Лицо из числа детей - сирот
    val pat = DictValue.PERSONAL_APPEAL_TYPE.id // Личное обращение
    val grnts = DictValue.GRANTED_PLEA_STATUS.id // Статуз заявления Предоставлено
    val acpts = DictValue.ACCEPT_PLEA_STATUS.id // Назначено
    val adpts = DictValue.ADOPT_PLEA_STATUS.id // Усыновление
    val adptr = DictValue.ADOPTION_TARGET.id // Усыновление/удочерение
    // Различные движения постановки на учет
    val beredu = "9024c492-da10-11e9-b916-00155d000518"  // Поступление на обучение в профессиональные образовательные организации на полное
    val khftarr = "2301d66a-3faf-11eb-a69c-00155d02221d" // Устройство в детские дома семейного типа
    val edorarr = "ae39719c-3fb0-11eb-85ac-00155d02221d" // Устройство в образовательные организации
    val uniedu = "f68c424c-a380-11eb-aaf4-00155d02221d" // Поступление на обучение в образовательные организации высшего образования на полное
    val tmporgarr = "e85e093c-3fb0-11eb-862c-00155d02221d" // Помещение в учреждения временного пребывания (больницы, спец.учреждения для несовершеннолетних)
    val prorgarr = "0d79dbe8-3fb0-11eb-a69c-00155d02221d" // Устройство в негосударственные учреждения
    val medorgarr =  DictValue.ARRANGEMENT_MEDORGS.id // Устройство в медицинские организации
    val socorgarr = DictValue.ARRANGEMENT_SOCORGS.id // Устройство в организации, оказывающие социальные услуги
    val opoip = DictValue.SUPERVISION_TUTEV.id // Назначение опекуном ООиП
    val othcar = DictValue.OTHER_OPEKA_TUTEV.id //  Иная форма возмездной опеки (попечительства)
    val nprfcar = DictValue.NOPROFIT_OPEKA_TUTEV.id  // Безвозмездная форма опеки (попечительства)
    val adcar = DictValue.ADOPT_TUTEV.id  // Усыновление/удочерение
    val adfcar = DictValue.ADOPT_FAMILY_TUTEV.id // Приемная семья
    val patfcar = DictValue.PATRON_FAMILY_TUTEV.id // Патронатная семья
    val precar = DictValue.PREOPEKA_TUTEV.id // Предварительная опека (попечительство)

    var reqrec = DictValue.RECEIVED_REQ_STATUS.id // Статус запроса - Получен
    // @formatter:on
    // Новый формат справочников, те которые не передаются на клиента должны быть помечены @JsonIgnore
    // Уходим от DictValue - все используемые справочники будут храниться здесь

    /**
     * Решение суда с изменением персональных данных
     */
    val withChangePD = "3ea7ab32-87d1-11eb-9915-00155d02221d"

    /**
     * Решение суда без изменения персональных данных
     */
    val woutChangePD = "4f9cf230-87d1-11eb-9915-00155d02221d"

    /**
     * Виды цели обращения
     */
    val kindPleaTarget = "43ea5946-0fda-11ec-8490-00155d02221d"

    /**
     * Учащийся
     */
    val learning = "7724ad27-bff9-457c-a3a6-9b2337b78d2b"

    /**
     * Заявление об установлении патронажа
     */
    val pleaPatronage = "f3ab176c-1084-11ec-a3bc-00155d02221d"

    /**
     * Причины постановки на учет дееспособных
     */
    val regCaseNormal = "944cc0f4-1084-11ec-a3bc-00155d02221d"

    /**
     * Причины снятия с учета нуждающихся в патронаже
     */
    val unregCasePatronage = "bf27a39c-1085-11ec-9bf8-00155d02221d"

    /**
     * Решение суда об усыновлении(удочерении)
     */
    val decCourtAdoption = DictValue.COURT_RECEIPT.id

    /**
     * Заключение органа опеки и попечительства о возможности заявителя быть помощником
     */
    val docAllowAssistant = "dfabf294-1477-11ec-bd2e-00155d02221d"

    /**
     * Заключение органа опеки и попечительства о невозможности заявителя быть помощником
     */
    val docNoallowAssistant = "0891b75c-1478-11ec-a5f2-00155d02221d"

    /**
     * Учет несовершеннолетних
     */
    val underReg = DictValue.UNDERAGE_REG.id

    /**
     * Учет нуждающихся в патронаже
     */
    val patronReg = DictValue.PATRON_REG.id

    /**
     * Учет совершеннолетних недееспособных
     */
    val incapReg = DictValue.INCAPABLE_REG.id

    /**
     * Причина движения - Оформление СУ
     */
    val caseFamilyArr = DictValue.FAMILY_ARRANGEMENT_CASE_STATUS.id

    /**
     * Причины прекращения патронажа
     */
    val stopPatronCase = "f51ecf38-172f-11ec-b5db-00155d02221d"

    /**
     * Движение в карте учета - Патронаж
     */
    val patronEvent = DictValue.PATRONAGE.id

    /**
     * проживает самостоятельно
     */
    val liveSelf = "22abb7e4-7344-11eb-8655-00155d02221d"

    /**
     * Причины прекращения устройства в организацию соц. обслуживания
     */
    val stopSocArCase = "d10e8d08-17a3-11ec-8bf2-00155d02221d"

    /**
     * Состоят в браке
     */
    val married = "b01c458c-1529-11ea-820d-00155d00050e"

    /**
     * не состоят в браке
     */
    val notMarried = "b01c49c4-1529-11ea-820d-00155d00050e"

    /**
     * нет данных
     */
    val unknowMarried = "b01c4d0c-1529-11ea-820d-00155d00050e"

    /**
     *  Решение суда о признании недееспособным
     */
    val acceptDisabled = "e6bd6a42-2b29-11ea-a399-00155d00050e"

    /**
     * Решение суда о признании не полностью дееспособным
     */
    val acceptPartDisabled = "0c2a7100-7109-11eb-94f4-00155d02221d"

    /**
     * Причины прибытия
     */
    val arrivalCause = "38c4ddae-9108-11ea-9f6d-00155d02221d"

    /**
     * Типы ответственного съемщика
     */
    val rentType = "ce6c0336-5d0c-11e9-b67f-e36b44052131"

    /**
     * Тип регистрации - "По месту пребывания"
     */
    val stayReg = DictValue.STAY_PLACE_REG.id

    /**
     * Тип съемщика - Собственник
     */
    val owner = DictValue.OWNER_PROP.id

    /**
     *  Место выдачи справок
     */
    val receiptPlace = "8cca14b0-77a6-11ec-b5ea-00155d02221d"

    /**
     * Ответственный квартиросъемщик
     */
    val mainRenter = "f3e96e98-5807-11ec-a652-00155d02221d"

    /**
     * Погибли по вине усыновителей, опекунов, попечителей, приемных родителей, патронатных воспитателей
     */
    val diedByOpekun = "dda22e00-048e-11ea-92bb-00155d00050e"

    /**
     * Смерть ребенка
     */
    val childDeath = DictValue.DEATH_CHILD_CASE_STATUS.id

    /**
     * Смерть ребенка в результате суицида
     */
    val suicideChildDeath = "dda23e86-048e-11ea-92bb-00155d00050e"

    /**
     * Достижение несовершеннолетним совершеннолетнего возраста
     */
    val moreNotChild = "dda1bdee-048e-11ea-92bb-00155d00050e"

    /**
     * Объявление несовершеннолетнего полностью дееспособным, в том числе вступление несовершеннолетнего в брак
     */
    val childMarriage = "a4bdecbe-a8cf-11eb-87c1-00155d02221d"

    /**
     * Ребенок возвращен родителям
     */
    val childRetrunedParents = "caba6820-f97f-11ea-87da-00155d02221d"

    /**
     * Смерть ребенка по вине усыновителей, опекунов, попечителей, приемных родителей, патронатных воспитателей
     */
    val deathFromOpekun = "3e9ea2b0-f980-11ea-87da-00155d02221d"

    /**
     * Смерть ребенка
     */
    val deadBaby = DictValue.CHILD_DEATH.id

    /**
     *  Смерть ребенка в результате суицида
     */
    val suicideBaby = DictValue.CHILD_DEATH_SUICIDE.id

    /**
     * Достижение подопечным совершеннолетнего возраста
     */
    val yetAdult = "d65b6ef4-f97f-11ea-87da-00155d02221d"

    /**
     * Объявление несовершеннолетнего полностью дееспособным, в том числе вступление несовершеннолетнего в брак
     */
    val childMan = "b174a916-a8cf-11eb-860f-00155d02221d"

    /**
     * Оформление другого вида устройства для несовершеннолетних
     */
    val otherArr = "16f46d3a-f980-11ea-87da-00155d02221d"

    /**
     * Оформление другого вида устройства для недееспособных
     */
    val otherArrInc = "a1555a0a-f991-11ea-9d9a-00155d02221d"

    /**
     * причина снятия с учета Смерть
     */
    val death = DictValue.DEATH.id

    /**
     * причина прекращения семейного устройства Смерть подопечного
     */
    val caredDeath = DictValue.CARED_DEATH.id

    /**
     * Результат ознакомления
     * Выдать направление на посещение
     */
    val allowVisit = "2643b282-5d3b-11ea-9e65-00155d02221d"

    /**
     * Результат ознакомления
     * Продолжить подбор
     */
    val keepChoose = "2f07ce30-5d3b-11ea-9332-00155d02221d"
}

