package ru.insoft.libs.models

data class Page<T>(val data: List<T>, val size: Int, val count: Long, val page: Long) {
    val pages = if (count == 0L) 0 else if (count <= size) 1 else count / size + (if (count % size == 0L) 0 else 1);
    val last = pages == 0L || page == pages
    val first = pages == 0L || page == 1L
}

