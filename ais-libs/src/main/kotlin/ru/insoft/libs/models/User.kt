package ru.insoft.libs.models

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.userdetails.UserDetails

/**
 * Используется для хранения данных о пользователе,
 * работающим с системой
 */
class User : UserDetails {
    /**
     * Идентификатор из таблицы userlinks
     */
    lateinit var id: String
    var fio: String? = null
    var aisId: Int? = null
    var position: String? = null

    /**
     * Поле printname из таблицы organisations
     */
    var orgName: String? = null
    var orgType: String? = null
    var orgShort: String? = null

    /**
     * Идентификатор организации пользователя
     */
    lateinit var orgId: String

    /**
     * Идентификатор из таблицы roles
     */
    lateinit var roleId: String
    var email: String? = null
    var phone: String? = null

    /**
     * Логин пользоватотеля (loginname из таблицы users)
     */
    lateinit var login: String

    @JsonIgnore
    var pass: String? = null

    @JsonIgnore
    override fun getAuthorities() = listOf(Role(roleId))

    /**
     * При логине пользователь выбирает роль, зарегестрированную в определенной организации,
     * id нужен чтобы связать его с системным пользователем, который может иметь несколько ролей
     * в разных организациях.
     */
    @JsonIgnore
    override fun getUsername() = "$id|$login|$orgId|$roleId"

    @JsonIgnore
    override fun getPassword() = pass

    @JsonIgnore
    override fun isEnabled() = true

    @JsonIgnore
    override fun isCredentialsNonExpired() = isEnabled

    @JsonIgnore
    override fun isAccountNonExpired() = isEnabled

    @JsonIgnore
    override fun isAccountNonLocked() = isEnabled
}
