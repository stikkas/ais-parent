package ru.insoft.libs.models

import org.springframework.security.core.GrantedAuthority


class Role(private val roleId: String) : GrantedAuthority {
    override fun getAuthority() = roleId
}
