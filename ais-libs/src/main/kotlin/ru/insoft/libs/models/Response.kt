package ru.insoft.libs.models

class Response {
    var status: Boolean = true
        private set
    var data: Any? = null
        private set

    private constructor()

    companion object {
        fun ok(data: Any? = null) = Response().apply { this.data = data }
        fun fail(data: Any? = null) = Response().apply {
            this.status = false
            this.data = data
        }
    }
}
