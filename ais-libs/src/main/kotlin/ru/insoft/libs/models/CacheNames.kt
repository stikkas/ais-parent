package ru.insoft.libs.models

class CacheNames {
    companion object {
        const val Roles = "roles"
        const val Ais = "ais"
        const val OrgCategories = "orgCategories"
        const val Nationalities = "nationalities"
        const val Countries = "countries"
        const val AllAddressLevels = "all-address-levels"
        const val AddressLevels = "address-levels"
        const val DocTypes = "doc-types"
        const val Classifiers = "classifiers"
        const val FamilyMembers = "family-members"
        const val ClassifiersUs = "classifiers-us"
        const val Dicts = "dicts"
        const val DictsStr = "dicts-str"
        const val Citizenships = "citizenships"
        const val DictsUs = "dicts-us"
        const val DictsUsStr = "dicts-us-str"
        const val AllTree = "all-tree"
        const val TwoLevels = "two-levels"
        const val DictsAndFlag = "dicts-and-flag"
        const val AppealRelations = "appeal-relations"
    }
}
