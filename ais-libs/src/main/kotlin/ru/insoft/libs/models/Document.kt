package ru.insoft.libs.models

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.time.LocalDate

/**
 * Данные документа личного дела
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Document : ContainsFile<String> {
    /**
     * Идентификатор документа
     */
    override var id: String? = null

    /**
     * Идентификатор типа документа
     */
    var typeId: String? = null

    /**
     * Название документа, для отображения в таблице
     */
    var name: String? = null

    /**
     * Серия
     */
    var series: String? = null

    /**
     * Номер
     */
    var number: String? = null

    /**
     * Дата выдачи
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var issueDate: LocalDate? = null

    /**
     * Дата начала действия
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var startAction: LocalDate? = null

    /**
     * Кем выдан
     */
    var whogive: String? = null

    /**
     * Код подразделения
     */
    var depCode: String? = null

    /**
     * Примечание, Причина замены
     */
    var note: String? = null

    var active: Short? = null

    /**
     * Имя файла, прикрепленного к документу
     */
    override var fileName: String? = null

    @JsonIgnore
    fun isWrong() = !noData() && typeId == null

    @JsonIgnore
    fun isEmpty() = noData() && typeId == null

    private fun noData() =
        issueDate == null && series.isNullOrBlank() && number.isNullOrBlank() && whogive.isNullOrBlank() && startAction == null
}
