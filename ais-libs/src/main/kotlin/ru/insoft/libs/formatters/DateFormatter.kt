package ru.insoft.libs.formatters

import org.springframework.format.Formatter
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.util.*

open class DateFormatter : Formatter<LocalDate?> {

    companion object {
        val format: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
    }

    override fun print(date: LocalDate, locale: Locale) = date.format(format)

    override fun parse(str: String, locale: Locale) = try {
        LocalDate.parse(str, format)
    } catch (ex: DateTimeParseException) {
        LocalDate.MIN
    }
}

