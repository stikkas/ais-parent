package ru.insoft.libs.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.models.Document

/**
 * Служит общими кусками sql для всех нуждающихся
 */
@Mapper
interface CommonMapper {
    fun findOtherDocument(id: String?): Document?
    fun insertOtherDoc(doc: Document)
    fun updateOtherDoc(doc: Document)
    fun removeOtherDoc(@Param("id") id: String, @Param("logical") logical: Boolean = false)

    /**
     * Возвращает true если заданный id является потомком parent
     */
    fun isChildOf(@Param("id") id: String, @Param("parent") parent: DictValue): Boolean

}

