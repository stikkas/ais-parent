package ru.insoft.libs.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.libs.models.*

@Mapper
interface SharedDictMapper {
    /**
     * Нужен для вызова из xml файлов, иначе не работает
     */
    fun addressLevels(@Param("point") addressId: String?): List<AddressDict>

    fun levels(
        @Param("point") addressId: String?,
        @Param("trim") trimToPaid: String? = null,
        @Param("parentId") parentId: String? = null,
        @Param("pattern") pattern: String? = null,
        @Param("limit") limit: Int? = null,
        @Param("levelId") levelId: String? = null,
        @Param("query") query: String? = null,
        @Param("sort") sort: Boolean = false
    ): List<AddressDict>

    fun addressIds(
        @Param("parentId") parentId: String?, @Param("pattern") pattern: String?,
        @Param("query") addressName: String?, @Param("limit") limit: Int?
    ): List<String>

    fun docTypes(): List<DictDoc>
    fun classifiers(@Param("parent") parent: DictValue): List<Dict<DictValue>>
    fun familyMembers(@Param("parent") parent: DictValue, @Param("aisId") aisId: Int?): List<Dict<DictValue>>
    fun classifiersUs(@Param("cls") cls: List<DictValue>): List<Dict<DictValue>>
    fun dict(@Param("parents") parent: List<DictValue>): List<Dict<String>>
    fun dictUs(@Param("cls") cls: List<DictValue>): List<Dict<String>>
    fun dictStr(@Param("parents") cls: List<String>): List<Dict<String>>
    fun dictUsStr(@Param("cls") cls: List<String>): List<Dict<String>>
    fun allTree(@Param("parent") parent: DictValue): List<Dict<String>>
    fun myOrgs(orgId: String): List<DictShort<String>>
    fun countries(@Param("query") query: String?, @Param("limit") limit: Int): List<Dict<String>>
    fun orgCategories(): List<Dict<Int>>
    fun lastNames(@Param("query") query: String?, @Param("limit") limit: Int): List<Dict<String>>
    fun smevNames(@Param("query") query: String?, @Param("limit") limit: Int): List<Dict<String>>
    fun logins(@Param("query") query: String?, @Param("limit") limit: Int): List<Dict<String>>
    fun roles(aisId: Int): List<Dict<String>>

    fun execNames(
        @Param("query") query: String?,
        @Param("limit") limit: Int,
        @Param("orgId") orgId: String
    ): List<Dict<String>>

    fun orgs(@Param("query") query: String?, @Param("limit") limit: Int): List<Dict<String>>
    fun positions(@Param("login") login: String, @Param("aisId") aisId: Int): List<Dict<String>>
    fun ais(): List<Dict<Int>>
    fun orgTypes(@Param("catId") catId: Int?): List<Dict<String>>
    fun smevOfAppeal(@Param("appealId") appealId: Long?, @Param("interId") interaction: DictValue): List<DictSmev>
    fun citizenShips(@Param("query") query: String?, @Param("limit") limit: Int): List<Dict<String>>
    fun nationalities(@Param("query") query: String?, @Param("limit") limit: Int): List<DictShort<String>>
    fun whogives(@Param("query") query: String?, @Param("limit") limit: Int): List<Dict<String>>
    fun jobPlaces(@Param("query") query: String?, @Param("limit") limit: Int): List<Dict<String>>
    fun zags(query: String?): List<Dict<String>>
    fun whogivesOther(@Param("query") query: String?, @Param("limit") limit: Int): List<Dict<String>?>
    fun whogivesZags(): List<Dict<String>?>
    fun opekaFamilyKids(familyId: String): List<DictShort<String>>
    fun applicantTypes(targetId: Int): List<Dict<String>>?
    fun sameOrgs(@Param("orgId") orgId: String, @Param("newOrgId") newOrgId: String?): List<Dict<String>>
    fun users(@Param("userId") userId: String?, @Param("orgId") id: String, @Param("ais") aisId: Int): List<DictUser>
    fun registerOOP(orgId: String?): List<DictShort<String>>
}
