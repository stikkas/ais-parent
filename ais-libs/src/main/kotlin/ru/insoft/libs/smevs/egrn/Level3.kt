package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Строение
 */
class Level3 {

    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_level3") set

    /**
     * Значение
     */
    var name: String? = null
        @XmlElement(name = "name_level3") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")
}
