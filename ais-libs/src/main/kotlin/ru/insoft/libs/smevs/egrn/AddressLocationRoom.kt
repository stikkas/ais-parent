package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class AddressLocationRoom {

    /**
     * Адрес (местоположение) помещения
     */
    var address: AddressOksLocation? = null
        @XmlElement set

    /**
     * Номер комнаты в квартире
     */
    var number: String? = null
        @XmlElement(name = "room_number") set

    override fun toString() = listOfNotNull(address, number?.let { "Номер комнаты в квартире: $it" })
        .joinToString("\n")
}
