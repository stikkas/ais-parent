package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class ConstructionRecords {
    var records: List<ConstructionRecord>? = null
        @XmlElement(name = "construction_record") set

    override fun toString() = records?.joinToString("\n") ?: ""
}
