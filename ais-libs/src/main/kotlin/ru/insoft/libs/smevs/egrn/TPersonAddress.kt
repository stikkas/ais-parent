package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

/**
 * Выбрать фактический адрес человека из smev_plea.man_id.
 * Если у данного человека нет фактического адреса, выбрать адрес регистрации
 */
@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/commons/Subjects",
    propOrder = ["livingAddress", "fias", "region"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class TPersonAddress {
    var livingAddress = true
    var fias: String? = null
    var region: TAddressElement2? = null
}
