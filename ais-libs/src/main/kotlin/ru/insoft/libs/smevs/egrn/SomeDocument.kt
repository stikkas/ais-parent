package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

/**
 * Набор документов прикладываемых к заявлению
 */
@XmlType(namespace = "http://rosreestr.ru/services/v0.1/commons/Documents")
@XmlAccessorType(XmlAccessType.FIELD)
class SomeDocument {
    /**
     * Документ, удостоверяющий личность
     */
    var idDocument: IdDocumentRestr? = null
}
