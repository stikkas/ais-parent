package ru.insoft.libs.smevs.socpay

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

@XmlRootElement(name = "BenefitsFormInfoRequest")
@XmlAccessorType(XmlAccessType.FIELD)
class SocPayRequest {

    @XmlElement(name = "Snils")
    var snils: String? = null
        set(value) {
            field = value?.replace("-", "")
        }

    @XmlElement(name = "FamilyName", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    lateinit var lastName: String

    @XmlElement(name = "FirstName", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    lateinit var firstName: String

    @XmlElement(name = "Patronymic", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    var middleName: String? = null

    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    @XmlElement(name = "BirthDate")
    lateinit var birthDate: LocalDate

    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    @XmlElement(name = "AppDate")
    val date = LocalDate.now()

    @XmlElement(name = "TerOrganName")
    val organName = "МИЦ ПФР"

    @XmlElement(name = "TerOrganCode")
    val organCode = "101000"

    /**
     * Тип причины запроса
     * Подтверждение факта назначения в определённом размере (c детализацией).
     */
    @XmlElement(name = "IssuingReason")
    val reason = "003"


    override fun toString() = """СНИЛС: ${snils ?: ""}
        |ФИО: ${listOfNotNull(lastName, firstName, middleName).joinToString(" ")}
        |Дата рождения: ${birthDate.format()}
        |Дата, на которую предоставляются сведения об установленных выплатах: ${date.format()}
        |""".trimMargin()
}
