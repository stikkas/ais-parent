package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения об ограничениях прав и обременениях объекта недвижимости (части (частей) ОН)
 */
class RestrictRecordsIndivid {
    var records: List<RestrictRecord>? = null
        @XmlElement(name = "restrict_record") set

    override fun toString() = records?.mapNotNull { it.data }?.joinToString("\n") ?: ""
}
