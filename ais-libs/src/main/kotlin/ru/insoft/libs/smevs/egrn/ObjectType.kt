package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Общие сведения об объекте недвижимости (вид, кадастровый номер)
 */
class ObjectType {
    /**
     * Общие сведения
     */
    var data: CommonDataType? = null
        @XmlElement(name = "common_data") set

    override fun toString() = listOfNotNull(data?.type?.value?.let { "Вид объекта недвижимости: $it" },
        data?.number?.let { "Кадастровый номер: $it" }).joinToString("\n")
}
