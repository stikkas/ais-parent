package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class EstateRecords {

    /**
     * Единый недвижимый комплекс
     */
    var records: List<UnifiedRealEstateComplexRecord>? = null
        @XmlElement(name = "unified_real_estate_complex_record") set

    override fun toString() = records?.joinToString("\n") ?: ""
}
