package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Дом
 */
class Level1 {

    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_level1") set

    /**
     * Значение
     */
    var name: String? = null
        @XmlElement(name = "name_level1") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")
}
