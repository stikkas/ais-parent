package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Правообладатель ФЛ
 */
@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/commons/Subjects",
    propOrder = ["surname", "firstname", "patronymic", "birthDate", "idDocumentRef", "address", "snils"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class TEGRNRequestOwner {
    var surname: String? = null
    var firstname: String? = null
    var patronymic: String? = null

    /**
     * Дата рождения
     */
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var birthDate: LocalDate? = null

    /**
     * Данные удостоверения личности
     */
    var idDocumentRef = TDocumentRefer()

    /**
     * Адрес места жительства или пребывания
     */
    var address: TPersonAddress? = null

    /**
     * СНИЛС
     */
    var snils: String? = null

    override fun toString() = """ФИО: ${listOfNotNull(surname, firstname, patronymic).joinToString(" ")}
        ${snils?.let { "|СНИЛС: $it" } ?: ""}
        |Дата рождения: ${birthDate?.format()}""".trimMargin()
}
