package ru.insoft.libs.smevs.marry

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о нотариальном засвидетельствовании документа
 */
class SvidetelType {
    /**
     * Регистрационный номер в реестре нотариальных действий
     */
    var regNumber: String? = null
        @XmlAttribute(name = "РегНомРеестрНотар") set

    /**
     * Дата совершения нотариальных действий
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlAttribute(name = "ДатаНотарДейств") set

    /**
     * ФИО нотариуса
     */
    var notarius: String? = null
        @XmlAttribute(name = "ФИОНотар") set

    /**
     * ФИО переводчика
     */
    var dolmetscher: String? = null
        @XmlAttribute(name = "ФИОПеревод") set

    /**
     * Количество листов копии документа (перевода документа)
     */
    var pageCount: Int? = null
        @XmlAttribute(name = "КолЛистДок") set
}
