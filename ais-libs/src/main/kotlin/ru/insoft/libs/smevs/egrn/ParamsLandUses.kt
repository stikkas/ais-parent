package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Характеристики земельного участка (площадь, разрешенное использование)
 */
class ParamsLandUses {
    /**
     * Площадь
     */
    var area: LandArea? = null
        @XmlElement set

    /**
     * Разрешенное использование
     */
    var permittedUse: AllowedUse? = null
        @XmlElement(name = "permitted_use") set

    /**
     * Разрешенное использование по градостроительному регламенту
     */
    var permittedUseGrad: PermittesUsesGradReg? = null
        @XmlElement(name = "permittes_uses_grad_reg") set

    override fun toString() = listOfNotNull(
        area?.value?.let { "Площадь: $it" },
        permittedUse?.toString(), permittedUseGrad?.toString()
    ).filter { it.isNotEmpty() }.joinToString("\n")
}
