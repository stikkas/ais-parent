package ru.insoft.libs.smevs.adapters

import java.time.LocalTime
import javax.xml.bind.annotation.adapters.XmlAdapter

class LocalTimeAdapter : XmlAdapter<String, LocalTime>() {
    override fun marshal(v: LocalTime?) = v?.let { it.toString() }

    override fun unmarshal(v: String?) = v?.let {
        LocalTime.parse(v)
    }
}
