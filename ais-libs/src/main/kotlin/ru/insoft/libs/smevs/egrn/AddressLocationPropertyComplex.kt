package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Местоположение предприятия, как имущественного комплекса (адрес (местонахожение) правообладателя)
 */
class AddressLocationPropertyComplex {
    /**
     * Адрес
     */
    var address: AddressMain? = null
        @XmlElement set

    /**
     * Местоположение
     */
    var location: LocationOks? = null
        @XmlElement set

    override fun toString() = listOfNotNull(address, location).joinToString("\n")
}
