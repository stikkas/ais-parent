package ru.insoft.libs.smevs.sud

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Переквалификация
 */
@XmlAccessorType(XmlAccessType.FIELD)
class ReQualification {
    /**
     * Дата
     */
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var date: LocalDate? = null

    /**
     * Орган
     */
    var enforcer: String? = null

    /**
     * Переквалификация с
     */
    var from: List<Legislation>? = null

    /**
     * Переквалификация на
     */
    var to: List<Legislation>? = null

    override fun toString(): String {
        val res = ArrayList<String>()
        date?.let { res.add(it.toString()) }
        enforcer?.let { res.add(it) }
        from?.forEach { res.add(it.toString()) }
        to?.forEach { res.add(it.toString()) }
        return res.filter { it.isNotBlank() }.joinToString()
    }
}
