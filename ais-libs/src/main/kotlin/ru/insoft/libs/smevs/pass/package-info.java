@XmlSchema(
		elementFormDefault = XmlNsForm.QUALIFIED,
		namespace = "urn://mvd/guvm/passport-validity/1.0.0",
		xmlns = {
				@XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
				@XmlNs(prefix = "ns", namespaceURI = "urn://mvd/guvm/passport-validity/1.0.0")
		}
)
package ru.insoft.libs.smevs.pass;


import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
