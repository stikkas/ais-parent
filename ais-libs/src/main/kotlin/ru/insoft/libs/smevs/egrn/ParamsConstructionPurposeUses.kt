package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Характеристики сооружения (основные параметры, назначение, разрешенное использование)
 */
class ParamsConstructionPurposeUses {
    /**
     * Основные характеристики
     */
    var params: BaseParameters? = null
        @XmlElement(name = "base_parameters") set

    /**
     * Назначение
     */
    var purpose: String? = null
        @XmlElement set

    /**
     * Вид(ы) разрешенного использования
     */
    var uses: PermittedUses? = null
        @XmlElement(name = "permitted_uses") set

    override fun toString() = listOfNotNull(
        params, purpose?.let { "Проектируемое назначение: $it" }, uses?.let { "Вид разрешенного использования: $it" }
    ).joinToString("\n")
}
