package ru.insoft.libs.smevs.egrn

import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

/**
 * Выписка из ЕГРН о правах отдельного лица на имевшиеся/имеющиеся у него ОН
 */
@XmlType(
    propOrder = ["owner", "period", "realtyTypes", "territory"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class TExtractSubjectAction {
    /**
     * Правообладатель
     */
    var owner: TEGRNRequestOwnerParams? = null

    /**
     * За период
     */
    var period = TPeriod().apply { dateEnd = LocalDate.now() }

    /**
     * Вид (ы) объекта (ов) недвижимости
     */
    var realtyTypes = TRealtyTypes()

    /**
     * Территория РФ
     */
    var territory = TTerritory()
}
