package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Машино-место
 */
class CarParkingSpaceRecord {

    /**
     * Общие сведения об объекте недвижимости
     */
    var obj: ObjectType? = null
        @XmlElement(name = "object") set

    /**
     * Характеристики машино-места
     */
    var params: Area? = null
        @XmlElement set

    /**
     * Адрес (местоположение) машино-места
     */
    var address: AddressLocationCarParkingSpace? = null
        @XmlElement(name = "address_room") set

    /**
     * Сведения о праве
     */
    var rightRecord: RightRecordIndivid? = null
        @XmlElement(name = "right_record") set

    /**
     * Сведения об ограничениях прав и обременениях объекта недвижимости
     */
    var restrictRecords: RestrictRecordsIndividNoPart? = null
        @XmlElement(name = "restrict_records") set

    override fun toString() = listOfNotNull(
        obj, params?.area?.let { "Площадь, в кв. метрах: $it" },
        address?.address, rightRecord, restrictRecords
    ).joinToString("\n")
}
