package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.format
import javax.xml.bind.annotation.XmlElement

/**
 * Документы-основания (все реквизиты)
 */
class UnderlyingDocumentsOut {

    var docs: List<UnderlyingDocumentOut>? = null
        @XmlElement(name = "underlying_document") set

    override fun toString() = docs?.map {
        listOfNotNull(
            "Документы-основания:",
            it.code?.value?.let { "Код документа: $it" },
            it.series?.let { "Серия документа: $it" },
            it.name?.let { "Наименование: $it" },
            it.number?.let { "Номер документа: $it" },
            it.date?.let { "Дата документа: ${it.format()}" },
            it.issuer?.let { "Орган власти, организация, выдавшие документ: $it" }
        ).joinToString("\n")
    }?.filter { it.isNotEmpty() }?.joinToString("\n") ?: ""
}
