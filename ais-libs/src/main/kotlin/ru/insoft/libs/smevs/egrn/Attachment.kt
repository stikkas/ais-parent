package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

/**
 * Прикладываемый документ
 */
@XmlType(namespace = "http://rosreestr.ru/services/v0.1/commons/Documents")
@XmlAccessorType(XmlAccessType.FIELD)
class Attachment {
    /**
     * Атрибуты файла
     */
    var fileDesc: FileDesc? = null
}
