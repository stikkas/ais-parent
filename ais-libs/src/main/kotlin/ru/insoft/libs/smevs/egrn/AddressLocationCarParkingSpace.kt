package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Адрес (местоположение) машино-места
 */
class AddressLocationCarParkingSpace {
    var address: AddressOksLocation? = null
        @XmlElement set
}
