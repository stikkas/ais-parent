package ru.insoft.libs.smevs.death

import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * Сведения о дате по представленным документам
 */
@XmlType(namespace = "urn://x-artefacts-zags-fatalinf/types/4.0.1")
class PartialDate {

    /**
     * Сведения о дне в соответствии с представленными документами
     */
    var day: Int? = null
        @XmlElement(name = "День") set

    /**
     * Сведения о месяце в соответствии с представленными документами
     */
    var month: Int? = null
        @XmlElement(name = "Месяц") set

    /**
     * Сведения о годе в соответствии с представленными документами
     */
    var year: Int? = null
        @XmlElement(name = "Год") set

    fun toLocalDate(): LocalDate? = year?.let { LocalDate.of(it, month ?: 1, day ?: 1) }

    override fun toString() = listOf(day ?: "01", month ?: "01", year?.toString() ?: "").joinToString(".")
}
