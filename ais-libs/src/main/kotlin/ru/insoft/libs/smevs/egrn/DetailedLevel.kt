package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Детализированный уровень
 */
class DetailedLevel {

    /**
     * Улица
     */
    var street: Street? = null
        @XmlElement set

    /**
     * Дом
     */
    var level1: Level1? = null
        @XmlElement set

    /**
     * Корпус
     */
    var level2: Level2? = null
        @XmlElement set

    /**
     * Строение
     */
    var level3: Level3? = null
        @XmlElement set

    /**
     * Квартира
     */
    var apartment: Apartment? = null
        @XmlElement set

    /**
     * Иное описание местоположения
     */
    var other: String? = null
        @XmlElement set

    override fun toString() = listOfNotNull(
        street?.toString(), level1?.toString(), level2?.toString(), level3?.toString(), apartment?.toString(), other
    ).filter { it.isNotEmpty() }.joinToString()
}
