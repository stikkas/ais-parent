package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Запрос на предоставление сведений
 */
@XmlAccessorType(XmlAccessType.FIELD)
class TRequestDataAction {
    /**
     * Выписка из ЕГРН о правах отдельного лица на имевшиеся/имеющиеся у него объекты недвижимости
     */
    var extractSubject: TExtractSubjectAction? = null
}
