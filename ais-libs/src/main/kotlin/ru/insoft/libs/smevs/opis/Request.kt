package ru.insoft.libs.smevs.opis

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

/**
 * Файл описи для запроса ЕГРН
 */
@XmlType(propOrder = ["statementFile", "file", "requestType"])
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
class Request {
    /**
     * Файл запроса
     */
    var statementFile: StatementFile? = null

    /**
     * Файл сканированной копии документа
     */
    var file: StatementFile? = null

    var requestType = "111300003000"
}
