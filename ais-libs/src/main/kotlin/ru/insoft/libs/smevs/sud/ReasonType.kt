package ru.insoft.libs.smevs.sud

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Причина
 */
@XmlAccessorType(XmlAccessType.FIELD)
class ReasonType {
    /**
     * Описание
     */
    var description: String? = null

    /**
     * Основание
     */
    var cause: List<Legislation>? = null

    override fun toString(): String {
        val res = ArrayList<String>()
        description?.let { res.add(it) }
        cause?.forEach { res.add(it.toString()) }
        return res.filter { it.isNotBlank() }.joinToString()
    }
}
