package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения об объекте (объектах) недвижимости,  правах, ограничениях прав, обременениях объекта недвижимости
 */
class BaseDataRightRestrictIndivid {
    /**
     * Земельные участки
     */
    var records: LandRecords? = null
        @XmlElement(name = "land_records") set

    /**
     * Помещения
     */
    var rooms: RoomRecords? = null
        @XmlElement(name = "room_records") set

    /**
     * Машино-места
     */
    var carRooms: CarRooms? = null
        @XmlElement(name = "car_parking_space_records") set

    /**
     * Предприятия как имущественные комплексы
     */
    var properties: PropertyRecords? = null
        @XmlElement(name = "property_complex_records") set

    /**
     * Единые недвижимые комплексы
     */
    var estates: EstateRecords? = null
        @XmlElement(name = "unified_real_estate_complex_records") set

    /**
     * Объекты незавершенного строительства
     */
    var underConstructs: UnderConstructionRecords? = null
        @XmlElement(name = "object_under_construction_records") set

    /**
     * Здания
     */
    var builds: BuildRecords? = null
        @XmlElement(name = "build_records") set

    /**
     * Сооружения
     */
    var constructs: ConstructionRecords? = null
        @XmlElement(name = "construction_records") set

    override fun toString() = listOfNotNull(
        records, rooms, carRooms, properties, estates,
        underConstructs, builds, constructs
    ).joinToString("\n")

}
