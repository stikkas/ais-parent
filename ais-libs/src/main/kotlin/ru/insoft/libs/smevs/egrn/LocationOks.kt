package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Местоположение ОКС
 */
class LocationOks {

    /**
     * OKATO
     */
    var okato: String? = null
        @XmlElement set

    /**
     * OKTMO
     */
    var oktmo: String? = null
        @XmlElement set

    /**
     * Код региона
     */
    var region: CDDict? = null
        @XmlElement set

    /**
     * Описание местоположения
     */
    var position: String? = null
        @XmlElement(name = "position_description") set

    override fun toString() = listOfNotNull(okato?.let { "ОКАТО: $it" }, oktmo?.let { "ОКТМО: $it" },
        region?.value?.let { "Код региона: $it" }, position?.let { "Описание местоположения: $it" })
        .joinToString("\n")
}
