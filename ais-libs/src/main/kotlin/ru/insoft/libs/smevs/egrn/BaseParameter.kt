package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Основная характеристика
 */
class BaseParameter {
    /**
     * Площадь в кв. метрах
     */
    var area: Double? = null
        @XmlElement set

    /**
     * Площадь застройки в квадратных метрах с округлением до 0,1 квадратного метра
     */
    var builtUpArea: Double? = null
        @XmlElement(name = "built_up_area") set

    /**
     * Протяженность в метрах с округлением до 1 метра
     */
    var extension: Double? = null
        @XmlElement set

    /**
     * Глубина в метрах с округлением до 0,1 метра
     */
    var depth: Double? = null
        @XmlElement set

    /**
     * Глубина залегания в метрах с округлением до 0,1 метра
     */
    var occurenceDepth: Double? = null
        @XmlElement(name = "occurence_depth") set

    /**
     * Объем в кубических метрах с округлением до 1 кубического метра
     */
    var volume: Double? = null
        @XmlElement set

    /**
     * Высота в метрах с округлением до 0,1 метра
     */
    var height: Double? = null
        @XmlElement set

    override fun toString() = listOfNotNull(
        area?.let { "Площадь в кв. метрах: $it" },
        builtUpArea?.let { "Площадь застройки в квадратных метрах с округлением до 0,1 квадратного метра: $it" },
        extension?.let { "Протяженность в метрах с округлением до 1 метра: $it" },
        depth?.let { "Глубина в метрах с округлением до 0,1 метра: $it" },
        occurenceDepth?.let { "Глубина залегания в метрах с округлением до 0,1 метра: $it" },
        volume?.let { "Объем в кубических метрах с округлением до 1 кубического метра: $it" },
        height?.let { "Высота в метрах с округлением до 0,1 метра: $it" }
    ).joinToString("\n")
}
