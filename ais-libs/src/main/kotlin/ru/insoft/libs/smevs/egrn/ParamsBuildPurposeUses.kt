package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Характеристики здания (площадь, назначение, разрешенное использование)
 */
class ParamsBuildPurposeUses {
    /**
     * Площадь, в кв. метрах
     */
    var area: Double? = null
        @XmlElement set

    /**
     * Назначение здания
     */
    var purpose: CDDict? = null
        @XmlElement set

    /**
     * Вид(ы) разрешенного использования
     */
    var uses: PermittedUses? = null
        @XmlElement(name = "permitted_uses") set

    override fun toString() = listOfNotNull(
        area?.let { "Площадь: $it" },
        purpose?.value?.let { "Назначение здания: $it" },
        uses?.let { "Вид разрешенного использования: $it" }
    ).joinToString("\n")
}
