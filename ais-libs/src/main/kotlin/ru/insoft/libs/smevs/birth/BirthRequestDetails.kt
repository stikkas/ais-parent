package ru.insoft.libs.smevs.birth

import java.util.*
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement

@XmlAccessorType(XmlAccessType.FIELD)
class BirthRequestDetails {
    /**
     * Идентификатор документа
     */
    @XmlAttribute(name = "ИдДок", required = true)
    val id = UUID.randomUUID()

    /**
     * Сведения о физическом лице, в отношении которого сформирован запрос
     */
    @XmlElement(name = "СведФЛ")
    var fizInfo: ManDetails? = null

    /**
     * Признак отсутствия сведений о субъекте Российской Федерации, где зарегистрирован акт гражданского состояния
     */
    @XmlElement(name = "ПрРегионРегАГС")
    val regAgs: Int = 1
}
