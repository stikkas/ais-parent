package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Помещение
 */
class RoomRecord {
    /**
     * Общие сведения об объекте недвижимости
     */
    var obj: ObjectType? = null
        @XmlElement(name = "object") set

    /**
     * Характеристики помещения
     */
    var params: ParamsRoomBaseWithoutName? = null
        @XmlElement set

    /**
     * Адрес (местоположение) помещения
     */
    var address: AddressLocationRoom? = null
        @XmlElement(name = "address_room") set

    /**
     * Сведения о праве
     */
    var rightRecord: RightRecordIndivid? = null
        @XmlElement(name = "right_record") set

    /**
     * Сведения об ограничениях прав и обременениях объекта недвижимости
     */
    var restrictRecord: RestrictRecordsIndivid? = null
        @XmlElement(name = "restrict_records") set

    override fun toString() = listOfNotNull(obj, params, address, rightRecord, restrictRecord)
        .joinToString("\n")
}
