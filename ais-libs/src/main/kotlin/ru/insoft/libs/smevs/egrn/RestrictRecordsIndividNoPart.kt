package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения об ограничениях прав и обременениях объекта недвижимости
 */
class RestrictRecordsIndividNoPart {
    /**
     * Ограничение прав и обременение объекта недвижимости
     */
    var record: List<RestrictRecordIndividNoPart>? = null
        @XmlElement(name = "restrict_record") set

    override fun toString() = record?.mapNotNull { it.data }?.joinToString("\n") ?: ""
}
