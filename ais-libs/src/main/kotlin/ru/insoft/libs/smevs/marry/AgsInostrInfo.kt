package ru.insoft.libs.smevs.marry

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения о внесении в ЕГР ЗАГС сведений о документе компетентного органа иностранного государства (Раздел 2)
 */
class AgsInostrInfo {
    /**
     * Сведения об уведомлении о регистрации компетентным органом иностранного государства акта гражданского состояния
     */
    var reqInfo: RegInfoFremd? = null
        @XmlElement(name = "СвУведомл") set

    /**
     * Сведения о документе компетентного органа иностранного государства
     */
    var docInfo: DocInfoFremd? = null
        @XmlElement(name = "СвДокИн") set

    override fun toString() = docInfo?.toString() ?: ""
}

