@XmlSchema(
		elementFormDefault = XmlNsForm.UNQUALIFIED,
		namespace = "http://rosreestr.ru/services/v0.12/TRequest",
		xmlns = {
				@XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
				@XmlNs(prefix = "ns", namespaceURI = "http://rosreestr.ru/services/v0.12/TRequest")
		}
)
package ru.insoft.libs.smevs.opis;


import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
