package ru.insoft.libs.smevs.birth

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения о родах, в результате которых родился ребенок
 */
class RodyInfo {

    /**
     * Число родившихся детей при родах
     */
    var childCount: Int? = null
        @XmlElement(name = "ЧислоРодДетей") set

    /**
     * Которым по счету был рожден ребенок
     */
    var file: Int? = null
        @XmlElement(name = "Счет") set

    override fun toString() = """Сведения о родах, в результате которых родился ребенок: 
        |Число родившихся детей при родах: ${childCount ?: "Неизвестно"}
        |Которым по счету был рожден ребенок: ${file ?: "Неизвестно"}
    |""".trimMargin()
}
