package ru.insoft.libs.smevs.opis

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

@XmlAccessorType(XmlAccessType.FIELD)
class StatementFile {
    var fileName: String? = null
}
