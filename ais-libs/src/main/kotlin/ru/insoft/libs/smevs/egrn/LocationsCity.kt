package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class LocationsCity {
    /**
     * Наименование субъекта Российской Федерации, муниципального образования, населенного пункта (при наличии)
     */
    var location: List<LocationCity>? = null
        @XmlElement set

    override fun toString() = location?.joinToString("\n") ?: ""
}
