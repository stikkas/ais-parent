package ru.insoft.libs.smevs.divorce

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

/**
 * Ответ на запрос, сформированный участником взаимодействия СМЭВ
 * на предоставление сведений об актах гражданского состояния о расторжении брака,
 * содержащихся в Едином государственном реестре записей актов гражданского состояния
 */
@XmlRootElement(name = "BRAKRASTINFResponse")
class DivorceResponse {
    /**
     * Сведения ответа на запрос о государственной регистрации АГС о расторжении брака
     */
    var data: List<ResAgsDetails>? = null
        @XmlElement(name = "СведОтветАГС") set

    override fun toString() = data?.firstOrNull()?.toString() ?: "Нет данных"
}
