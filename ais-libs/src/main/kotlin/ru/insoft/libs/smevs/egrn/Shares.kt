package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Размер доли в праве
 */
class Shares {
    /**
     * Размер доли в праве
     */
    var share: Share? = null
        @XmlElement set

    /**
     * Размер доли в праве в балло-гектарах
     */
    var shareBal: ShareBalHectare? = null
        @XmlElement(name = "share_bal_hectare") set

    /**
     * Размер доли в праве в гектарах
     */
    var shareHect: ShareHectare? = null
        @XmlElement(name = "share_hectare") set

    /**
     * Не указан размер доли в праве общей долевой
     * собственности на общее имущество, в том числе на земельный участок,
     * собственников помещений, машино-мест в здании,
     * если объектом недвижимости является помещение, машино-место в здании
     */
    var shareUnknown: ShareUnknown? = null
        @XmlElement(name = "share_unknown") set

    /**
     * Размер доли в праве общей долевой собственности на общее имущество собственников комнат в жилом помещении
     */
    var roomShare: RoomOwnersShare? = null
        @XmlElement(name = "room_owners_share") set

    /**
     * Доля в праве общей долевой собственности пропорциональна размеру общей площади помещений, машино-мест,
     * не переданных участникам долевого строительства
     */
    var builderShare: BuilderShare? = null
        @XmlElement(name = "builder_share") set

    /**
     * Доля в праве общей долевой собственности пропорциональна размеру общей
     * площади помещений, машино-мест, не переданных участникам долевого строительства, а также помещений, машино-мест
     */
    var builderShareWObject: BuilderShareWithObject? = null
        @XmlElement(name = "builder_share_with_object") set

    override fun toString() =
        (share ?: shareBal ?: shareHect ?: shareUnknown ?: roomShare ?: builderShare ?: builderShareWObject)?.toString()
            ?: ""
}
