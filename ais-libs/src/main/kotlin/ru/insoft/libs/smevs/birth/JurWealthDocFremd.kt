package ru.insoft.libs.smevs.birth

import javax.xml.bind.annotation.XmlAttribute

/**
 * Сведения о юридической значимости документа, выданного компетентным органом иностранного государства, и его легализации
 */
class JurWealthDocFremd {
    /**
     * Сведения о необходимости легализации документа, выданного компетентным органом иностранного государства
     */
    var legalNeeded: Int? = null
        @XmlAttribute(name = "СвЛегал") set

    fun legalizationRequired() = when (legalNeeded) {
        1 -> "документ легализован"
        2 -> "получен отказ в легализации документа"
        3 -> "проставлен апостиль"
        4 -> "легализация документа не требуется"
        else -> "неизвестно"
    }
}
