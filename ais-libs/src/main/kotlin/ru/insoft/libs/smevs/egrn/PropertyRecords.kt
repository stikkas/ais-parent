package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Предприятия как имущественные комплексы
 */
class PropertyRecords {
    var records: List<PropertyComplexRecord>? = null
        @XmlElement(name = "property_complex_record") set

    override fun toString() = records?.joinToString("\n") ?: ""
}
