package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class PartNumbers {
    var numbers: List<PartNumber>? = null
        @XmlElement(name = "part_number") set

    override fun toString() = numbers?.mapNotNull { it.number }?.joinToString() ?: ""
}
