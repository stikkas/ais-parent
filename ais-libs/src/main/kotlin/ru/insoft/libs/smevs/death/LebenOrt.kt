package ru.insoft.libs.smevs.death

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

@XmlType(namespace = "urn://x-artefacts-zags-fatalinf/types/4.0.1")
@XmlAccessorType(XmlAccessType.FIELD)
class LebenOrt {

    // Что-то одно должно быть

    /**
     * Адрес места жительства на территории Российской Федерации, введенный по ФИАС
     */
    @XmlElement(name = "АдрРФФИАС")
    var fias: AddressRF? = null

    /**
     * Адрес места жительства на территории Российской Федерации с указанием адреса из ФИАС, максимально соответствующего указанным в документе сведениям
     */
    @XmlElement(name = "АдрРФНеФИАС")
    var noFias: AddressRF? = null

    @XmlElement(name = "АдрТекст")
    var text: String? = null

    fun addrOrNull() = (fias ?: noFias ?: text)?.toString()?.let { it.ifEmpty { null } }
}
