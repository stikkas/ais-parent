package ru.insoft.libs.smevs.sud

import ru.insoft.libs.smevs.TimeType
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Наказание
 */
@XmlAccessorType(XmlAccessType.FIELD)
class Judgement {
    /**
     * Вид Лишение/Арест/Ограничение/Работы
     */
    var type: String? = null

    /**
     * Штраф
     */
    var fine: Int? = null

    /**
     * Временные показатели
     */
    var limit: TimeType? = null

    override fun toString(): String {
        val res = ArrayList<String>()
        type?.let { res.add(it) }
        fine?.let { res.add(it.toString()) }
        limit?.let { res.add(it.toString()) }
        return res.filter { it.isNotBlank() }.joinToString()
    }
}
