package ru.insoft.libs.smevs.birth

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о родившемся
 */
class BirthedInfo {
    /**
     * Живорожденный, мертворожденный
     */
    var alive: Int? = null
        @XmlElement(name = "ЖивМертв") set

    /**
     * Пол
     */
    var sex: Int? = null
        @XmlElement(name = "Пол") set

    /**
     * Фамилия, имя, отчество рожденного ребенка
     */
    var fio: Fio? = null
        @XmlElement(name = "ФИОРожд") set

    /**
     * Дата рождения (календарная дата)
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаРождКаленд") set

    /**
     * Дата рождения по представленным документам (неполная дата)
     */
    var docDate: PartialDate? = null
        @XmlElement(name = "ДатаРождДок") set

    /**
     * Место рождения
     */
    var place: Place? = null
        @XmlElement(name = "МестоРожден") set

    var snils: String? = null
        @XmlAttribute(name = "СНИЛС") set

    override fun toString() = "Сведения о родившемся: " +
            listOfNotNull(
                if (alive == 1) "мертворожденный" else if (alive == 0) "живорожденный" else null,
                if (sex == 1) "Пол мужской" else if (sex == 2) "Пол женский" else null,
                fio?.toString(),
                (date ?: docDate?.toLocalDate())?.let { "Дата рождения ${it.format()}" },
                place?.text?.let { "Место рождения: $it" }
            ).joinToString()
}
