package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class CarRooms {
    var record: List<CarParkingSpaceRecord>? = null
        @XmlElement(name = "car_parking_space_record") set

    override fun toString() = record?.joinToString("\n") ?: ""
}
