package ru.insoft.libs.smevs.divorce

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о документе, удостоверяющем личность физического лица (серия и номер раздельно)
 */
@XmlAccessorType(XmlAccessType.FIELD)
class PersonalDoc {
    /**
     * Код вида документа, удостоверяющего личность
     */
    @XmlAttribute(name = "КодВидДок", required = true)
    var taxCode: String? = null

    val name: String
        get() = when (taxCode?.trim()?.toIntOrNull()) {
            1 -> "ПАСПОРТ СССР"
            3 -> "СВИДЕТЕЛЬСТВО О РОЖДЕНИИ"
            4 -> "УДОСТОВЕРЕНИЕ ЛИЧНОСТИ ОФИЦЕРА"
            5 -> "СПРАВКА"
            6 -> "ПАСПОРТ МИНМОРФЛОТА"
            7 -> "ВОЕННЫЙ БИЛЕТ"
            10 -> "ИНОСТРАННЫЙ ПАСПОРТ"
            12 -> "ВИД НА ЖИТЕЛЬСТВО"
            14 -> "ВРЕМЕННОЕ УДОСТОВЕРЕНИЕ ЛИЧНОСТИ"
            15 -> "РАЗРЕШЕНИЕ НА ВРЕМЕННОЕ ПРОЖИВАНИЕ"
            21 -> "ПАСПОРТ РФ"
            22 -> "ЗАГРАНИЧНЫЙ ПАСПОРТ"
            23 -> "ИНОСТРАННОЕ СВИДЕТЕЛЬСТВО О РОЖДЕНИИ"
            24 -> "УДОСТОВЕРЕНИЕ ЛИЧНОСТИ"
            else -> "Неизвестный"
        }

    @XmlAttribute(name = "СерДок")
    var series: String? = null

    @XmlAttribute(name = "НомДок", required = true)
    var number: String? = null

    @XmlAttribute(name = "ДатаДок")
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var date: LocalDate? = null

    /**
     * Наименование органа, выдавшего документ, удостоверяющий личность
     */
    @XmlAttribute(name = "ВыдДок")
    var whogive: String? = null

    /**
     * Код подразделения органа, выдавшего документ, удостоверяющий личность
     */
    @XmlAttribute(name = "КодВыдДок")
    var whoCode: String? = null

    override fun toString() =  listOfNotNull(name, series, number, whogive, date?.format()?.let { "от $it" }).joinToString(" ")
}
