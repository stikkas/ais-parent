package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Площадь и погрешность вычисления
 */
class LandArea {
    /**
     * Значение в кв. метрах
     */
    var value: Double? = null
        @XmlElement set
}
