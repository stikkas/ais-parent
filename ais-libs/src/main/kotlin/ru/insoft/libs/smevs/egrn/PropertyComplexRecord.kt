package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Предприятие как имущественный комплекс
 */
class PropertyComplexRecord {
    /**
     * Общие сведения об объекте недвижимости
     */
    var obj: ObjectType? = null
        @XmlElement(name = "object") set

    /**
     * Характеристики объекта недвижимости (назначение)
     */
    var params: ParamsPurpose? = null
        @XmlElement set

    /**
     * Местоположение предприятия, как имущественного комплекса (адрес (местонахожение) правообладателя)
     */
    var address: AddressLocationPropertyComplex? = null
        @XmlElement set

    /**
     * Сведения о праве
     */
    var rightRecord: RightRecordIndivid? = null
        @XmlElement(name = "right_record") set

    /**
     * Сведения об ограничениях прав и обременениях объекта недвижимости
     */
    var restrictRecords: RestrictRecordsIndividNoPart? = null
        @XmlElement(name = "restrict_records") set

    override fun toString() = listOfNotNull(obj, params, address, rightRecord, restrictRecords).joinToString("\n")
}
