package ru.insoft.libs.smevs.death

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения о документе компетентного органа иностранного государства
 */
class DocInfoFremd {

    var countryCode: String? = null
        @XmlElement(name = "КодОКСМ") set

    var country: String? = null
        @XmlElement(name = "НаимСтраны") set

    /**
     * Реквизиты документа, выданного компетентным органом иностранного государства
     */
    var reqDoc: DocRequisiteFremd? = null
        @XmlElement(name = "РеквизИнДок") set

    /**
     * Сведения о юридической значимости документа, выданного компетентным органом иностранного государства, и его легализации
     */
    var jurWealth: JurWealthDocFremd? = null
        @XmlElement(name = "ЮрЗначДок") set

    override fun toString() =
        """Сведения о документе компетентного органа иностранного государства: ${
            listOfNotNull(country, reqDoc).joinToString()
        }
        |Сведения о необходимости легализации документа, выданного компетентным органом иностранного государства: ${jurWealth?.legalizationRequired() ?: ""}
    |""".trimMargin()
}
