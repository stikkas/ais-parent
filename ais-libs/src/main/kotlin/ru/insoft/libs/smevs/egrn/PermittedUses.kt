package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class PermittedUses {
    /**
     * Вид разрешенного использования
     */
    var use: List<PermittedUse>? = null
        @XmlElement(name = "permitted_use") set

    override fun toString() = use?.mapNotNull { it.name }?.joinToString() ?: ""
}
