package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class ParamsConstructionPurpose {
    /**
     * Основные характеристики
     */
    var params: BaseParameters? = null
        @XmlElement(name = "base_parameters") set

    /**
     * Назначение
     */
    var purpose: String? = null
        @XmlElement set

    override fun toString() = listOfNotNull(params, purpose?.let { "Проектируемое назначение: $it" }).joinToString("\n")
}
