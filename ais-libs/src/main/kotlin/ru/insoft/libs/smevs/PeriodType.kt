package ru.insoft.libs.smevs

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Период
 */
@XmlAccessorType(XmlAccessType.FIELD)
class PeriodType {

    /**
     * Годы
     */
    var years: Int? = null

    /**
     * Месяцы
     */
    var months: Int? = null

    /**
     * Дни
     */
    var days: Int? = null

    /**
     * Часы
     */
    var hours: Int? = null

    override fun toString(): String {
        var res = ArrayList<String>()
        years?.let { res.add("годы: $it") }
        months?.let { res.add("месяцы: $it") }
        days?.let { res.add("дни: $it") }
        hours?.let { res.add("часы: $it") }
        return res.joinToString()
    }
}
