package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Адрес (местоположение) земельного участка
 */
class AddressLocationLand {
    var type: CDDict? = null
        @XmlElement(name = "address_type") set

    var address: AddressMain? = null
        @XmlElement set

    /**
     * Местоположение относительно ориентира
     */
    var position: LocationByARefPoint? = null
        @XmlElement(name = "rel_position") set

    override fun toString() = listOfNotNull(
        type?.value?.let { "Тип адреса: $it" },
        address?.toString(),
        position?.toString()
    ).filter { it.isNotEmpty() }.joinToString("\n")
}
