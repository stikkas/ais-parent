package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Детали заявления на предоставление сведений
 */
@XmlAccessorType(XmlAccessType.FIELD)
class TEGRNRequestDetails {
    /**
     * Запрос на предоставление сведений об объектах недвижимости и (или) правообладателях, о зонах и территориях
     */
    var requestEGRNDataAction: TRequestDataAction? = null
}
