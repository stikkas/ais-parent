package ru.insoft.libs.smevs.adapters

import java.time.LocalDate
import javax.xml.bind.annotation.adapters.XmlAdapter

class LocalDateAdapter : XmlAdapter<String, LocalDate>() {
    override fun marshal(v: LocalDate?) = v?.let { it.toString() }

    override fun unmarshal(v: String?) = v?.let {
        LocalDate.parse(v)
    }
}
