package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Номера регистрации вновь возникшего права
 */
class NewAroseRightNumbers {
    var number: List<RightRecordNumber>? = null
        @XmlElement(name = "new_arose_right_number") set

    override fun toString() = number?.map {
        listOfNotNull(
            "Номер регистрации вновь возникшего права: ",
            it.number?.let { "Номер реестровой записи: $it" },
            it.rightNumber?.let { "Номер регистрации вещного права: $it" }).joinToString("\n")
    }?.filter { it.isNotEmpty() }?.joinToString("\n") ?: ""
}
