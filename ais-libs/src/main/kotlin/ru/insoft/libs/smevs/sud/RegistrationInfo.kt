package ru.insoft.libs.smevs.sud

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

@XmlAccessorType(XmlAccessType.FIELD)
class RegistrationInfo {
    /**
     * Тип регистрации
     */
    var type: String? = null

    /**
     * Место регистрации (код региона)
     */
    var regionCode: String? = null

    /**
     * Место регистрации
     */
    var place: String? = null
}
