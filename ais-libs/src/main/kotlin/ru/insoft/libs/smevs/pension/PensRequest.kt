package ru.insoft.libs.smevs.pension

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

@XmlRootElement(name = "ReceiptPensionFactRequest")
@XmlAccessorType(XmlAccessType.FIELD)
class PensRequest {

    @XmlElement(name = "FamilyName", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    lateinit var lastName: String

    @XmlElement(name = "FirstName", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    lateinit var firstName: String

    @XmlElement(name = "Patronymic", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    var middleName: String? = null

    @XmlElement(name = "Snils")
    var snils: String? = null
        set(value) {
            field = value?.replace("-", "")
        }


    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    @XmlElement(name = "BirthDate")
    lateinit var birthDate: LocalDate

    override fun toString() = """ФИО: ${listOfNotNull(lastName, firstName, middleName).joinToString(" ")}
        |СНИЛС: ${snils ?: ""}
        |Дата рождения: ${birthDate.format()}
        |""".trimMargin()
}
