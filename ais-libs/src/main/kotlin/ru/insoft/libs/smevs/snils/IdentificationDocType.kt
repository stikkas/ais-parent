package ru.insoft.libs.smevs.snils

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

@XmlType(namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2")
@XmlAccessorType(XmlAccessType.FIELD)
class IdentificationDocType {
    @XmlElement(name = "Type", nillable = false)
    var type: String? = null

    @XmlElement(name = "Document", nillable = false)
    var document: DocType? = null

    override fun toString() = listOfNotNull(type, document).joinToString()
}
