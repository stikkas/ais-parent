package ru.insoft.libs.smevs.divorce

import ru.insoft.libs.format
import javax.xml.bind.annotation.*

/**
 * Запрос на предоставление сведений об актах гражданского состояния о расторжении брака,
 * содержащихся в Едином государственном реестре записей актов гражданского состояния
 */
@XmlRootElement(name = "BRAKRASTINFRequest")
@XmlAccessorType(XmlAccessType.FIELD)
class DivorceRequest {
    /**
     * Количество документов в представленном файле
     */
    @XmlAttribute(required = true, name = "КолДок")
    val docsCount: Int = 1

    /**
     * Тип акта гражданского состояния, в отношении которого сформирован запрос
     */
    @XmlAttribute(required = true, name = "ТипАГС")
    val actType: String = "03"

    /**
     * Идентификатор запроса, сформированный запрашивающей стороной
     */
    @XmlAttribute(required = true, name = "ИдЗапрос")
    var id: String? = null

    /**
     * Сведения о нормативно-правовых основаниях запрашивающей стороны
     * для получения сведений из ЕГР ЗАГС об актах гражданского состояния о расторжении брака
     */
    @XmlElement(name = "СведОсн")
    val mainInfo = NormGrand()

    /**
     * Сведения, содержащиеся в запросе
     */
    @XmlElement(name = "СведЗапрос")
    var details: RequestDetails? = null

    override fun toString() = """ФИО: ${details?.fizInfo?.fio ?: ""}
        |Дата рождения: ${details?.fizInfo?.birthDate?.format() ?: ""}${details?.fizInfo?.snils?.let { "\n|СНИЛС: $it" } ?: ""}
        |${
        details?.agsInfo?.let { a ->
            listOfNotNull(a.nomer?.let { "Номер записи акта гражданского состояния: $it" },
                a.date?.format()?.let { "Дата записи акта гражданского состояния: $it" },
                a.organ?.let { "Орган ЗАГС: $it" }
            ).joinToString("\n|")
        } ?: ""
    }
        |""".trimMargin()
//    ${details?.fizInfo?.ausweis?.let { "\n|Документ, удостоверяющий личность: $it" } ?: ""}
}
