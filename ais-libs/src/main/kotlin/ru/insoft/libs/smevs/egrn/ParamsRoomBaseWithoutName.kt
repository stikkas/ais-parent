package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Характеристики помещения (за исключением наименования)
 */
class ParamsRoomBaseWithoutName {
    /**
     * Площадь, в кв. метрах
     */
    var area: Double? = null
        @XmlElement set

    /**
     * Вид(ы) разрешенного использования помещения
     */
    var use: PermittedUses? = null
        @XmlElement(name = "permitted_uses") set

    /**
     * Назначение помещения
     */
    var purpose: CDDict? = null
        @XmlElement set

    /**
     * Вид жилого помещения
     */
    var type: CDDict? = null
        @XmlElement set

    /**
     * Общее имущество собственников помещений в многоквартирном доме
     */
    var property: Boolean? = null
        @XmlElement(name = "common_property") set

    /**
     * Имущество общего пользования
     */
    var service: Boolean? = null
        @XmlElement set

    /**
     * Вид жилого помещения специализированного жилищного фонда
     */
    var specType: CDDict? = null
        @XmlElement(name = "special_type") set

    override fun toString() = listOfNotNull(
        area?.let { "Площадь: $it" },
        use?.let { if (it.toString().isNotEmpty()) "Вид(ы) разрешенного использования помещения: $it" else null },
        purpose?.value?.let { "Назначение помещения: $it" },
        type?.value?.let { "Вид жилого помещения: $it" },
        property?.let { "Общее имущество собственников помещений: ${if (it) "Да" else "Нет"}" },
        service?.let { "Имущество общего пользования: ${if (it) "Да" else "Нет"}" },
        specType?.value?.let { "Вид жилого помещения специализированного жилищного фонда: $it" }
    ).joinToString("\n")
}
