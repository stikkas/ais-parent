package ru.insoft.libs.smevs.birth

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlTransient

/**
 * Сведения об органе, которым произведена государственная регистрация акта гражданского состояния
 */
@XmlAccessorType(XmlAccessType.FIELD)
class BagsType {

    /**
     * zagsdocumentid
     */
    @XmlTransient
    var id: String? = null

    @XmlAttribute(name = "НаимЗАГС", required = true)
    var name: String? = null

    @XmlAttribute(name = "КодЗАГС")
    var code: String? = null

    override fun toString() = when (val res = listOfNotNull(name, code).joinToString()) {
        "" -> id ?: ""
        else -> res
    }
}
