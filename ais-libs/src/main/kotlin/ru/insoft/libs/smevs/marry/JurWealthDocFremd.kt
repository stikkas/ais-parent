package ru.insoft.libs.smevs.marry

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement

/**
 * Сведения о юридической значимости документа, выданного компетентным органом иностранного государства, и его легализации
 */
class JurWealthDocFremd {
    /**
     * Сведения о лице, подписавшем документ иностранного государства, текст
     */
    var podpisant: String? = null
        @XmlElement(name = "СвПодпТекст") set

    /**
     * Сведения о нотариальном засвидетельствовании документа
     */
    var document: SvidetelType? = null
        @XmlElement(name = "ЗасвидДок") set

    /**
     * Сведения о нотариальном засвидетельствовании перевода документа или подлинности подписи переводчика
     */
    var ubersatz: SvidetelType? = null
        @XmlElement(name = "ЗасвидПеревод") set

    /**
     * Сведения о легализации документа о проставлении апостиля
     */
    var legalization: DocLegalisation? = null
        @XmlElement(name = "Легализация") set

    /**
     * Количество листов в копии документа, выданного компетентным органом иностранного государства
     */
    var pageCount: Int? = null
        @XmlAttribute(name = "КолЛистДок") set

    /**
     * Сведения о наличии или отсутствии оттиска печати на документе иностранного государства
     */
    var ottisk: Int? = null
        @XmlAttribute(name = "СвОттискПеч") set

    /**
     * Сведения о необходимости легализации документа, выданного компетентным органом иностранного государства
     */
    var legalNeeded: Int? = null
        @XmlAttribute(name = "СвЛегал") set

    fun legalizationRequired() = when (legalNeeded) {
        1 -> "документ легализован"
        2 -> "получен отказ в легализации документа"
        3 -> "проставлен апостиль"
        4 -> "легализация документа не требуется"
        else -> "неизвестно"
    }
}
