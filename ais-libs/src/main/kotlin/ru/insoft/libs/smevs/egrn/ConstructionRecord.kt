package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Сооружение
 */
class ConstructionRecord {
    /**
     * Общие сведения об объекте недвижимости
     */
    var obj: ObjectType? = null
        @XmlElement(name = "object") set

    /**
     * Характеристики сооружения
     */
    var params: ParamsConstructionPurposeUses? = null
        @XmlElement set

    /**
     * Адрес (местоположение)
     */
    var address: AddressLocationConstruction? = null
        @XmlElement(name = "address_location") set

    /**
     * Сведения о праве
     */
    var rightRecord: RightRecordIndivid? = null
        @XmlElement(name = "right_record") set

    /**
     * Сведения об ограничениях прав и обременениях объекта недвижимости
     */
    var restrictRecords: RestrictRecordsIndivid? = null
        @XmlElement(name = "restrict_records") set

    override fun toString() = listOfNotNull(obj, params, address, rightRecord, restrictRecords).joinToString("\n")
}
