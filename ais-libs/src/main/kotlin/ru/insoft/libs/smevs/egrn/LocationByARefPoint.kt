package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Местоположение земельного участка относительно ориентира
 */
class LocationByARefPoint {

    /**
     * Наименование ориентира
     */
    var name: String? = null
        @XmlElement(name = "ref_point_name") set

    /**
     * Расположение относительно ориентира
     */
    var description: String? = null
        @XmlElement(name = "location_description") set

    override fun toString() = listOfNotNull(name?.let { "Наименование ориентира: $it" },
        description?.let { "Расположение относительно ориентира: $it" }
    ).joinToString("\n")
}
