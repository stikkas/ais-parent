package ru.insoft.libs.smevs.divorce

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlElements
import javax.xml.bind.annotation.XmlType

@XmlType(namespace = "urn://x-artefacts-zags-brakrastinf/types/4.0.0", propOrder = ["lastName", "firstName", "middleName"])
class Fio {
    var lastName: String? = null
        @XmlElement(name = "Фамилия") get

    var firstName: String? = null
        @XmlElement(name = "Имя") get

    var middleName: Any? = null
        @XmlElements(
            value = [XmlElement(name = "Отчество", type = String::class),
                XmlElement(name = "ПрОтчество", type = Int::class)]
        ) get() = if (field?.toString()?.isNotBlank() == true) (field as String) else 1

    override fun toString() = "$lastName $firstName ${if (middleName?.toString() == "1") "" else (middleName ?: "")}"
}
