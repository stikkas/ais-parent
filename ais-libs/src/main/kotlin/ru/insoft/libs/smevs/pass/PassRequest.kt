package ru.insoft.libs.smevs.pass

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter


@XmlRootElement(name = "passportValidityRequest")
@XmlType(propOrder = ["series", "number", "issueDate"])
@XmlAccessorType(XmlAccessType.FIELD)
class PassRequest {
    var series: String? = null

    var number: String? = null

    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var issueDate: LocalDate? = null

    override fun toString() = """Серия: ${series ?: ""}
        |Номер: ${number ?: ""}
        |Дата выдачи: ${issueDate?.format() ?: ""}
        |""".trimMargin()
}
