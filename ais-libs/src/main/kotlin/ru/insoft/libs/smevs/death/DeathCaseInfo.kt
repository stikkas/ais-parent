package ru.insoft.libs.smevs.death

import javax.xml.bind.annotation.XmlAttribute

/**
 * Сведения о причине смерти (не справочные значения)
 */
class DeathCaseInfo {
    /**
     * Наименование причины смерти (не по справочнику)
     */
    var name: String? = null
        @XmlAttribute(name = "ПричСмертПроизв") set
}
