package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Установленное разрешенное использование
 */
class PermittedUseEstablished {
    var byDoc: String? = null
        @XmlElement(name = "by_document") set

    var use: CDDict? = null
        @XmlElement(name = "land_use") set

    var mer: CDDict? = null
        @XmlElement(name = "land_use_mer") set
}
