package ru.insoft.libs.smevs.snils

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.*
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

@XmlRootElement(name = "SnilsByAdditionalDataRequest")
@XmlAccessorType(XmlAccessType.FIELD)
class SnilsRequest {
    /**
     * ФИО физического лица.
     */
    @XmlElement(name = "FamilyName", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2")
    var familyName: String? = null

    @XmlElement(name = "FirstName", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2")
    var firstName: String? = null

    @XmlElement(name = "Patronymic", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2")
    var patronymic: String? = null

    @XmlElement(name = "BirthDate", nillable = false)
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var birthDate: LocalDate? = null

    @XmlTransient
    var sex: Int? = null
        set(value) {
            value?.let {
                if (it == 1) {
                    gender = "Female"
                } else if (it == 0) {
                    gender = "Male"
                }
            }
        }

    @XmlElement(name = "Gender", nillable = false)
    var gender: String? = null
        private set

    @XmlElement(name = "BirthPlace")
    var birthPlace: BirthPlaceType? = null

    @XmlElements(
        value = [XmlElement(
            name = "PassportRF",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = PassportRF::class
        ), XmlElement(
            name = "ForeignPassport",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = ForeignPassport::class
        ), XmlElement(
            name = "ResidencePermitRF",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = ResidencePermitRF::class
        ), XmlElement(
            name = "InternationalPassportRF",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = InternationalPassportRF::class
        ), XmlElement(
            name = "MilitaryPassport",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = MilitaryPassport::class
        ), XmlElement(
            name = "SailorPassport",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = SailorPassport::class
        ), XmlElement(
            name = "SovietPassport",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = SovietPassport::class
        ), XmlElement(
            name = "BirthCertificate",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = BirthCertificate::class
        ), XmlElement(
            name = "DrivingLicenseRF",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = DrivingLicenseRF::class
        ), XmlElement(
            name = "PfrIdentificationDocument",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = IdentificationDocType::class
        ), XmlElement(
            name = "ReleaseCertificate",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = ReleaseCertificate::class
        ), XmlElement(
            name = "PassportLossCertificate",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = PassportLossCertificate::class
        ), XmlElement(
            name = "Form9Certificate",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = Form9Certificate::class
        ), XmlElement(
            name = "TemporaryIdentityCardRF",
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
            type = TemporaryIdentityCardRF::class
        )]
    )
    var identity: Any? = null

    override fun toString() = """Дата рождения: ${birthDate?.format() ?: ""}
        |Адрес места рождения: ${birthPlace ?: ""}
        |Документ, удостоверяющий личность: ${identity ?: ""}
        |""".trimMargin()

}
