package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class PartNumber {

    var number: Int? = null
        @XmlElement(name = "part_number") set
}
