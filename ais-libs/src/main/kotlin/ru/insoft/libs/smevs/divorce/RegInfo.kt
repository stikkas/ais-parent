package ru.insoft.libs.smevs.divorce

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

class RegInfo {

    /**
     * Дата прекращения брака
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаПрекрБрак") set

    /**
     * Дата прекращения брака по представленным документам (неполная дата)
     */
    var docDate: PartialDate? = null
        @XmlElement(name = "ДатаПрекрБракДок") set

    var husband: Spouse? = null
        @XmlElement(name = "Супруг") set

    var wife: Spouse? = null
        @XmlElement(name = "Супруга") set

    override fun toString() = """Дата прекращения брака: ${(date ?: docDate?.toLocalDate())?.format() ?: ""}
        |Супруг: ${husband ?: ""}
        |Супруга: ${wife ?: ""}
    |""".trimMargin()
}
