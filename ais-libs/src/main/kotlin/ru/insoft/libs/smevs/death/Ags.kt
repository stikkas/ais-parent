package ru.insoft.libs.smevs.death

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о государственной регистрации АГС о смерти, в отношении которого сформирован запрос
 */
@XmlAccessorType(XmlAccessType.FIELD)
class Ags {
    /**
     * Дата составления актовой записи  гражданского состояния (календарная дата)
     */
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    @XmlElement(name = "ДатаЗапис")
    var date: LocalDate? = null

    @XmlElement(name = "ОрганЗАГС")
    var organ: DagsType? = null

    @XmlAttribute(name = "НомерЗапис")
    var nomer: String? = null
}
