package ru.insoft.libs.smevs.snils

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "SnilsByAdditionalDataResponse")
@XmlAccessorType(XmlAccessType.FIELD)
class SnilsResponse {

    @XmlElement(
        name = "Snils",
        namespace = "http://kvs.pfr.com/snils-by-additionalData/1.0.2",
        nillable = false
    )
    var snils: String? = null

    override fun toString() = "СНИЛС: ${snils ?: ""}"
}
