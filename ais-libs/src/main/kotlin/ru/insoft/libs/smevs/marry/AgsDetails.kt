package ru.insoft.libs.smevs.marry

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о государственной регистрации АГС о заключении брака, в отношении которого сформирован запрос
 */
@XmlAccessorType(XmlAccessType.FIELD)
class AgsDetails {

    @XmlElement(name = "ДатаЗапис")
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var date: LocalDate? = null

    /**
     * Сведения об органе ЗАГС, которым произведена государственная регистрация акта гражданского состояния
     */
    @XmlElement(name = "ОрганЗАГС")
    var organ: AgsType? = null

    /**
     * Номер записи акта гражданского состояния
     */
    @XmlAttribute(name = "НомерЗапис", required = true)
    var nomer: String? = null
}
