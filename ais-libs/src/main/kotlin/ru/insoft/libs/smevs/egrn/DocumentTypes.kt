package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

/**
 * Мнемоника типа документа согласно справочника "Документы" ЕС УНСИ
 */
@XmlType(namespace = "http://rosreestr.ru/services/v0.1/commons/Documents")
@XmlAccessorType(XmlAccessType.FIELD)
class DocumentTypes {
    var documentTypeCode: String? = null
}
