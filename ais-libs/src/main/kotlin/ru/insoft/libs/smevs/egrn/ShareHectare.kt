package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Размер доли в праве в гектарах
 */
class ShareHectare {

    /**
     * Гектары
     */
    var hectare: Double? = null
        @XmlElement set

    override fun toString() = hectare?.let { "Размер доли в праве в гектарах: $it" } ?: ""
}
