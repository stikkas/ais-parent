package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Разрешенное использование земельного участка
 */
class AllowedUse {
    /**
     * Установленное разрешенное использование земельного участка
     */
    var use: PermittedUseEstablished? = null
        @XmlElement(name = "permitted_use_established") set

    override fun toString() = use?.let { u ->
        listOfNotNull(
            "Разрешенное использование:",
            u.byDoc?.let { "Разрешенное использование по документу: $it" },
            u.use?.value?.let { "Вид разрешенного использования земельного участка в соответствии с ранее использовавшимся классификатором: $it" },
            u.mer?.value?.let { "Вид разрешенного использования земельного участка в соответствии с классификатором, утвержденным приказом Минэкономразвития России от 01.09.2014 № 540: $it" })
            .joinToString("\n")
    } ?: ""
}
