package ru.insoft.libs.smevs

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Значение классификатора
 */
@XmlAccessorType(XmlAccessType.FIELD)
class ClassifierValue {
    /**
     * Код
     */
    var code: String? = null

    /**
     * Значение
     */
    var value: String? = null

    override fun toString() = listOfNotNull(code, value).joinToString(" - ")
}
