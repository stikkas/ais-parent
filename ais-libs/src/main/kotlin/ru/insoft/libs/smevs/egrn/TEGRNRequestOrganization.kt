package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

/**
 * Описание предыдущего наименования юридического лица в рамках заявления по Предоставленю сведений
 */
@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/commons/Subjects",
    propOrder = ["name", "nativeForeignParams"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class TEGRNRequestOrganization {
    /**
     * Полное наименование
     */
    var name: String? = null

    /**
     * Российское ЮЛ или иностранное ЮЛ
     */
    var nativeForeignParams: TBaseNativeForeignParams? = null
}
