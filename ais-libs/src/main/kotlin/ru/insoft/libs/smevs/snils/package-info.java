@XmlSchema(
		elementFormDefault = XmlNsForm.QUALIFIED,
		namespace = "http://kvs.pfr.com/snils-by-additionalData/1.0.2",
		xmlns = {
				@XmlNs(prefix = "tns", namespaceURI = "http://kvs.pfr.com/snils-by-additionalData/1.0.2"),
				@XmlNs(prefix = "smev", namespaceURI = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2"),
				@XmlNs(prefix = "pfr", namespaceURI = "http://common.kvs.pfr.com/1.0.0")
		}
)
package ru.insoft.libs.smevs.snils;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
