package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Реквизиты выписки
 */
class DetailsStatement {
    /**
     * Группа верхних реквизитов
     */
    var top: TopRequisites? = null
        @XmlElement(name = "group_top_requisites") set

    /**
     * Группа нижних реквизитов
     */
    var lower: LowerRequisites? = null
        @XmlElement(name = "group_lower_requisites") set

}
