package ru.insoft.libs.smevs.death

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения о причинах смерти
 */
class DeathCase {

    /**
     * Сведения о причине смерти (не справочные значения)
     */
    var data: DeathCaseInfo? = null
        @XmlElement(name = "СвПричСмертПроизв") set
}
