package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Документ-основание (все реквизиты)
 */
class UnderlyingDocumentOut {
    /**
     * Код документа
     */
    var code: CDDict? = null
        @XmlElement(name = "document_code") set

    /**
     * Наименование
     */
    var name: String? = null
        @XmlElement(name = "document_name") set

    /**
     * Серия документа
     */
    var series: String? = null
        @XmlElement(name = "document_series") set

    /**
     * Номер документа
     */
    var number: String? = null
        @XmlElement(name = "document_number") set

    /**
     * Дата документа
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "document_date") set

    /**
     * Орган власти, организация, выдавшие документ
     */
    var issuer: String? = null
        @XmlElement(name = "document_issuer") set
}
