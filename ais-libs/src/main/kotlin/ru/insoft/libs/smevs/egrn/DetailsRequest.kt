package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Реквизиты поступившего запроса, период, за который запрашивается
 * информация и данные о правообладателе согласно сведениям, указанным в запросе
 */
class DetailsRequest {
    /**
     * Дата поступившего запроса
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "date_received_request") set

    /**
     * Дата получения запроса органом регистрации прав
     */
    var dateReceipt: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "date_receipt_request_reg_authority_rights") set

    /**
     * Дата, по состоянию на которую или период, за который запрошены сведения
     */
    var period: PeriodStartEndAvailable? = null
        @XmlElement set

    /**
     * Сведения о правообладателе согласно сведениям, указанным в запросе
     */
    var holder: String? = null
        @XmlElement(name = "right_holder") set

    /**
     * Вид и (или) назначение объектов недвижимости, о зарегистрированных правах на которые запрошены сведения
     */
    var objects: ObjectsType? = null
        @XmlElement(name = "objects_type") set

    /**
     * Территории определенного(ых) субъекта(ов) Российской Федерации,  указанные в запросе, на которых расположены объекты
     */
    var territory: String? = null
        @XmlElement(name = "required_territory") set
}
