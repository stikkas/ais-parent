package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlTransient
import javax.xml.bind.annotation.XmlType

/**
 * Описание файла документа
 */
@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/commons/Documents",
    propOrder = ["fileURI", "md5sum"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class AttachmentFile {

    /**
     * URI файла
     * example - embedded://Pasport_Semenov.pdf
     */
    var fileURI: String? = null
        get() = fileName?.let { "embedded://$it" }

    @XmlTransient
    var fileName: String? = null

    /**
     * MD5 сумма для прикладываемого файла
     * example - F9F8B917314CE26D40651BA7C8296721
     */
    var md5sum: String? = null
}
