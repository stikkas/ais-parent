package ru.insoft.libs.smevs.marry

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.*
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

@XmlAccessorType(XmlAccessType.FIELD)
class FizDetails {
    @XmlElement(name = "ФИО")
    var fio: FioType? = null

    @XmlElement(name = "ДатаРождКаленд")
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var birthDate: LocalDate? = null

    @XmlTransient
//    @XmlElement(name = "УдЛичнФЛ")
    var ausweis: Personalausweis? = null

    @XmlAttribute(name = "СНИЛС")
    var snils: String? = null
}
