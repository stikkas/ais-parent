package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Разрешенное использование по градостроительному регламенту
 */
class PermittesUsesGradReg {
    var border: String? = null
        @XmlElement(name = "reg_numb_border") set

    var use: CDDict? = null
        @XmlElement(name = "land_use") set

    /**
     * Разрешенное использование (текстовое описание)
     */
    var text: String? = null
        @XmlElement(name = "permitted_use_text") set

    override fun toString() = listOfNotNull(
        "Разрешенное использование по градостроительному регламенту:",
        border?.let { "Реестровый номер границы: $it" },
        use?.value?.let { "Вид разрешенного использования в соответствии с классификатором, утвержденным приказом Минэкономразвития России от 01.09.2014 № 540: $it" },
        text?.let { "Разрешенное использование (текстовое описание): $it" }
    ).joinToString("\n")
}
