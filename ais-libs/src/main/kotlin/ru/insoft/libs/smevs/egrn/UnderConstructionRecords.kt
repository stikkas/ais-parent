package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class UnderConstructionRecords {
    var records: List<ObjectUnderConstructionRecord>? = null
        @XmlElement(name = "object_under_construction_record") set

    override fun toString() = records?.joinToString("\n") ?: ""
}
