package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Назначение объекта недвижимости
 */
class ParamsPurpose {
    /**
     * Назначение
     */
    var purpose: String? = null
        @XmlElement set

    override fun toString() = purpose?.let { "Назначение объекта недвижимости: $it" } ?: ""
}
