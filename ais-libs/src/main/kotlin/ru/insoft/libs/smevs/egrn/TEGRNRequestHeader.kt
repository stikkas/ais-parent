package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.smevs.adapters.LocalDateTimeAdapter
import java.time.LocalDateTime
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Технологический заголовок заявления на Предоставление сведений
 */
@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/TStatementCommons",
    propOrder = ["actionCode", "statementType", "creationDate", "appliedDocument"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class TEGRNRequestHeader {

    /**
     * Код УРД
     */
    val actionCode = "659511111112"

    /**
     * Вид заявления по справочнику "Заявления и запросы ЕГРН"
     */
    val statementType = "558630200000"

    /**
     * Дата создания документа
     */
    @XmlJavaTypeAdapter(LocalDateTimeAdapter::class)
    val creationDate = LocalDateTime.now()

    /**
     * Приложенные документы
     */
    var appliedDocument: SomeDocument? = null
}
