package ru.insoft.libs.smevs.divorce

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.*
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

@XmlAccessorType(XmlAccessType.FIELD)
class ManDetails {
    @XmlElement(name = "ФИО")
    var fio: Fio? = null

    @XmlElement(name = "ДатаРождКаленд")
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var birthDate: LocalDate? = null

    // TODO убрать для всех подобных элементов выборку из базы
    @XmlTransient
//    @XmlElement(name = "УдЛичнФЛ")
    var ausweis: PersonalDoc? = null

    @XmlAttribute(name = "СНИЛС")
    var snils: String? = null
}
