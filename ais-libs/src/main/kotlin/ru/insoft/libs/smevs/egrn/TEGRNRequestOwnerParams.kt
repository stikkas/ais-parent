package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Параметры правообладателя в запросе на предоставление сведений о правах отдельного лица
 */
@XmlAccessorType(XmlAccessType.FIELD)
class TEGRNRequestOwnerParams {
    /**
     * Физическое лицо. Правообладатель
     */
    var person: TEGRNRequestOwner? = null
}
