package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Улица
 */
class Street {

    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_street") set

    /**
     * Наименование
     */
    var name: String? = null
        @XmlElement(name = "name_street") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")

}
