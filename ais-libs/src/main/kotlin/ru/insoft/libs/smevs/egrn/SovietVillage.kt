package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Сельсовет
 */
class SovietVillage {

    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_soviet_village") set

    /**
     * Наименование
     */
    var name: String? = null
        @XmlElement(name = "name__soviet_village") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")
}
