package ru.insoft.libs.smevs.marry

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

/**
 * Ответ на запрос, сформированный участником взаимодействия СМЭВ
 * на предоставление сведений об актах гражданского состояния о заключении брака,
 * содержащихся в Едином государственном реестре записей актов гражданского состояния
 */
@XmlRootElement(name = "BRAKZAKINFResponse")
class BrakResponse {

    /**
     * Сведения ответа на запрос о государственной регистрации АГС о заключении брака
     */
    var data: List<ResDetailsAgs>? = null
        @XmlElement(name = "СведОтветАГС") set

    override fun toString() = data?.firstOrNull()?.toString() ?: "Нет данных"
}
