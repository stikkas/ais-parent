package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Номер реестровой записи о вещном праве
 */
class RightRecordNumber {
    /**
     * Номер реестровой записи
     */
    var number: String? = null
        @XmlElement set

    /**
     * Номер регистрации вещного права
     */
    var rightNumber: String? = null
        @XmlElement(name = "right_number") set
}
