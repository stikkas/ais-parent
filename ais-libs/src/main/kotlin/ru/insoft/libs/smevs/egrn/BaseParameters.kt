package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Основные характеристики
 */
class BaseParameters {
    var params: List<BaseParameter>? = null
        @XmlElement(name = "base_parameter") set

    override fun toString() = params?.joinToString("\n") ?: ""
}
