package ru.insoft.libs.smevs.sud

import ru.insoft.libs.smevs.PersonalInfo
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Розыск
 */
@XmlAccessorType(XmlAccessType.FIELD)
class Manhunt {

    /**
     * Персональные данные
     */
    var personalInfo: PersonalInfo? = null

    /**
     * Данные по розыску
     */
    var manhuntInfo: LegalInfo? = null

    /**
     * Дополнительные данные
     */
    var additionalInfo: String? = null

    override fun toString() = listOfNotNull(
        "Розыск:",
        personalInfo?.let { "Персональные данные: $it" },
        manhuntInfo?.let { "Данные по розыску: $it" },
        additionalInfo?.let { "Дополнительные данные: $it" }
    ).joinToString("\n")
}
