package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class AddressMain {
    var fias: Address? = null
        @XmlElement(name = "address_fias") set

    var note: String? = null
        @XmlElement set

    var address: String? = null
        @XmlElement(name = "readable_address") set

    override fun toString() = listOfNotNull(
        fias?.let { "Адрес (местоположение): $it" },
        note?.let { "Неформализованное описание: $it" },
        address?.let { "Адрес в соответствии с ФИАС (Текст): $it" }
    ).filter { it.isNotEmpty() }.joinToString("\n")
}
