package ru.insoft.libs.smevs.sud

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Решение
 */
@XmlAccessorType(XmlAccessType.FIELD)
class DecisionType {
    /**
     * Описание
     */
    var description: String? = null

    /**
     * Юридическая информация
     */
    var legalInfo: LegalInfo? = null

    override fun toString(): String {
        val res = ArrayList<String>()
        description?.let { res.add(it) }
        legalInfo?.let { res.add(it.toString()) }
        return res.filter { it.isNotBlank() }.joinToString()
    }
}
