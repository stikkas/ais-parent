package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Местоположение (до уровня населенного пункта)
 */
class LocationCity {

    /**
     * Уровень населенного пункта
     */
    var level: AddressCity? = null
        @XmlElement(name = "level_settlement") set

    /**
     * Описание местоположения
     */
    var description: String? = null
        @XmlElement(name = "position_description") set

    override fun toString() = listOfNotNull(
        level?.let { l ->
            listOfNotNull(
                l.fias?.let { "Код ФИАС: $it" },
                l.okato?.let { "ОКАТО: $it" },
                l.kladr?.let { "КЛАДР: $it" },
                l.oktmo?.let { "ОКТМО: $it" },
                l.zip?.let { "Почтовый индекс: $it" },
                l.region?.value?.let { "Код региона: $it" },
                l.district?.type?.let { "Тип района: $it" },
                l.district?.name?.let { "Наименование района: $it" },
                l.city?.type?.let { "Тип муниципального образования: $it" },
                l.city?.name?.let { "Наименование муниципального образования: $it" },
                l.urban?.type?.let { "Тип городского района: $it" },
                l.urban?.name?.let { "Наименование городского района: $it" },
                l.selsovet?.type?.let { "Тип сельсовета: $it" },
                l.selsovet?.name?.let { "Наименование сельсовета: $it" },
                l.locality?.type?.let { "Тип населенного пункта: $it" },
                l.locality?.name?.let { "Наименование населенного пункта: $it" }
            ).joinToString("\n")
        },
        description?.let { "Описание местоположения: $it" }
    ).joinToString("\n", "Адрес (описание местоположения) до уровня населённого пункта:\n")

}
