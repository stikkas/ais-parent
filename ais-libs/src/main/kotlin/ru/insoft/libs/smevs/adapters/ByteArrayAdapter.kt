package ru.insoft.libs.smevs.adapters

import java.util.*
import javax.xml.bind.annotation.adapters.XmlAdapter

class ByteArrayAdapter : XmlAdapter<String, ByteArray>() {
    override fun marshal(v: ByteArray?) = v?.let { Base64.getEncoder().encodeToString(it) }

    override fun unmarshal(v: String?) = v?.let { Base64.getDecoder().decode(it) }

}
