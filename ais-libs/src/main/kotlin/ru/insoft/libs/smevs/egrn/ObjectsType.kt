package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Вид и (или) назначение объектов недвижимости, о зарегистрированных правах на которые запрошены сведения
 */
class ObjectsType {
    /**
     * Вид объектов недвижимости
     */
    var type: String? = null
        @XmlElement set

    /**
     * Назначение объектов недвижимости
     */
    var purpose: String? = null
        @XmlElement set
}
