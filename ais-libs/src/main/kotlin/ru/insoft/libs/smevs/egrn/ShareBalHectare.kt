package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Размер доли в праве в балло-гектарах
 */
class ShareBalHectare {
    /**
     * Балло-гектары
     */
    var bal: Double? = null
        @XmlElement(name = "bal_hectare") set

    override fun toString() = bal?.let { "Размер доли в праве в балло-гектарах: $it" } ?: ""
}
