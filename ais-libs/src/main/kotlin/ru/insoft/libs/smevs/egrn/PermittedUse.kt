package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class PermittedUse {
    var name: String? = null
        @XmlElement set
}
