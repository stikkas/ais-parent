package ru.insoft.libs.smevs.marry

import ru.insoft.libs.format
import javax.xml.bind.annotation.*

/**
 * Предоставление из ЕГР ЗАГС по запросу сведений о заключении брака
 */
@XmlRootElement(name = "BRAKZAKINFRequest")
@XmlAccessorType(XmlAccessType.FIELD)
class BrakRequest {

    /**
     * Количество документов в представленном файле
     */
    @XmlAttribute(required = true, name = "КолДок")
    val docsCount: Int = 1

    /**
     * Тип акта гражданского состояния, в отношении которого сформирован запрос
     */
    @XmlAttribute(required = true, name = "ТипАГС")
    val actType: String = "02"

    /**
     * Идентификатор запроса, сформированный запрашивающей стороной
     */
    @XmlAttribute(required = true, name = "ИдЗапрос")
    var id: String? = null

    /**
     * Сведения о нормативно-правовых основаниях запрашивающей стороны для получения сведений
     * из ЕГР ЗАГС об актах гражданского состояния о заключении брака
     */
    @XmlElement(name = "СведОсн")
    val mainInfo = MainInfoType()

    /**
     * Сведения, содержащиеся в запросе
     */
    @XmlElement(name = "СведЗапрос")
    var details: ReqDetails? = null

    override fun toString() = """ФИО: ${details?.fizInfo?.fio ?: ""}
        |Дата рождения: ${details?.fizInfo?.birthDate?.format() ?: ""}${details?.fizInfo?.snils?.let { "\n|СНИЛС: $it" } ?: ""}
        |${
        details?.agsInfo?.let { a ->
            listOfNotNull(a.nomer?.let { "Номер записи акта гражданского состояния: $it" },
                a.date?.format()?.let { "Дата записи акта гражданского состояния: $it" },
                a.organ?.let { "Орган ЗАГС: $it" }
            ).joinToString("\n|")
        } ?: ""
    }
        |""".trimMargin()
//    ${details?.fizInfo?.ausweis?.let { "\n|Документ, удостоверяющий личность: $it" } ?: ""}
}
