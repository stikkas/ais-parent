package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Муниципальное образование
 */
class City {

    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_city") set

    /**
     * Наименование
     */
    var name: String? = null
        @XmlElement(name = "name_city") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")
}
