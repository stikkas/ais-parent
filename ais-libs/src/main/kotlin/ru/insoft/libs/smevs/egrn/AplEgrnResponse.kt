package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.format
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

/**
 * Выписка о правах отдельного лица на имевшиеся (имеющиеся) у него объекты недвижимости
 */
@XmlRootElement(name = "extract_rights_individ_available_real_estate_objects")
class AplEgrnResponse {
    /**
     * Реквизиты выписки
     */
    var details: DetailsStatement? = null
        @XmlElement(name = "details_statement") set

    /**
     * Реквизиты поступившего запроса
     */
    var inputRequest: DetailsRequest? = null
        @XmlElement(name = "details_request") set

    /**
     * Сведения об объекте (объектах) недвижимости,  правах, ограничениях прав, обременениях объекта недвижимости
     */
    var baseData: BaseDataRightRestrictIndivid? = null
        @XmlElement(name = "base_data") set

    override fun toString() = listOfNotNull(details?.let { d ->
        listOfNotNull(
            d.top?.let { t ->
                listOfNotNull(t.rights?.let { "Полное наименование органа регистрации прав: $it" },
                    t.date?.let { "Дата формирования выписки: $it" },
                    t.number?.let { "Регистрационный номер: $it" }).joinToString("\n")
            },
            d.lower?.let { l ->
                listOfNotNull(l.name?.let { "Полное наименование должности: $it" },
                    l.initials?.let { "Инициалы, фамилия: $it" }).joinToString("\n")
            }).joinToString("\n")
    }, inputRequest?.let { r ->
        listOfNotNull(r.date?.let { "Дата поступившего запроса: ${it.format()}" },
            r.dateReceipt?.let { "Дата получения запроса органом регистрации прав: ${it.format()}" },
            r.period?.let { p ->
                listOfNotNull(
                    p.date?.let { "Дата, по состоянию на которую или период, за который запрошены сведения: ${it.format()}" },
                    p.start?.let { "Дата начала периода: ${it.format()}" },
                    p.end?.let { "Дата окончания периода: ${it.format()}" }
                ).joinToString("\n")
            },
            r.holder?.let { "Сведения о правообладателе согласно сведениям, указанным в запросе: $it" },
            r.objects?.let { o ->
                listOfNotNull(
                    o.type?.let { "Вид объектов недвижимости, о зарегистрированных правах на которые запрошены сведения: $it" },
                    o.purpose?.let { "Назначение объектов недвижимости, о зарегистрированных правах на которые запрошены сведения: $it" }
                ).joinToString("\n")
            },
            r.territory?.let { "Территории определенного(ых) субъекта(ов) Российской Федерации, указанные в запросе, на которых расположены объекты: $it" }
        ).joinToString("\n")
    }, baseData).joinToString("\n")
        .split("\n").filter { d -> d.isNotBlank() }.joinToString("\n")
}
