package ru.insoft.libs.smevs.marry

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о государственной регистрации заключения брака
 */
class RegBrakInfo {
    /**
     * Раздел ЕГР ЗАГС, в котором содержатся сведения о регистрации акта гражданского состояния
     */
    var part: Int? = null
        @XmlAttribute(name = "РазделЕГР") set

    /**
     * Номер актовой записи о заключении брака в ЕГР ЗАГС
     */
    var egrRecNumber: String? = null
        @XmlAttribute(name = "НомерЗаписЕГР") set

    /**
     * Номер, актовой записи указанный в свидетельстве о заключении брака
     */
    var agsRecNumber: String? = null
        @XmlAttribute(name = "НомАГССвид") set

    /**
     * Номер версии записи
     */
    var versionNumber: String? = null
        @XmlAttribute(name = "НомерВерс") set

    /**
     * Дата версии записи
     */
    var recDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlAttribute(name = "ДатаВерс") set

    /**
     * Код состояния и статуса записи акта о заключении брака по справочнику СОСТАГС
     */
    var statusCode: String? = null
        @XmlAttribute(name = "КодСостСтат", required = true) set

    /**
     * Дата составления актовой  записи о заключении брака по представленным документам (неполная дата)
     */
    var regDocDate: PartialDate? = null
        @XmlElement(name = "ДатаЗаписДок") set

    /**
     * Дата составления актовой  записи о заключении брака
     */
    var regDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаЗапис") set

    /**
     * Сведения об органе ЗАГС, которым произведена государственная регистрация заключения брака
     */
    var organ: AgsType? = null
        @XmlElement(name = "ОрганЗАГС") set

    /**
     * Сведения о выданном Свидетельстве о заключении брака
     */
    var cert: List<MarryCert>? = null
        @XmlElement(name = "СвидетЗаклБрак") set

    /**
     * Передаваемые сведения о государственной регистрации заключения брака
     */
    var regInfo: RegInfo? = null
        @XmlElement(name = "ПрдСведРег") set

    /**
     * Сведения о внесении в ЕГР ЗАГС сведений о документе компетентного органа иностранного государства (Раздел 2)
     */
    var reg2Info: AgsInostrInfo? = null
        @XmlElement(name = "СВедЕГРРазд2") set

    override fun toString() =
        """Раздел ЕГР ЗАГС: ${if (part == 1) "Первый раздел" else if (part == 2) "Второй раздел" else "Раздел $part"}
        |Номер актовой записи о заключении брака, содержащийся в ЕГР ЗАГС: ${egrRecNumber ?: ""}
        |Номер актовой записи, указанный в свидетельстве о заключении брака: ${agsRecNumber ?: ""}
        |Дата составления актовой записи о заключении брака: ${(regDate ?: regDocDate?.toLocalDate())?.format() ?: ""}
        |Сведения об органе ЗАГС, которым произведена государственная регистрация акта гражданского состояния: ${organ?.name ?: ""}
        |${cert?.firstOrNull() ?: "Нет сведений о выданном Свидетельстве о заключении брака"}
        |${regInfo ?: ""}
        |${reg2Info ?: ""}
    |""".trimMargin()

}
