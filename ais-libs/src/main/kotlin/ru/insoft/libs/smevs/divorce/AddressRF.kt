package ru.insoft.libs.smevs.divorce

import javax.xml.bind.annotation.XmlAttribute

/**
 * Адрес места жительства на территории Российской Федерации
 */
class AddressRF {
    var id: String? = null
        @XmlAttribute(name = "ИдНом") set

    var text: String? = null
        @XmlAttribute(name = "АдрТекст") set

    var oktmo: String? = null
        @XmlAttribute(name = "ОКТМО") set

    var index: Int? = null
        @XmlAttribute(name = "Индекс") set

    var code: String? = null
        @XmlAttribute(name = "КодРегион") set

    var region: String? = null
        @XmlAttribute(name = "НаимРегион") set

    var district: String? = null
        @XmlAttribute(name = "Район") set

    var city: String? = null
        @XmlAttribute(name = "Город") set

    var punkt: String? = null
        @XmlAttribute(name = "НаселПункт") set

    var street: String? = null
        @XmlAttribute(name = "Улица") set

    var house: String? = null
        @XmlAttribute(name = "Дом") set

    var korpus: String? = null
        @XmlAttribute(name = "Корпус") set

    var flat: String? = null
        @XmlAttribute(name = "Кварт") set

    override fun toString() = text ?: listOfNotNull(oktmo?.let { "ОКТМО $it" },
        index?.let { "Индекс $it" }, region?.let { "Регион $it" }, district?.let { "Район $it" },
        city?.let { "Город $it" }, punkt?.let { "Населенный пункт $it" }, street?.let { "Улица $it" },
        house?.let { "Дом $it" }, korpus?.let { "Корпус $it" }, flat?.let { "Квартира $it" }).joinToString()

}
