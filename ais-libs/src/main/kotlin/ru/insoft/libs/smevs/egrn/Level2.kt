package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Корпус
 */
class Level2 {

    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_level2") set

    /**
     * Значение
     */
    var name: String? = null
        @XmlElement(name = "name_level2") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")
}
