package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Городской район
 */
class UrbanDistrict {
    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_urban_district") set

    /**
     * Наименование
     */
    var name: String? = null
        @XmlElement(name = "name_urban_district") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")
}
