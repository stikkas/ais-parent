package ru.insoft.libs.smevs.marry

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения об уведомлении о регистрации компетентным органом иностранного государства акта гражданского состояния
 */
class RegInfoFremd {

    /**
     * Вид уведомления по справочнику СДРАГС
     */
    var type: String? = null
        @XmlElement(name = "КодВидУвед") set

    /**
     * Дата подачи уведомления
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаУвед") set

    /**
     * Сведения об органе ЗАГС Российской Федерации или консульском учреждении Российской Федерации,
     * куда направлено уведомление о факте регистрации компетентным органом
     * иностранного государства АГС в отношении гражданина РФ
     */
    var organZags: ConsulOrganZags? = null
        @XmlElement(name = "СведЗАГСУвед") set

    /**
     * Способ подачи уведомления
     */
    var method: Int? = null
        @XmlAttribute(name = "СпоcПод") set

    /**
     * Количество листов в уведомлении
     */
    var pageCount: Int? = null
        @XmlAttribute(name = "КолЛистДок") set
}
