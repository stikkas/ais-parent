package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute

/**
 * Ссылка на документ по его идентификатору
 */
@XmlAccessorType(XmlAccessType.FIELD)
class TDocumentRefer {

    /**
     * Идентификатор документа
     */
    @XmlAttribute
    var documentID = "1"
}
