package ru.insoft.libs.smevs.sud

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType


/**
 * Информация о запросе
 */
@XmlRootElement(name = "request")
@XmlType(propOrder = ["birthDate", "SNILS", "surname", "name", "patronymicName", "registrationPlace"])
class SudimostRequest {
    /**
     * Дата рождения
     */
    var birthDate: IncompleteDate? = null

    /**
     * СНИЛС
     */
    var SNILS: String? = null
        set(value) {
            field = value?.replace("-", "")
        }


    /**
     * Фамилия
     */
    var surname: String? = null
        get() = field?.trim()

    /**
     * Имя
     */
    var name: String? = null
        get() = field?.trim()

    /**
     * Отчество
     */
    var patronymicName: String? = null
        get() = field?.trim()

    /**
     * Регистрации
     */
    var registrationPlace: List<RegistrationInfo>? = null

    /**
     * Идентификатор элемента
     */
    var Id: String? = null
        @XmlAttribute(required = true, name = "Id") get

    override fun toString() = """Дата рождения: ${birthDate ?: ""}
        |СНИЛС: ${SNILS ?: ""}
        |""".trimMargin() + (registrationPlace?.map {
        "Адрес: ${it.type ?: ""} - ${it.place ?: ""}"
    }?.joinToString("\n") ?: "Адрес:")
}
