package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

/**
 * Сведения о организации, выдавшей документ, или авторе документа
 */
@XmlType(namespace = "http://rosreestr.ru/services/v0.1/commons/Documents")
@XmlAccessorType(XmlAccessType.FIELD)
class IssuerInfo {
    var name: String? = null
}
