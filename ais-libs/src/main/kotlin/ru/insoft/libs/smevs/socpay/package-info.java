@XmlSchema(
		elementFormDefault = XmlNsForm.QUALIFIED,
		namespace = "http://kvs.pfr.com/benefits-form-info/1.0.0",
		xmlns = {
				@XmlNs(prefix = "smev", namespaceURI = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1"),
				@XmlNs(prefix = "tns", namespaceURI = "http://kvs.pfr.com/benefits-form-info/1.0.0"),
				@XmlNs(prefix = "xmlns", namespaceURI = "http://www.w3.org/2001/XMLSchema")
		}
)
package ru.insoft.libs.smevs.socpay;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

