package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class RoomRecords {
    var record: List<RoomRecord>? = null
        @XmlElement(name = "room_record") set

    override fun toString() = record?.joinToString("\n") ?: ""
}
