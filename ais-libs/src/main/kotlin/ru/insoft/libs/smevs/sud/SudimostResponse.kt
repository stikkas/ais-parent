package ru.insoft.libs.smevs.sud

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
class SudimostResponse {
    var records: Records? = null

    override fun toString() = records?.toString() ?: "Нет сведений"
}
