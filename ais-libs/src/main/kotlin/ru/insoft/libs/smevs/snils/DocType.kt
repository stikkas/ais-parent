package ru.insoft.libs.smevs.snils

import ru.insoft.libs.format
import ru.insoft.libs.models.Document
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlTransient
import javax.xml.bind.annotation.XmlType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

@XmlType(
    namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.2",
    propOrder = ["series", "number", "issueDate", "issuer"]
)
open class DocType {

    var series: String? = null
        @XmlElement(name = "Series") get

    var number: String? = null
        @XmlElement(name = "Number") get

    var issueDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "IssueDate") get

    var issuer: String? = null
        @XmlElement(name = "Issuer") get

    var name: String? = null
        @XmlTransient get

    companion object {
        fun build(doc: Document?) = doc?.let {
            when (it.typeId) {
                "1" -> SovietPassport()
                "3" -> MilitaryPassport()
                "4" -> ResidencePermitRF()
                "5" -> BirthCertificate()
                "6" -> TemporaryIdentityCardRF()
                "7" -> InternationalPassportRF()
                "8" -> Form9Certificate()
                "9" -> PassportRF()
                "13" -> SailorPassport()
                "14" -> ForeignPassport()
                else -> IdentificationDocType().apply {
                    type = it.name
                    document = DocType().apply {
                        series = it.series?.replace(Regex("[\\s-]"), "")
                        number = it.number?.replace(Regex("[\\s-]"), "")
                        issueDate = it.issueDate
                        issuer = it.whogive
                        name = it.name
                    }
                }
            }.apply {
                if (this is DocType) {
                    series = it.series?.replace(Regex("[\\s-]"), "")
                    number = it.number?.replace(Regex("[\\s-]"), "")
                    issueDate = it.issueDate
                    issuer = it.whogive
                    name = it.name
                }
            }
        }
    }

    override fun toString() =
        listOfNotNull(name, series, number, issuer, issueDate?.format()?.let { "от $it" }).joinToString(" ")
}
