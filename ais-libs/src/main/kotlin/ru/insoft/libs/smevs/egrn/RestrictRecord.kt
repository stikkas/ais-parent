package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement


class RestrictRecord {
    /**
     * Общие сведения об ограничениях и обременениях
     */
    var data: RestrictData? = null
        @XmlElement(name = "restrictions_encumbrances_data") set
}
