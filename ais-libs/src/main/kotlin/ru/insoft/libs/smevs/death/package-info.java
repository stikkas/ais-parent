@XmlSchema(
		elementFormDefault = XmlNsForm.QUALIFIED,
		attributeFormDefault = XmlNsForm.UNQUALIFIED,
		namespace = "urn://x-artefacts-zags-fatalinf/root/112-52/4.0.1",
		xmlns = {
				@XmlNs(prefix = "ns1", namespaceURI = "urn://x-artefacts-zags-fatalinf/root/112-52/4.0.1"),
				@XmlNs(prefix = "tns", namespaceURI = "urn://x-artefacts-zags-fatalinf/root/112-52/4.0.1"),
				@XmlNs(prefix = "usch", namespaceURI = "http://www.unisoftware.ru/schematron-extensions"),
				@XmlNs(prefix = "sch", namespaceURI = "http://purl.oclc.org/dsdl/schematron"),
				@XmlNs(prefix = "fns", namespaceURI = "urn://x-artefacts-zags-fatalinf/types/4.0.1"),
				@XmlNs(prefix = "fnst", namespaceURI = "urn://x-artefacts-zags-fatalinf/types/4.0.1"),
				@XmlNs(prefix = "xs", namespaceURI = "http://www.w3.org/2001/XMLSchema")
		}
)
package ru.insoft.libs.smevs.death;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
