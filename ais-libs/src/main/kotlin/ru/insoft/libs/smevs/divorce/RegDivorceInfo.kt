package ru.insoft.libs.smevs.divorce

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о государственной регистрации расторжения брака
 */
class RegDivorceInfo {

    /**
     * Дата составления актовой  записи о расторжении брака
     */
    var regDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаЗапис") set

    /**
     * Дата составления актовой  записи о расторжении брака по представленным документам (неполная дата)
     */
    var regDocDate: PartialDate? = null
        @XmlElement(name = "ДатаЗаписДок") set

    /**
     * Сведения об органе ЗАГС, которым произведена государственная регистрация расторжения брака
     */
    var organ: RagsType? = null
        @XmlElement(name = "ОрганЗАГС") set

    /**
     * Сведения о выданном Свидетельстве о расторжении брака
     */
    var cert: List<DivorceCert>? = null
        @XmlElement(name = "СвидетРастБрак") set

    /**
     * Передаваемые сведения о государственной регистрации расторжения брака
     */
    var regInfo: RegInfo? = null
        @XmlElement(name = "ПрдСведРег") set

    /**
     * Сведения о внесении в ЕГР ЗАГС сведений о документе компетентного органа иностранного государства (Раздел 2)
     */
    var reg2Info: AgsInostrInfo? = null
        @XmlElement(name = "СВедЕГРРазд2") set

    /**
     * Раздел ЕГР ЗАГС, в котором содержатся сведения о регистрации акта гражданского состояния
     */
    var part: Int? = null
        @XmlAttribute(name = "РазделЕГР") set

    /**
     * Номер актовой записи о заключении брака в ЕГР ЗАГС
     */
    var egrRecNumber: String? = null
        @XmlAttribute(name = "НомерЗаписЕГР") set

    /**
     * Номер, актовой записи указанный в свидетельстве о заключении брака
     */
    var agsRecNumber: String? = null
        @XmlAttribute(name = "НомАГССвид") set

    override fun toString() =
        """Раздел ЕГР ЗАГС: ${if (part == 1) "Первый раздел" else if (part == 2) "Второй раздел" else "Раздел $part"}
        |Номер актовой записи о расторжении брака, содержащийся в ЕГР ЗАГС: ${egrRecNumber ?: ""}
        |Номер актовой записи, указанный в свидетельстве о расторжении брака: ${agsRecNumber ?: ""}
        |Дата составления актовой записи о расторжении брака: ${(regDate ?: regDocDate?.toLocalDate())?.format() ?: ""}
        |Сведения об органе ЗАГС, которым произведена государственная регистрация акта гражданского состояния: ${organ?.name ?: ""}
        |${cert?.firstOrNull() ?: "Нет сведений о выданном Свидетельстве о расторжении брака"}
        |${regInfo ?: ""}
        |${reg2Info ?: ""}
    |""".trimMargin()

}
