package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class CadNumber {
    var number: String? = null
        @XmlElement(name = "cad_number") set
}
