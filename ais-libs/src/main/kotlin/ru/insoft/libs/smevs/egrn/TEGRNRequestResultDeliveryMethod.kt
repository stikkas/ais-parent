package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

@XmlType(namespace = "http://rosreestr.ru/services/v0.1/TStatementCommons")
@XmlAccessorType(XmlAccessType.FIELD)
class TEGRNRequestResultDeliveryMethod {
    /**
     * Способ доставки результата
     */
    var recieveResultTypeCode = "webService"
}
