package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/commons/Subjects",
    propOrder = ["ogrn", "inn"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class TNativeOrgParams {
    /**
     * Основной государствкенный регистрационный номер (ОГРН) организации
     */
    var ogrn: String? = null

    /**
     * Идентификационный номер налогоплательщика (ИНН) организации
     */
    var inn: String? = null
}
