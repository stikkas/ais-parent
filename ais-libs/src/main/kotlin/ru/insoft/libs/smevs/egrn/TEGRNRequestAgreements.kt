package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/TStatementCommons",
    propOrder = ["persDataProcessingAgreement", "actualDataAgreement", "qualityOfServiceAgreement", "qualityOfServiceTelephoneNumber"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class TEGRNRequestAgreements {
    var persDataProcessingAgreement = "01"
    var actualDataAgreement = "03"
    var qualityOfServiceAgreement = "01"
    var qualityOfServiceTelephoneNumber: String? = null
}
