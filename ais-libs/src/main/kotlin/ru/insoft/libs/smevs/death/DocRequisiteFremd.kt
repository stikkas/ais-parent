package ru.insoft.libs.smevs.death

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Реквизиты документа, выданного компетентным органом иностранного государства
 */
class DocRequisiteFremd {
    /**
     * Наименование документа иностранного государства (текстом)
     */
    var name: String? = null
        @XmlElement(name = "НаимДокТекст") set

    /**
     * Номер документа иностранного государства
     */
    var number: String? = null
        @XmlElement(name = "Номер") set

    /**
     * Дата документа (календарная дата) иностранного государства
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаКаленд") set

    /**
     * Дата документа иностранного государства по представленным документам
     */
    var docDate: PartialDate? = null
        @XmlElement(name = "ДатаДок") set

    /**
     * Наименование компетентного органа иностранного государства, которым составлен документ
     */
    var orgName: String? = null
        @XmlElement(name = "НаимОрган") set

    /**
     * Тип документа  (по справочнику СДРАГС)
     */
    var type: String? = null
        @XmlAttribute(name = "ТипДок") set

    override fun toString(): String {
        val doc = listOfNotNull(name, number, date, docDate).joinToString(" ")
        return doc + (orgName?.let { "${if (doc.isNotEmpty()) ", " else ""}$it" } ?: "")
    }
}
