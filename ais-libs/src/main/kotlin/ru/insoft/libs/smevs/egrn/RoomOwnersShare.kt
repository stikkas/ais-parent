package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Размер доли в праве общей долевой собственности на общее имущество собственников комнат в жилом помещении
 */
class RoomOwnersShare {
    var info: String? = null
        @XmlElement(name = "room_owners_share_info") set

    override fun toString() =
        info?.let { "Размер доли в праве общей долевой собственности на общее имущество собственников комнат в жилом помещении: $it" }
            ?: ""
}
