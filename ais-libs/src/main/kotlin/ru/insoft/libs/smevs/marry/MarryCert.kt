package ru.insoft.libs.smevs.marry

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о выданном Свидетельстве
 */
@XmlType(namespace = "urn://x-artefacts-zags-brakzakinf/types/4.0.0")
class MarryCert {
    /**
     * Серия свидетельства
     */
    var series: String? = null
        @XmlElement(name = "СерияСвидет") set

    /**
     * Номер свидетельства
     */
    var number: String? = null
        @XmlElement(name = "НомерСвидет") set

    /**
     * Дата выдачи свидетельства (календарная дата)
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаВыдСвидет") set

    /**
     * Дата выдачи свидетельства по представленным документам (неполная дата)
     */
    var docDate: PartialDate? = null
        @XmlElement(name = "ДатаВыдСвидетДок") set

    var type: Int? = null
        @XmlAttribute(name = "ТипСвидет") set

    override fun toString() =
        """Тип выданного свидетельства: ${if (type == 1) "Первичное свидетельство" else if (type == 2) "Повторно выданное свидетельство" else "Тип $type"}
        |Серия свидетельства: ${series ?: ""}
        |Номер свидетельства: ${number ?: ""}
        |Дата выдачи свидетельства: ${(date ?: docDate?.toLocalDate())?.format() ?: ""}
    |""".trimMargin()
}
