package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.format
import javax.xml.bind.annotation.XmlElement

/**
 * Общие сведения о правах (переходе/прекращении права)
 */
class RightDataAndCancel {

    /**
     * Вид зарегистрированного вещного права
     */
    var type: CDDict? = null
        @XmlElement(name = "right_type") set

    /**
     * Номер регистрации вещного права
     */
    var number: String? = null
        @XmlElement(name = "right_number") set

    /**
     * Размер доли в праве
     */
    var shares: Shares? = null
        @XmlElement set

    /**
     * Описание доли текстом
     */
    var shareDesc: String? = null
        @XmlElement(name = "share_description") set

    /**
     * Номер регистрации перехода/прекращения права
     */
    var cancelNumber: String? = null
        @XmlElement(name = "cancel_right_number") set

    /**
     * Номера регистрации вновь возникшего права
     */
    var newAroseNumber: NewAroseRightNumbers? = null
        @XmlElement(name = "new_arose_right_numbers") set

    /**
     * Восстановление права на основании судебного акта
     */
    var reinstatement: ReinstatementDate? = null
        @XmlElement set

    override fun toString() = listOfNotNull(
        type?.value?.let { "Вид зарегистрированного вещного права: $it" },
        number?.let { "Номер регистрации вещного права: $it" },
        shares?.toString(),
        shareDesc?.let { "Описание доли текстом: $it" },
        cancelNumber?.let { "Номер регистрации перехода/прекращения права: $it" },
        newAroseNumber?.toString(),
        reinstatement?.date?.let { "Восстановление права на основании судебного акта\nДата ранее произведенной государственной регистрации права: ${it.format()}" }
    ).filter { it.isNotEmpty() }.joinToString("\n")
}
