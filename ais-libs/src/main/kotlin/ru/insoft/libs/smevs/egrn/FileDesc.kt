package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

/**
 * Описание файла с подписью
 */
@XmlType(namespace = "http://rosreestr.ru/services/v0.1/commons/Documents")
@XmlAccessorType(XmlAccessType.FIELD)
class FileDesc {
    /**
     * Атрибуты файла прикладываемого документа
     */
    var file: AttachmentFile? = null
}
