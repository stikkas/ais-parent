package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения о помещениях, машино-местах
 */
class InfoObjects {

    var infoObject: List<CadNumber>? = null
        @XmlElement(name = "info_object") set

    override fun toString() = infoObject?.mapNotNull { it.number }?.joinToString() ?: ""
}
