package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.smevs.adapters.LocalDateTimeAdapter
import java.time.LocalDateTime
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Восстановление права на основании судебного акта
 */
class ReinstatementDate {
    /**
     * Дата ранее произведенной государственной регистрации права
     */
    var date: LocalDateTime? = null
        @XmlJavaTypeAdapter(LocalDateTimeAdapter::class)
        @XmlElement(name = "prev_registration_date") set
}
