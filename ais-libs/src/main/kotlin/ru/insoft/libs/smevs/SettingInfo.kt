package ru.insoft.libs.smevs

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Установочные данные
 */
@XmlAccessorType(XmlAccessType.FIELD)
class SettingInfo {
    /**
     * Фамилия
     */
    var lastName: String? = null

    /**
     * Имя
     */
    var firstName: String? = null

    /**
     * Отчество
     */
    var patronymicName: String? = null

    /**
     * Дата рождения
     */
    var birthDate: IncompleteDate? = null

    /**
     * Место рождения
     */
    var birthplace: String? = null

    override fun toString() = listOf("$lastName $firstName $patronymicName", birthDate?.toString(), birthplace)
        .filterNot { it.isNullOrBlank() }.joinToString()
}
