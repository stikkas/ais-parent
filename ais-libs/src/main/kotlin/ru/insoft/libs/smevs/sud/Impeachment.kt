package ru.insoft.libs.smevs.sud

import ru.insoft.libs.smevs.PersonalInfo
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Привлечение
 */
@XmlAccessorType(XmlAccessType.FIELD)
class Impeachment {

    /**
     * Персональные данные
     */
    var personalInfo: PersonalInfo? = null

    /**
     * Данные по привлечению
     */
    var impeachmentInfo: LegalInfo? = null

    /**
     * Решение
     */
    var judgment: DecisionType? = null

    /**
     * Дополнительные данные
     */
    var additionalInfo: String? = null

    override fun toString() = listOfNotNull(
        "Привлечение:",
        personalInfo?.let { "Персональные данные: $it" },
        impeachmentInfo?.let { "Данные по привлечению: $it" },
        judgment?.let { "Решение: $it" },
        additionalInfo?.let { "Дополнительные данные: $it" }
    ).joinToString("\n")

}
