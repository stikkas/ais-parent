package ru.insoft.libs.smevs.sud

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter


/**
 * Освобождение
 */
@XmlAccessorType(XmlAccessType.FIELD)
class Release {
    /**
     * Дата
     */
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var date: LocalDate? = null

    /**
     * Основание
     */
    var cause: String? = null

    override fun toString() = listOf(date?.toString(), cause).filterNot { it.isNullOrBlank() }.joinToString(" - ")
}
