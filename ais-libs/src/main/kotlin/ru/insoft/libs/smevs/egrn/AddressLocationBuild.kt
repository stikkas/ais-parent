package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Адрес (местоположение) здания
 */
class AddressLocationBuild {

    /**
     * Тип адреса
     */
    var type: CDDict? = null
        @XmlElement(name = "address_type") set

    /**
     * Адрес (местоположение)
     */
    var address: AddressMain? = null
        @XmlElement set

    /**
     * Местоположение
     */
    var location: LocationOks? = null
        @XmlElement set

    override fun toString() = listOfNotNull(
        type?.value?.let { "Тип адреса: $it" },
        address, location
    ).joinToString("\n")
}
