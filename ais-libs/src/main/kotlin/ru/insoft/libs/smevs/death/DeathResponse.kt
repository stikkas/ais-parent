package ru.insoft.libs.smevs.death

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

/**
 * Ответ на запрос, сформированный участником взаимодействия СМЭВ
 * на предоставление сведений об актах гражданского состояния о смерти,
 * содержащихся в Едином государственном реестре записей актов гражданского состояния
 */
@XmlRootElement(name = "FATALINFResponse")
class DeathResponse {
    /**
     * Сведения ответа на запрос о государственной регистрации АГС о смерти
     */
    var data: List<ResAgsDetails>? = null
        @XmlElement(name = "СведОтветАГС") set

    override fun toString() = data?.firstOrNull()?.toString() ?: "Нет данных"
}
