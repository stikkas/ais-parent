package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateTimeAdapter
import java.time.LocalDateTime
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Даты государственной регистрации (постановки/снятия с учета (регистрации))
 */
class RecordInfo {
    /**
     * Дата постановки на учет/ регистрации
     */
    var regDate: LocalDateTime? = null
        @XmlJavaTypeAdapter(LocalDateTimeAdapter::class)
        @XmlElement(name = "registration_date") set

    /**
     * Дата снятия с учета/регистрации
     */
    var cancelDate: LocalDateTime? = null
        @XmlJavaTypeAdapter(LocalDateTimeAdapter::class)
        @XmlElement(name = "cancel_date") set

    override fun toString() = listOfNotNull(
        regDate?.let { "Дата постановки на учет/ регистрации: ${it.format()}" },
        cancelDate?.let { "Дата снятия с учета/регистрации (перехода/ прекращения права): ${it.format()}" }
    ).joinToString("\n")
}
