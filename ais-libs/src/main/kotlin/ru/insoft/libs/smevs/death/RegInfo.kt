package ru.insoft.libs.smevs.death

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import ru.insoft.libs.smevs.adapters.LocalTimeAdapter
import java.time.LocalDate
import java.time.LocalTime
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Передаваемые сведения о государственной регистрации смерти
 */
class RegInfo {
    /**
     * Пол
     */
    var sex: Int? = null
        @XmlElement(name = "Пол") set

    /**
     * Наименование страны гражданства (Не справочное значение)
     */
    var citizenship: String? = null
        @XmlElement(name = "ГраждТекст") set

    /**
     * Фамилия, имя, отчество умершего
     */
    var fio: Fio? = null
        @XmlElement(name = "ФИОУмер") set

    /**
     * Место рождения
     */
    var birthPlace: Place? = null
        @XmlElement(name = "МестоРожден") set

    /**
     * Место смерти
     */
    var deathPlace: Place? = null
        @XmlElement(name = "МестоСмерт") set

    /**
     * Дата рождения (календарная дата)
     */
    var birthDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаРождКаленд") set

    /**
     * Дата рождения по представленным документам
     */
    var birthDocDate: PartialDate? = null
        @XmlElement(name = "ДатаРождДок") set

    /**
     * Сведения о документе, удостоверяющем личность
     */
    var ausweis: Ausweis? = null
        @XmlElement(name = "УдЛичнФЛ") set

    /**
     * Последнее место жительства умершего
     */
    var lastPlace: LebenAdresse? = null
        @XmlElement(name = "МЖПосл") set

    /**
     * Дата смерти (календарная дата)
     */
    var deathDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаСмертКаленд") set

    /**
     * Дата смерти по представленным документам
     */
    var deathDocDate: PartialDate? = null
        @XmlElement(name = "ДатаСмертДок") set

    /**
     * Время смерти
     */
    var deathTime: LocalTime? = null
        @XmlJavaTypeAdapter(LocalTimeAdapter::class)
        @XmlElement(name = "ВремяСмерт") set

    /**
     * Сведения о причинах смерти
     */
    var cause: List<DeathCase>? = null
        @XmlElement(name = "СведПричСмерт") set

    var snils: String? = null
        @XmlAttribute(name = "СНИЛС") set

    override fun toString() = "Умерший: " +
            listOfNotNull(
                if (sex == 1) "Пол мужской" else if (sex == 2) "Пол женский" else null,
                fio, citizenship?.let { "Гражданство $it" },
                (birthDate ?: birthDocDate?.toLocalDate())?.let { "Дата рождения ${it.format()}" },
                birthPlace?.text?.let { "Место рождения $it" },
                deathPlace?.text?.let { "Место смерти $it" },
                lastPlace?.let { "Адрес места жительства $it" },
                ausweis?.let { "Сведения о документе, удостоверяющем личность: $it" },
                (deathDate ?: deathDocDate?.toLocalDate())?.let { "Дата смерти ${it.format()}" },
                deathTime?.let { "Время смерти $it" },
                cause?.firstOrNull()?.data?.name?.let { "Наименование причины смерти $it" }
            ).joinToString()
}
