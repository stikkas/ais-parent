@XmlSchema(
		elementFormDefault = XmlNsForm.QUALIFIED,
		attributeFormDefault = XmlNsForm.UNQUALIFIED,
		namespace = "urn://x-artefacts-zags-brakzakinf/root/112-54/4.0.0",
		xmlns = {
				@XmlNs(prefix = "ns1", namespaceURI = "urn://x-artefacts-zags-brakzakinf/root/112-54/4.0.0"),
				@XmlNs(prefix = "fns", namespaceURI = "urn://x-artefacts-zags-brakzakinf/types/4.0.0"),
				@XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance")
		}
)
package ru.insoft.libs.smevs.marry;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
