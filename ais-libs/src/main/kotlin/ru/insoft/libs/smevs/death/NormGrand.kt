package ru.insoft.libs.smevs.death

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement

@XmlAccessorType(XmlAccessType.FIELD)
class NormGrand {
    /**
     * Признак отсутствия сведений об организационно-распорядительном документе,
     * подтверждающем основания для запроса сведений из ЕГР ЗАГС
     */
    @XmlElement(name = "ПрСведДокОсн")
    val data: Int = 1

    @XmlAttribute(name = "КодНормОсн")
    val attr: String = "01"
}
