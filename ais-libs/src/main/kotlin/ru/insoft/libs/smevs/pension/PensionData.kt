package ru.insoft.libs.smevs.pension

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

class PensionData {

    var cause: String? = null
        @XmlElement(name = "PensionCause") set

    var startDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "StartDate") set

    var endDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "EndDate") set

    override fun toString() = """|   Основание назначения пенсии: ${cause ?: ""}
        |   Дата, с которой установлена пенсия: ${startDate?.format() ?: ""}
        |   Дата, по которую установлена пенсия: ${endDate?.format() ?: ""}
    """
}
