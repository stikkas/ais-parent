package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

/**
 * Информация о доставке заявлений в ЕГРН и результатов от ЕГРН к заявителям в рамках заявлений по Предоставлению сведений
 */
@XmlType(namespace = "http://rosreestr.ru/services/v0.1/TStatementCommons")
@XmlAccessorType(XmlAccessType.FIELD)
class TEGRNRequestDeliveryDetails {
    /**
     * Информация о способе информирования и доставки результата
     */
    var resultDeliveryMethod = TEGRNRequestResultDeliveryMethod()
}
