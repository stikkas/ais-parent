package ru.insoft.libs.smevs.sud

import ru.insoft.libs.smevs.PersonalInfo
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Судимость
 */
@XmlAccessorType(XmlAccessType.FIELD)
class Conviction {

    /**
     * Персональные данные
     */
    var personalInfo: PersonalInfo? = null

    /**
     * Осуждение
     */
    var condemnation: LegalInfo? = null

    /**
     * Наказание
     */
    var judgement: List<Judgement>? = null

    /**
     * Переквалификация
     */
    var reQualification: List<ReQualification>? = null

    /**
     * Изменение срока
     */
    var changingTerm: List<ChangingTerm>? = null

    /**
     * Освобождение
     */
    var release: Release? = null

    /**
     * Основания для снятия судимости
     */
    var removeConvictionReasons: String? = null

    /**
     * Дополнительные данные
     */
    var additionalInfo: String? = null

    override fun toString() = listOfNotNull(
        "Судимость:",
        personalInfo?.let { "Персональные данные: $it" },
        condemnation?.let { "Осуждение: $it" },
        judgement?.let { "Наказание: ${it.joinToString(";") { it.toString() }}" },
        reQualification?.let { "Переквалификация: ${it.joinToString(";") { it.toString() }}" },
        changingTerm?.let { "Изменение срока: ${it.joinToString(";") { it.toString() }}" },
        release?.let { "Освобождение: $it" },
        removeConvictionReasons?.let { "Основания для снятия судимости: $it" },
        additionalInfo?.let { "Дополнительные данные: $it" }).joinToString("\n")

}
