package ru.insoft.libs.smevs.sud

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Юридическая информация
 */
@XmlAccessorType(XmlAccessType.FIELD)
class LegalInfo {
    /**
     * Дата
     */
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var date: LocalDate? = null

    /**
     * Орган
     */
    var enforcer: String? = null

    /**
     * Основание
     */
    var cause: List<Legislation>? = null

    override fun toString(): String {
        val res = ArrayList<String>()
        date?.let {
            res.add(it.toString())
        }
        enforcer?.let {
            res.add(it)
        }
        cause?.forEach {
            res.add(it.toString())
        }
        return res.filter { it.isNotBlank() }.joinToString()
    }
}
