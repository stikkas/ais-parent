package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Квартира
 */
class Apartment {

    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_apartment") set

    /**
     * Значение
     */
    var name: String? = null
        @XmlElement(name = "name_apartment") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")
}
