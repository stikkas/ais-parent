package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Район
 */
class District {
    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_district") set

    /**
     * Наименование
     */
    var name: String? = null
        @XmlElement(name = "name_district") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")
}
