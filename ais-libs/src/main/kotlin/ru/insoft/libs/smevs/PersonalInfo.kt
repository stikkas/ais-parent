package ru.insoft.libs.smevs

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Персональные данные
 */
@XmlAccessorType(XmlAccessType.FIELD)
class PersonalInfo {
    /**
     * Установочные данные
     */
    var mainData: SettingInfo? = null

    /**
     * ИЦ
     */
    var infoCenter: List<ClassifierValue>? = null

    override fun toString(): String {
        val res = ArrayList<String>()
        mainData?.let {
            res.add(it.toString())
        }
        infoCenter?.forEach {
            res.add(it.toString())
        }
        return res.filter { it.isNotBlank() }.joinToString()
    }
}
