package ru.insoft.libs.smevs.birth

import javax.xml.bind.annotation.XmlElement

/**
 * Передаваемые сведения о государственной регистрации рождения
 */
class RegInfo {

    /**
     * Которым по счету ребенок был рожден у матери
     */
    var file: Int? = null
        @XmlElement(name = "СчетМатер") set

    /**
     * Сведения о родах, в результате которых родился ребенок
     */
    var rody: RodyInfo? = null
        @XmlElement(name = "СведРоды") set

    /**
     * Сведения о родившемся
     */
    var baby: BirthedInfo? = null
        @XmlElement(name = "СведРодившемся") set

    /**
     * Сведения о матери
     */
    var mother: Parent? = null
        @XmlElement(name = "СведМать") set

    /**
     * Сведения об отце
     */
    var father: Parent? = null
        @XmlElement(name = "СведОтец") set

    override fun toString() = """Которым по счету ребенок был рожден у матери: ${file ?: "Неизвестно"}
        |${
        listOfNotNull(
            rody?.toString(),
            baby?.toString(),
            mother?.let { "Сведения о матери: $it" },
            father?.let { "Сведения об отце: $it" }).joinToString("\n|")
    }
    |""".trimMargin()
}
