package ru.insoft.libs.smevs.marry

import java.util.*
import javax.xml.bind.annotation.*

@XmlAccessorType(XmlAccessType.FIELD)
class ReqDetails {
    /**
     * Идентификатор документа
     */
    @XmlAttribute(name = "ИдДок", required = true)
    val id = UUID.randomUUID()

    /**
     * Сведения о государственной регистрации АГС о заключении брака (расторжении брака), в отношении которого сформирован запрос
     */
//    @XmlElement(name = "СведАГС")
    @XmlTransient
    var agsInfo: AgsDetails? = null

    /**
     * Сведения о физическом лице, в отношении которого сформирован запрос
     */
    @XmlElement(name = "СведФЛ")
    var fizInfo: FizDetails? = null

    /**
     * Признак отсутствия сведений о субъекте Российской Федерации, где зарегистрирован акт гражданского состояния
     */
    @XmlElement(name = "ПрРегионРегАГС")
    val regAgs: Int = 1
}
