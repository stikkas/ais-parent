package ru.insoft.libs.smevs.divorce

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о документе, удостоверяющем личность физического лица (с признаком отсутствия)
 */
@XmlType(namespace = "urn://x-artefacts-zags-brakrastinf/types/4.0.0")
class Ausweis {

    /**
     * Код вида документа, удостоверяющего личность
     */
    var code: String? = null
        @XmlElement(name = "КодВидДок") set
        get() = when (field) {
            "1" -> "ПАСПОРТ СССР"
            "3" -> "СВИДЕТЕЛЬСТВО О РОЖДЕНИИ"
            "4" -> "УДОСТОВЕРЕНИЕ ЛИЧНОСТИ ОФИЦЕРА"
            "5" -> "СПРАВКА"
            "6" -> "ПАСПОРТ МИНМОРФЛОТА"
            "7" -> "ВОЕННЫЙ БИЛЕТ"
            "10" -> "ИНОСТРАННЫЙ ПАСПОРТ"
            "12" -> "ВИД НА ЖИТЕЛЬСТВО"
            "14" -> "ВРЕМЕННОЕ УДОСТОВЕРЕНИЕ ЛИЧНОСТИ"
            "15" -> "РАЗРЕШЕНИЕ НА ВРЕМЕННОЕ ПРОЖИВАНИЕ"
            "21" -> "ПАСПОРТ РФ"
            "22" -> "ЗАГРАНИЧНЫЙ ПАСПОРТ"
            "23" -> "ИНОСТРАННОЕ СВИДЕТЕЛЬСТВО О РОЖДЕНИИ"
            "24" -> "УДОСТОВЕРЕНИЕ ЛИЧНОСТИ"
            else -> "Неизвестный"
        }

    /**
     * Серия документа, удостоверяющего личность
     */
    var series: String? = null
        @XmlElement(name = "СерДок") set

    /**
     * Номер документа, удстоверяющего личность
     */
    var number: String? = null
        @XmlElement(name = "НомДок") set

    /**
     * Дата выдачи документа, удостоверяющего личность (календарная дата)
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаДокКаленд") set

    /**
     * Дата выдачи документа, удостоверяющего личность (неполная дата)
     */
    var partDate: PartialDate? = null
        @XmlElement(name = "ДатаДокНеполн") set

    /**
     * Наименование органа, выдавшего документ, удостоверяющий личность
     */
    var whogive: String? = null
        @XmlElement(name = "ВыдДок") set

    /**
     * Код подразделения органа, выдавшего документ, удостоверяющий личность
     */
    var whoCode: String? = null
        @XmlElement(name = "КодВыдДок") set

    override fun toString() = listOfNotNull(code, series, number, (date ?: partDate?.toLocalDate())?.format(), whogive, whoCode).joinToString(" ")
}
