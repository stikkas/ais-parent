@XmlSchema(
		elementFormDefault = XmlNsForm.QUALIFIED,
		attributeFormDefault = XmlNsForm.UNQUALIFIED,
		namespace = "urn://x-artefacts-zags-brakrastinf/root/112-55/4.0.0",
		xmlns = {
				@XmlNs(prefix = "ns1", namespaceURI = "urn://x-artefacts-zags-brakrastinf/root/112-55/4.0.0"),
				@XmlNs(prefix = "usch", namespaceURI = "http://www.unisoftware.ru/schematron-extensions"),
				@XmlNs(prefix = "sch", namespaceURI = "http://purl.oclc.org/dsdl/schematron"),
				@XmlNs(prefix = "fnst", namespaceURI = "urn://x-artefacts-zags-brakrastinf/types/4.0.0"),
				@XmlNs(prefix = "tns", namespaceURI = "urn://x-artefacts-zags-brakrastinf/root/112-55/4.0.0"),
				@XmlNs(prefix = "xs", namespaceURI = "http://www.w3.org/2001/XMLSchema")
		}
)
package ru.insoft.libs.smevs.divorce;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
