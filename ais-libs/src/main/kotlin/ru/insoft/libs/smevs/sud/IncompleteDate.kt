package ru.insoft.libs.smevs.sud

import javax.xml.bind.annotation.XmlType

/**
 * Дата рождения
 */
@XmlType(propOrder = ["year", "month", "day"])
class IncompleteDate {
    var year: Int? = null

    var month: Int? = null
        get() = field?.let {
            if (it == 0) null else it
        }

    var day: Int? = null
        get() = field?.let {
            if (it == 0) null else it
        }

    override fun toString() = listOf(
        day?.let { if (it < 10) "0$it" else "$it" },
        month?.let { if (it < 10) "0$it" else "$it" }, year?.toString()
    ).filterNot { it.isNullOrBlank() }
        .joinToString(".")
}
