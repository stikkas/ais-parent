package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Населённый пункт
 */
class Locality {

    /**
     * Тип
     */
    var type: String? = null
        @XmlElement(name = "type_locality") set

    /**
     * Наименование
     */
    var name: String? = null
        @XmlElement(name = "name_locality") set

    override fun toString() = listOfNotNull(type, name).joinToString(" ")
}
