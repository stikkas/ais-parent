package ru.insoft.libs.smevs.birth

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о государственной регистрации рождения
 */
class RegBirthInfo {

    /**
     * Дата составления актовой  записи о расторжении брака
     */
    var regDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаЗапис") set

    /**
     * Дата составления актовой  записи о расторжении брака по представленным документам (неполная дата)
     */
    var regDocDate: PartialDate? = null
        @XmlElement(name = "ДатаЗаписДок") set

    /**
     * Сведения об органе ЗАГС, которым произведена государственная регистрация расторжения брака
     */
    var organ: BagsType? = null
        @XmlElement(name = "ОрганЗАГС") set

    /**
     * Сведения о выданном Свидетельстве о рождении
     */
    var cert: List<BirthCert>? = null
        @XmlElement(name = "СвидетРожд") set

    /**
     * Передаваемые сведения о государственной регистрации рождения
     */
    var regInfo: RegInfo? = null
        @XmlElement(name = "ПрдСведРег") set

    /**
     * Сведения о внесении в ЕГР ЗАГС сведений о документе компетентного органа иностранного государства (Раздел 2)
     */
    var reg2Info: AgsInostrInfo? = null
        @XmlElement(name = "СВедЕГРРазд2") set

    /**
     * Раздел ЕГР ЗАГС, в котором содержатся сведения о регистрации акта гражданского состояния
     */
    var part: Int? = null
        @XmlAttribute(name = "РазделЕГР") set

    /**
     * Номер актовой записи о рождении, содержащийся в ЕГР ЗАГС
     */
    var egrRecNumber: String? = null
        @XmlAttribute(name = "НомерЗаписЕГР") set

    /**
     * Номер, указанный в свидетельстве о рождении
     */
    var agsRecNumber: String? = null
        @XmlAttribute(name = "НомАГССвид") set

    override fun toString() =
        """Раздел ЕГР ЗАГС: ${if (part == 1) "Первый раздел" else if (part == 2) "Второй раздел" else "Раздел $part"}
        |Номер актовой записи о рождении, содержащийся в ЕГР ЗАГС: ${egrRecNumber ?: ""}
        |Номер актовой записи, указанный в свидетельстве о рождении: ${agsRecNumber ?: ""}
        |Дата составления актовой записи о рождении: ${(regDate ?: regDocDate?.toLocalDate())?.format() ?: ""}
        |Сведения об органе ЗАГС, которым произведена государственная регистрация акта гражданского состояния: ${organ?.name ?: ""}
        |${cert?.firstOrNull() ?: "Нет сведений о выданном Свидетельстве о расторжении брака"}
        |${regInfo ?: ""}
        |${reg2Info ?: ""}
    |""".trimMargin()

}
