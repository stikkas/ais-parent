package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Площадь, в кв. метрах
 */
class Area {
    var area: Double? = null
        @XmlElement set
}
