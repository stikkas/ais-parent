package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Доля в праве общей долевой собственности пропорциональна размеру общей площади помещений,
 * машино-мест, не переданных участникам долевого строительства, а также помещений, машино-мест
 */
class BuilderShareWithObject {
    var info: String? = null
        @XmlElement(name = "builder_share_with_object_info") set

    /**
     * Сведения о помещениях, машино-местах
     */
    var objects: InfoObjects? = null
        @XmlElement(name = "info_objects") set

    override fun toString() =
        listOfNotNull(info?.let { "Доля в праве общей долевой собственности пропорциональна размеру общей площади помещений, машино-мест, не переданных участникам долевого строительства, а также помещений, машино-мест: $it" },
            objects?.let { "Сведения о помещении, машино-месте. Кадастровый номер: $it" }
        ).joinToString("\n")
}
