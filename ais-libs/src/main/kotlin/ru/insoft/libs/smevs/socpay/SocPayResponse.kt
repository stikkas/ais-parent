package ru.insoft.libs.smevs.socpay

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

@XmlRootElement(name = "BenefitsFormInfoResponse")
@XmlAccessorType(XmlAccessType.FIELD)
class SocPayResponse {
    @XmlElement(name = "Snils")
    var snils: String? = null

    @XmlElement(name = "FamilyName", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    lateinit var lastName: String

    @XmlElement(name = "FirstName", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    lateinit var firstName: String

    @XmlElement(name = "Patronymic", namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    var middleName: String? = null

    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    @XmlElement(name = "BirthDate")
    var birthDate: LocalDate? = null

    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    @XmlElement(name = "AppDate")
    var date: LocalDate? = null

    @XmlElement(name = "TerOrganName")
    var organName: String? = null

    @XmlElement(name = "TerOrganCode")
    var organCode: String? = null

    @XmlElement(name = "IssuingReason")
    var reason: String? = null

    @XmlElement(name = "RefusalReason")
    var refuse: String? = null

    override fun toString() = listOfNotNull(snils?.let { "СНИЛС: $it" },
        listOfNotNull(lastName, firstName, middleName).joinToString(" ", "ФИО: "),
        birthDate?.let { "Дата рождения: ${it.format()}" },
        date?.let { "Дата, на которую предоставляются сведения об установленных выплатах: ${it.format()}" }
    ).joinToString("\n")
}
