package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/commons/Subjects",
    propOrder = ["code", "type", "name"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class TAddressElement2 {

    /**
     * Код
     */
    var code: Int? = null

    /**
     * Сокращенное наименование типа адресного элемента
     */
    var type: String? = null

    /**
     * Наименование адресного элемента
     */
    var name: String? = null
}
