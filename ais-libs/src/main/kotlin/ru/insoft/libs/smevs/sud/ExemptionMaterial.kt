package ru.insoft.libs.smevs.sud

import ru.insoft.libs.smevs.PersonalInfo
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Отказной материал
 */
@XmlAccessorType(XmlAccessType.FIELD)
class ExemptionMaterial {

    /**
     * Персональные данные
     */
    var personalInfo: PersonalInfo? = null

    /**
     * Отказ
     */
    var denial: LegalInfo? = null

    /**
     * Причина
     */
    var reason: ReasonType? = null

    /**
     * Дополнительные данные
     */
    var additionalInfo: String? = null

    override fun toString() = listOfNotNull(
        "Отказной материал:",
        personalInfo?.let { "Персональные данные: $it" },
        denial?.let { "Отказ: $it" },
        reason?.let { "Причина: $it" },
        additionalInfo?.let { "Дополнительные данные: $it" }
    ).joinToString("\n")

}
