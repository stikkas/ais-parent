package ru.insoft.libs.smevs.marry

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о легализации документа о проставлении апостиля
 */
class DocLegalisation {

    /**
     * Орган, которым осуществлена легализация документа/проставлен апостиль
     */
    var organ: String? = null
        @XmlElement(name = "ОрганЛегал") set

    /**
     * Дата легализации документа /проставления апостиля
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаЛегал") set

    /**
     * Номер записи о легализации документа/номер апостиля
     */
    var number: String? = null
        @XmlElement(name = "НомерЛегал") set

}
