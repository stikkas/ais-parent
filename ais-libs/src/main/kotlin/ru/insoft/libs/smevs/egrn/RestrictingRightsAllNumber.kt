package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Ограничиваемые права
 */
class RestrictingRightsAllNumber {
    var rights: List<RightRecordNumber>? = null
        @XmlElement(name = "restricting_right") set

    override fun toString() = rights?.map {
        listOfNotNull(it.number?.let { "Ограничиваемое право. Номер реестровой записи: $it" },
            it.rightNumber?.let { "Ограничиваемое право. Номер регистрации вещного права: $it" }).joinToString("\n")
    }?.filter { it.isNotEmpty() }?.joinToString("\n") ?: ""
}
