package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlType

@XmlType(namespace = "http://rosreestr.ru/services/v0.1/commons/Subjects")
@XmlAccessorType(XmlAccessType.FIELD)
class TBaseNativeForeignParams {
    /**
     * Данные по российским юридическим лицам
     */
    var nativeOrgParams: TNativeOrgParams? = null
}
