package ru.insoft.libs.smevs.pass

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

@XmlRootElement(name = "passportValidityResponse")
class PassResponse {
    var status: String? = null
        @XmlElement(name = "docStatus") set
        get() = when (field) {
            "300" -> "Паспорт действителен"
            "301" -> "Паспорт недействителен"
            "302" -> "Информация о паспорте не найдена"
            else -> ""
        }

    var reason: String? = null
        @XmlElement(name = "invalidityReason") set
        get() = when (field) {
            "601" -> "Истёк срок действия"
            "602" -> "Заменен на новый"
            "603" -> "Выдан с нарушением"
            "604" -> "Числится в розыске"
            "605" -> "Изъят, уничтожен"
            "606" -> "В связи со смертью владельца"
            "607" -> "Технический брак"
            else -> null
        }

    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "invalidityDate") set

    override fun toString() =
        status + (reason?.let { "\n$it" } ?: "") + (date?.let { "\n$it" } ?: "")
}
