package ru.insoft.libs.smevs.sud

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Ответы на запросы про судимость
 */
@XmlAccessorType(XmlAccessType.FIELD)
class Records {
    var conviction: List<Conviction>? = null

    var impeachment: List<Impeachment>? = null

    var manhunt: Manhunt? = null

    var exemptionMaterial: List<ExemptionMaterial>? = null

    /**
     * Дополнительные данные
     */
    var additionalInfo: String? = null

    override fun toString(): String {
        val res = ArrayList<String>()
        conviction?.forEach { res.add(it.toString()) }
        impeachment?.forEach { res.add(it.toString()) }
        manhunt?.let { res.add(it.toString()) }
        exemptionMaterial?.forEach { res.add(it.toString()) }
        additionalInfo?.let { res.add("Дополнительные данные: $it") }
        return res.filter { it.isNotBlank() }.joinToString("\n\n")
    }
}
