package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Адрес (описание местоположения) до уровня населённого пункта
 */
class AddressCity {
    /**
     * Код ФИАС
     */
    var fias: String? = null
        @XmlElement set

    /**
     * ОКАТО
     */
    var okato: String? = null
        @XmlElement set

    /**
     * Код КЛАДР
     */
    var kladr: String? = null
        @XmlElement set

    /**
     * ОКТМО
     */
    var oktmo: String? = null
        @XmlElement set

    /**
     * Почтовый индекс
     */
    var zip: String? = null
        @XmlElement(name = "postal_code") set

    /**
     * Код региона
     */
    var region: CDDict? = null
        @XmlElement set

    /**
     * Район
     */
    var district: District? = null
        @XmlElement set

    /**
     * Муниципальное образование
     */
    var city: City? = null
        @XmlElement set

    /**
     * Городской район
     */
    var urban: UrbanDistrict? = null
        @XmlElement(name = "urban_district") set

    /**
     * Сельсовет
     */
    var selsovet: SovietVillage? = null
        @XmlElement(name = "soviet_village") set

    /**
     * Населённый пункт
     */
    var locality: Locality? = null
        @XmlElement set

    override fun toString() = listOfNotNull(
        fias, okato, kladr, oktmo, zip, region?.value, district?.toString(), city?.toString(), urban?.toString(),
        selsovet?.toString(), locality?.toString()
    ).filter { it.isNotEmpty() }.joinToString()
}
