package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Не указан размер доли в праве общей долевой собственности
 * на общее имущество, в том числе на земельный участок, собственников помещений,
 * машино-мест в здании, если объектом недвижимости является помещение, машино-место в здании
 */
class ShareUnknown {
    /**
     * Доля в праве общей долевой собственности пропорциональна размеру общей площади
     */
    var desc: String? = null
        @XmlElement(name = "share_description") set

    /**
     * Кадастровый номер для расчета пропорций
     */
    var number: String? = null
        @XmlElement(name = "proportion_cad_number") set

    override fun toString() =
        listOfNotNull("Не указан размер доли в праве общей долевой собственности на общее имущество:",
            desc?.let { "Описание размера доли: $it" },
            number?.let { "Кадастровый номер для расчета пропорций: $it" }).joinToString("\n")
}
