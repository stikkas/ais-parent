package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

class TopRequisites {
    var rights: String? = null
        @XmlElement(name = "organ_registr_rights") set

    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "date_formation") set

    var number: String? = null
        @XmlElement(name = "registration_number") set

}
