package ru.insoft.libs.smevs.death

import java.util.*
import javax.xml.bind.annotation.*

/**
 * Сведения, содержащиеся в запросе
 */
@XmlAccessorType(XmlAccessType.FIELD)
class DeathRequestDetails {
    /**
     * Идентификатор документа
     */
    @XmlAttribute(name = "ИдДок", required = true)
    val id = UUID.randomUUID()

    /**
     * Сведения о государственной регистрации АГС о смерти, в отношении которого сформирован запрос
     */
//    @XmlElement(name = "СведАГС")
    @XmlTransient
    var ags: Ags? = null

    /**
     * Сведения о физическом лице, в отношении которого сформирован запрос
     */
    @XmlElement(name = "СведФЛ")
    var fizInfo: ManDetails? = null

    /**
     * Признак отсутствия сведений о субъекте Российской Федерации, где зарегистрирован акт гражданского состояния
     */
    @XmlElement(name = "ПрРегионРегАГС")
    val regAgs: Int = 1
}
