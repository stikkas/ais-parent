package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.*
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Документ удостоверяющий личность типизированный
 */
@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/commons/Documents",
    propOrder = ["documentTypes", "name", "number", "issueDate", "attachment", "series", "issuer"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class IdDocumentRestr {
    @XmlAttribute(name = "_id")
    var id: String = "1"

    /**
     * Информация по кодам документа
     */
    var documentTypes: DocumentTypes? = null

    /**
     * Наименование документа
     */
    var name: String? = null

    /**
     * Номер документа
     */
    var number: String? = null

    /**
     * Дата выдачи
     */
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var issueDate: LocalDate? = null

    /**
     * Описание приложенного файла
     */
    var attachment: Attachment? = null

    /**
     * Серия документа
     */
    var series: String? = null

    @XmlTransient
    var docId: String? = null

    /**
     * Организация, выдавшая документ, или автор документа
     */
    var issuer: IssuerInfo? = null

    override fun toString() =
        listOfNotNull(name, series, number, issueDate?.format()?.let { "от $it" }).joinToString(" ")
}
