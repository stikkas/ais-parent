package ru.insoft.libs.smevs.birth

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * Адрес места жительства
 */
@XmlType(namespace = "urn://x-artefacts-zags-rogdinf/types/4.0.1")
class LebenAdresse {
    // Что-то одно должно быть

    /**
     * Адрес места жительства на территории Российской Федерации
     */
    var rfAdr: LebenOrt? = null
        @XmlElement(name = "АдрМЖРФ") set

    /**
     * Адрес места жительства за пределами Российской Федерации
     */
    var outAdr: AuserLebenOrt? = null
        @XmlElement(name = "АдрМЖИн") set

    /**
     * Не классифицируемый адрес места жительства
     */
    var textAdr: String? = null
        @XmlElement(name = "АдрМЖКонв") set
}
