package ru.insoft.libs.smevs.birth

import ru.insoft.libs.format
import javax.xml.bind.annotation.*

/**
 * Запрос на предоставление сведений об актах гражданского состояния о рождении,
 * содержащихся в Едином государственном реестре записей актов гражданского состояния
 */
@XmlRootElement(name = "ROGDINFRequest")
@XmlAccessorType(XmlAccessType.FIELD)
class BirthRequest {
    /**
     * Количество документов в представленном файле
     */
    @XmlAttribute(required = true, name = "КолДок")
    val docsCount: Int = 1

    /**
     * Тип акта гражданского состояния, в отношении которого сформирован запрос
     */
    @XmlAttribute(required = true, name = "ТипАГС")
    val actType: String = "01"

    /**
     * Идентификатор запроса, сформированный запрашивающей стороной
     */
    @XmlAttribute(required = true, name = "ИдЗапрос")
    var id: String? = null

    /**
     * Сведения о нормативно-правовых основаниях запрашивающей стороны
     * для получения сведений из ЕГР ЗАГС об актах гражданского состояния о рождении
     */
    @XmlElement(name = "СведОсн")
    val mainInfo = NormGrand()

    /**
     * Сведения, содержащиеся в запросе
     */
    @XmlElement(name = "СведЗапрос")
    var details: BirthRequestDetails? = null

    override fun toString() = """СНИЛС: ${details?.fizInfo?.snils ?: ""}
        |ФИО: ${details?.fizInfo?.fio ?: ""}
        |Дата рождения: ${details?.fizInfo?.birthDate?.format() ?: ""}
        |""".trimMargin()
//    |Документ, удостоверяющий личность: ${details?.fizInfo?.ausweis ?: ""}
}
