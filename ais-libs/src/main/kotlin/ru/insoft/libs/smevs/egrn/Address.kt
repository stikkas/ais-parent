package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Адрес (описание местоположения) полный
 */
class Address {
    /**
     * Уровень населенного пункта
     */
    var level: AddressCity? = null
        @XmlElement(name = "level_settlement") set

    /**
     * Детализированный уровень
     */
    var details: DetailedLevel? = null
        @XmlElement(name = "detailed_level") set

    override fun toString() =
        listOfNotNull(level?.toString(), details?.toString()).filter { it.isNotEmpty() }.joinToString()
}
