package ru.insoft.libs.smevs.marry

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Супруг
 */
@XmlType(namespace = "urn://x-artefacts-zags-brakzakinf/types/4.0.0")
class Spouse {
    var lastNameBefore: String? = null
        @XmlElement(name = "ФамилияДо") set

    var lastNameAfter: String? = null
        @XmlElement(name = "ФамилияПосле") set

    var firstName: String? = null
        @XmlElement(name = "Имя") set

    var middleName: String? = null
        @XmlElement(name = "Отчество") set

    var citizenship: String? = null
        @XmlElement(name = "ГраждТекст") set

    /**
     * Дата рождения (календарная дата)
     */
    var birthDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаРождКаленд") set

    /**
     * Дата рождения по представленным документам
     */
    var birthDateDoc: PartialDate? = null
        @XmlElement(name = "ДатаРождДок") set

    /**
     * Место рождения
     */
    var birthPlace: Place? = null
        @XmlElement(name = "МестоРожден") set

    /**
     * Адрес места жительства
     */
    var address: LebenAdresse? = null
        @XmlElement(name = "АдрМЖ") set

    /**
     * Сведения о документе, удостоверяющем личность
     */
    var ausweis: PersonalDoc? = null
        @XmlElement(name = "УдЛичнФЛ") set

    var snils: String? = null
        @XmlAttribute(name = "СНИЛС") set

    override fun toString() = listOfNotNull(
        snils?.let { "СНИЛС $it" },
        lastNameBefore?.let { "Фамилия до заключения брака $it" },
        lastNameAfter?.let { "Фамилия после заключения брака $it" },
        firstName?.let { "Имя $it" },
        middleName?.let { "Отчество $it" },
        citizenship?.let { "Гражданство $it" },
        (birthDate ?: birthDateDoc?.toLocalDate())?.let { "Дата рождения ${it.format()}" },
        birthPlace?.text?.let { "Место рождения $it" },
        (address?.rfAdr?.addrOrNull() ?: address?.outAdr?.text
        ?: address?.textAdr)?.let { "Адрес места жительства $it" },
        ausweis?.let { "Сведения о документе, удостоверяющем личность: $it" }
    ).joinToString()
}
