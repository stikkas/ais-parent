package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Размер доли в праве
 */
@XmlAccessorType(XmlAccessType.FIELD)
class Share {

    /**
     * Числитель
     */
    var numerator: Int? = null

    /**
     * Знаменатель
     */
    var denominator: Int? = null

    override fun toString() = "Размер доли в праве: " + listOfNotNull(numerator, denominator).joinToString("/")
}
