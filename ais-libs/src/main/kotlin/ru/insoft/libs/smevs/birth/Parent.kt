package ru.insoft.libs.smevs.birth

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о родителе
 */
@XmlType(namespace = "urn://x-artefacts-zags-rogdinf/types/4.0.1")
class Parent {

    /**
     * Наименование страны гражданства (Не справочное значение)
     */
    var citizenship: String? = null
        @XmlElement(name = "ГраждТекст") set

    var fio: Fio? = null
        @XmlElement(name = "ФИО") set

    /**
     * Дата рождения (календарная дата)
     */
    var birthDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаРождКаленд") set

    /**
     * Дата рождения по представленным документам
     */
    var birthDateDoc: PartialDate? = null
        @XmlElement(name = "ДатаРождДок") set

    /**
     * Место рождения
     */
    var birthPlace: Place? = null
        @XmlElement(name = "МестоРожден") set

    /**
     * Сведения о документе, удостоверяющем личность
     */
    var ausweis: Ausweis? = null
        @XmlElement(name = "УдЛичнФЛ") set

    /**
     * Адрес места жительства
     */
    var address: LebenAdresse? = null
        @XmlElement(name = "АдрМЖ") set

    var snils: String? = null
        @XmlAttribute(name = "СНИЛС") set

    override fun toString() = listOfNotNull(
        snils?.let { "СНИЛС $it" }, fio?.toString(),
        (birthDate ?: birthDateDoc?.toLocalDate())?.let { "Дата рождения ${it.format()}" },
        citizenship?.let { "Гражданство $it" },
        birthPlace?.text?.let { "Место рождения $it" },
        ausweis?.let { "Сведения о документе, удостоверяющем личность: $it" },
        (address?.rfAdr?.addrOrNull() ?: address?.outAdr?.text
        ?: address?.textAdr)?.let { "Адрес места жительства $it" }
    ).joinToString()
}
