package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения о праве
 */
class RightRecordIndivid {
    /**
     * Даты государственной регистрации
     */
    var info: RecordInfo? = null
        @XmlElement(name = "record_info") set

    /**
     * Общие сведения о правах
     */
    var data: RightDataAndCancel? = null
        @XmlElement(name = "right_data") set

    /**
     * Документы-основания
     */
    var docs: UnderlyingDocumentsOut? = null
        @XmlElement(name = "underlying_documents") set

    override fun toString() = listOfNotNull(info, data, docs).joinToString("\n")
}
