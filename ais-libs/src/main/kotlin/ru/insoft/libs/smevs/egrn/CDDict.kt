package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Справочник НСИ
 */
@XmlAccessorType(XmlAccessType.FIELD)
class CDDict {

    /**
     * Код справочника НСИ
     */
    var code: String? = null

    /**
     * Текстовое значение, соответствующее коду справочника НСИ
     */
    var value: String? = null
}
