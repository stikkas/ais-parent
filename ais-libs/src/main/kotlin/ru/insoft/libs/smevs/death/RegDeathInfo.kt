package ru.insoft.libs.smevs.death

import ru.insoft.libs.format
import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения о государственной регистрации смерти
 */
class RegDeathInfo {

    /**
     * Дата составления актовой  записи о смерти
     */
    var regDate: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlElement(name = "ДатаЗапис") set

    /**
     * Дата составления актовой записи о смерти по представленным документам (неполная дата)
     */
    var regDocDate: PartialDate? = null
        @XmlElement(name = "ДатаЗаписДок") set

    /**
     * Сведения об органе ЗАГС, которым произведена государственная регистрация смерти
     */
    var organ: DagsType? = null
        @XmlElement(name = "ОрганЗАГС") set

    /**
     * Сведения о выданном Свидетельстве о смерти
     */
    var cert: List<DeathCert>? = null
        @XmlElement(name = "СвидетСмерт") set

    /**
     * Передаваемые сведения о государственной регистрации смерти
     */
    var regInfo: RegInfo? = null
        @XmlElement(name = "ПрдСведРег") set

    /**
     * Сведения о внесении в ЕГР ЗАГС сведений о документе компетентного органа иностранного государства (Раздел 2)
     */
    var reg2Info: AgsInostrInfo? = null
        @XmlElement(name = "СВедЕГРРазд2") set

    /**
     * Раздел ЕГР ЗАГС, в котором содержатся сведения о регистрации акта гражданского состояния
     */
    var part: Int? = null
        @XmlAttribute(name = "РазделЕГР") set

    /**
     * Номер актовой записи о рождении, содержащийся в ЕГР ЗАГС
     */
    var egrRecNumber: String? = null
        @XmlAttribute(name = "НомерЗаписЕГР") set

    /**
     * Номер, указанный в свидетельстве о рождении
     */
    var agsRecNumber: String? = null
        @XmlAttribute(name = "НомАГССвид") set

    override fun toString() =
        """Раздел ЕГР ЗАГС: ${if (part == 1) "Первый раздел" else if (part == 2) "Второй раздел" else "Раздел $part"}
        |Номер актовой записи о смерти, содержащийся в ЕГР ЗАГС: ${egrRecNumber ?: ""}
        |Номер актовой записи, указанный в свидетельстве о смерти: ${agsRecNumber ?: ""}
        |Дата составления записи акта о смерти: ${(regDate ?: regDocDate?.toLocalDate())?.format() ?: ""}
        |Сведения об органе ЗАГС, которым произведена государственная регистрация акта гражданского состояния: ${organ?.name ?: ""}
        |${cert?.firstOrNull() ?: "Нет сведений о выданном Свидетельстве о смерти"}
        |${regInfo ?: ""}
        |${reg2Info ?: ""}
    |""".trimMargin()

}
