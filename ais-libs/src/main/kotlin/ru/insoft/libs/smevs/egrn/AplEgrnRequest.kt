package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.format
import javax.xml.bind.annotation.*

/**
 * Запрос о предоставлении сведений
 */
@XmlType(propOrder = ["header", "declarant", "requestDetails", "deliveryDetails", "statementAgreements"])
@XmlRootElement(name = "EGRNRequest")
@XmlAccessorType(XmlAccessType.FIELD)
class AplEgrnRequest {
    /**
     * Идентификатор запроса в системе источнике запроса
     */
    @XmlAttribute(name = "_id")
    var id: String? = null

    /**
     * Заголовок запроса на предоставление сведений из ЕГРН
     */
    var header: TEGRNRequestHeader? = null

    /**
     * Описание заявителя, представителя заявителя
     */
    var declarant: TEGRNRequestDeclarant? = null

    /**
     * Детали запроса на предоставление сведений из ЕГРН
     */
    var requestDetails: TEGRNRequestDetails? = null

    /**
     * Описание способа получения запроса и доставки результата
     */
    var deliveryDetails = TEGRNRequestDeliveryDetails()

    /**
     * Соглашения с заявителем
     */
    var statementAgreements = TEGRNRequestAgreements()

    override fun toString() = """${requestDetails?.requestEGRNDataAction?.extractSubject?.owner?.person ?: ""}
        |Документ, удостоверяющий личность: ${header?.appliedDocument?.idDocument}
        |Дата, на которую запрошены сведения: ${requestDetails?.requestEGRNDataAction?.extractSubject?.period?.dateEnd?.format()}
        |""".trimMargin()
}
