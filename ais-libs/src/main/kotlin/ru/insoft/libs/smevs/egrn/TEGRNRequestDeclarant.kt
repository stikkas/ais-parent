package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlType

/**
 * Описание заявителя, представителя заявителя
 */
@XmlType(
    namespace = "http://rosreestr.ru/services/v0.1/commons/Subjects",
    propOrder = ["organization", "declarantKind"]
)
@XmlAccessorType(XmlAccessType.FIELD)
class TEGRNRequestDeclarant {
    @XmlAttribute(name = "_id")
    var id: String = "2"

    /**
     * Юридическое лицо
     */
    var organization: TEGRNRequestOrganization? = null

    /**
     * Тип заявителя
     */
    var declarantKind = "357018000000"
}
