package ru.insoft.libs.smevs.death

import ru.insoft.libs.format
import javax.xml.bind.annotation.*

/**
 * Запрос на предоставление сведений об актах гражданского состояния о смерти,
 * содержащихся в Едином государственном реестре записей актов гражданского состояния
 */
@XmlRootElement(name = "FATALINFRequest")
@XmlAccessorType(XmlAccessType.FIELD)
class DeathRequest {
    /**
     * Количество документов в представленном файле
     */
    @XmlAttribute(required = true, name = "КолДок")
    val docsCount: Int = 1

    /**
     * Тип акта гражданского состояния, в отношении которого сформирован запрос
     */
    @XmlAttribute(required = true, name = "ТипАГС")
    val actType: String = "07"

    /**
     * Идентификатор запроса, сформированный запрашивающей стороной
     */
    @XmlAttribute(required = true, name = "ИдЗапрос")
    var id: String? = null

    /**
     * Сведения о нормативно-правовых основаниях запрашивающей стороны
     * для получения сведений из ЕГР ЗАГС об актах гражданского состояния о рождении
     */
    @XmlElement(name = "СведОсн")
    val mainInfo = NormGrand()

    /**
     * Сведения, содержащиеся в запросе
     */
    @XmlElement(name = "СведЗапрос")
    var details: DeathRequestDetails? = null

    override fun toString() = """ФИО: ${details?.fizInfo?.fio ?: ""}
        |Дата рождения: ${details?.fizInfo?.birthDate?.format() ?: ""}
        |СНИЛС: ${details?.fizInfo?.snils ?: ""}
        |""".trimMargin()
//    |${
//        details?.ags?.let { a ->
//            listOfNotNull(a.nomer?.let { "Номер записи акта гражданского состояния: $it" },
//                a.date?.format()?.let { "Дата записи акта гражданского состояния: $it" },
//                a.organ?.let { "Орган ЗАГС: $it" }
//            ).joinToString("\n|")
//        } ?: ""
//    }

//    |Документ, удостоверяющий личность: ${details?.fizInfo?.ausweis ?: ""}
}
