package ru.insoft.libs.smevs.marry

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement

@XmlAccessorType(XmlAccessType.FIELD)
class MainInfoType {
    @XmlElement(name = "ПрСведДокОсн")
    val data: Int = 1

    @XmlAttribute(name = "КодНормОсн")
    val attr: String = "01"
}
