package ru.insoft.libs.smevs

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Временные показатели
 */
@XmlAccessorType(XmlAccessType.FIELD)
class TimeType {
    /**
     * Период
     * если нет то будет элемент "forever" - т.е. без ограничения
     */
    var period: PeriodType? = null

    override fun toString() = period?.toString() ?: "без ограничения"
}
