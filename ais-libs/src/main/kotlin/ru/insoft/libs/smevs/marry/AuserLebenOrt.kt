package ru.insoft.libs.smevs.marry

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement

/**
 * Адрес места жительства за пределами Российской Федерации
 */
@XmlAccessorType(XmlAccessType.FIELD)
class AuserLebenOrt {
    /**
     * Адрес места жительства физического лица, проживающего за пределами Российской Федерации,
     * указанный в печатной форме Свидетельства и актовой записи
     */
    @XmlElement(name = "АдрТекст")
    var text: String? = null

    /**
     * Код страны
     * Принимает значение цифрового кода в соответствии с Общероссийским классификатором стран мира ОК 025-2001 (ОКСМ)
     */
    @XmlElement(name = "ОКСМ")
    var countryCode: String? = null
}
