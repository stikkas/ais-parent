package ru.insoft.libs.smevs.divorce

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения о внесении в ЕГР ЗАГС сведений о документе компетентного органа иностранного государства (Раздел 2)
 */
class AgsInostrInfo {

    /**
     * Сведения о документе компетентного органа иностранного государства
     */
    var docInfo: DocInfoFremd? = null
        @XmlElement(name = "СвДокИн") set

    override fun toString() = docInfo?.toString() ?: ""
}

