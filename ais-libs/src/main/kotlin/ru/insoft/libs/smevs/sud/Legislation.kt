package ru.insoft.libs.smevs.sud

import ru.insoft.libs.smevs.ClassifierValue
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

/**
 * Закон
 */
@XmlAccessorType(XmlAccessType.FIELD)
class Legislation {
    /**
     * Кодекс
     */
    var code: ClassifierValue? = null

    /**
     * Статья
     */
    var article: String? = null

    /**
     * Часть
     */
    var part: String? = null

    /**
     * Пункт
     */
    var paragraph: String? = null

    override fun toString() =
        listOf(code?.toString(), article, part, paragraph).filterNot { it.isNullOrBlank() }.joinToString()

}
