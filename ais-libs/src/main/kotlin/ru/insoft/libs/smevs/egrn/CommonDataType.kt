package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Сведения об объекте недвижимости (вид, кадастровый номер)
 */
class CommonDataType {
    /**
     * Вид объекта недвижимости
     */
    var type: CDDict? = null
        @XmlElement set

    /**
     * Кадастровый номер
     */
    var number: String? = null
        @XmlElement(name = "cad_number") set
}
