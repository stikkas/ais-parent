package ru.insoft.libs.smevs.marry

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Сведения об органе ЗАГС Российской Федерации или консульском учреждении Российской Федерации,
 * куда направлено уведомление о факте регистрации компетентным органом
 * иностранного государства АГС в отношении гражданина РФ
 */
class ConsulOrganZags {
    var organ: AgsType? = null
        @XmlElement(name = "ОрганЗАГС") set

    /**
     * Фамилия, имя, отчество ответственного лица, которым принято уведомления о
     * факте регистрации компетентным органом иностранного государства АГС в отношении гражданина РФ
     */
    var fio: String? = null
        @XmlElement(name = "ФИООтв") set

    /**
     * Дата принятия органом ЗАГС уведомления о факте регистрации компетентным органом иностранного государства АГС
     * в отношении гражданина РФ
     */
    var date: LocalDate? = null
        @XmlJavaTypeAdapter(LocalDateAdapter::class)
        @XmlAttribute(name = "ДатаПринят") set

    /**
     * Должность ответственного лица, которым принято уведомления о факте регистрации компетентным органом
     * иностранного государства АГС в отношении гражданина РФ
     */
    var position: String? = null
        @XmlAttribute(name = "ДолжнОтв") set
}
