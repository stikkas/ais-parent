package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Доля в праве общей долевой собственности пропорциональна размеру общей площади помещений, машино-мест,
 * не переданных участникам долевого строительства
 */
class BuilderShare {

    var info: String? = null
        @XmlElement(name = "builder_share_info") set

    override fun toString() =
        info?.let { "Доля в праве общей долевой собственности пропорциональна размеру общей площади помещений, машино-мест, не переданных участникам долевого строительства: $it" }
            ?: ""
}
