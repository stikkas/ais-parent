package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class LowerRequisites {
    var name: String? = null
        @XmlElement(name = "full_name_position") set

    var initials: String? = null
        @XmlElement(name = "initials_surname") set
}
