package ru.insoft.libs.smevs.death

import javax.xml.bind.annotation.*

@XmlType(namespace = "urn://x-artefacts-zags-fatalinf/types/4.0.1", propOrder = ["lastName", "firstName", "middleName"])
@XmlAccessorType(XmlAccessType.FIELD)
class Fio {
    @XmlElement(name = "Фамилия")
    var lastName: String? = null

    @XmlElement(name = "Имя")
    var firstName: String? = null

    @XmlElements(
        value = [XmlElement(name = "Отчество", type = String::class),
            XmlElement(name = "ПрОтчество", type = Int::class)]
    )
    var middleName: Any? = null
        get() = if (field?.toString()?.isNotBlank() == true) (field as String) else 1

    override fun toString() = "$lastName $firstName ${if (middleName?.toString() == "1") "" else (middleName ?: "")}"
}
