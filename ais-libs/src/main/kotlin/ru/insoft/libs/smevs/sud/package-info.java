@XmlSchema(
		elementFormDefault = XmlNsForm.QUALIFIED,
		namespace = "urn://ru/mvd/ibd-m/convictions/search/1.0.2",
		xmlns = {
				@XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
				@XmlNs(prefix = "xs", namespaceURI = "urn://ru/mvd/ibd-m/convictions/search/1.0.2"),
				@XmlNs(prefix = "ns", namespaceURI = "urn://ru/mvd/ibd-m/convictions/search/1.0.2")
		}
)
package ru.insoft.libs.smevs.sud;


import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
