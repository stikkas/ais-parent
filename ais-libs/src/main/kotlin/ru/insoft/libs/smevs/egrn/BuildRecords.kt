package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class BuildRecords {
    var records: List<BuildRecord>? = null
        @XmlElement(name = "build_record") set

    override fun toString() = records?.joinToString("\n") ?: ""
}
