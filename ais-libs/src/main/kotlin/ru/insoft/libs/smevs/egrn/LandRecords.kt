package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Земельные участки
 */
class LandRecords {
    var record: List<LandRecord>? = null
        @XmlElement(name = "land_record") set

    override fun toString() = record?.joinToString("\n") ?: ""
}
