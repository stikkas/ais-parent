package ru.insoft.libs.smevs.adapters

import java.time.LocalDateTime
import javax.xml.bind.annotation.adapters.XmlAdapter

class LocalDateTimeAdapter : XmlAdapter<String, LocalDateTime>() {
    override fun marshal(v: LocalDateTime?) = v?.let { it.toString() }

    override fun unmarshal(v: String?) = v?.let {
        LocalDateTime.parse(v)
    }

}
