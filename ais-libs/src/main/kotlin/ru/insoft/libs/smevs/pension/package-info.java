@XmlSchema(
		elementFormDefault = XmlNsForm.QUALIFIED, attributeFormDefault = XmlNsForm.UNQUALIFIED,
		namespace = "http://kvs.pfr.com/receipt-pension-fact/1.0.1",
		xmlns = {
				@XmlNs(prefix = "jxb", namespaceURI = "http://java.sun.com/xml/ns/jaxb"),
				@XmlNs(prefix = "smev", namespaceURI = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1"),
				@XmlNs(prefix = "tns", namespaceURI = "http://kvs.pfr.com/receipt-pension-fact/1.0.1"),
				@XmlNs(prefix = "xmlns", namespaceURI = "http://www.w3.org/2001/XMLSchema")
		}
)
package ru.insoft.libs.smevs.pension;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
