package ru.insoft.libs.smevs.egrn

import ru.insoft.libs.smevs.adapters.LocalDateAdapter
import java.time.LocalDate
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * Параметры временного интервала в запросе на предоставление сведений о правах отдельного лица
 */
@XmlAccessorType(XmlAccessType.FIELD)
class TPeriod {
    /**
     * До указанной даты
     */
    @XmlJavaTypeAdapter(LocalDateAdapter::class)
    var dateEnd: LocalDate? = null
}
