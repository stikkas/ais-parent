package ru.insoft.libs.smevs.snils

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

@XmlType(namespace = "http://common.kvs.pfr.com/1.0.0")
@XmlAccessorType(XmlAccessType.FIELD)
class BirthPlaceType {

    @XmlElement(name = "PlaceType")
    var placeType: String? = null

    @XmlElement(name = "Settlement")
    var settlement: String? = null

    @XmlElement(name = "District")
    var district: String? = null

    @XmlElement(name = "Region")
    var region: String? = null

    @XmlElement(name = "Country")
    var country: String? = null

    override fun toString() = listOfNotNull(country?.let { "Страна $it" },
        region?.let { "Субъект $it" },
        district?.let { "Район $it" },
        settlement?.let { "Населенный пункт $it" },
        placeType?.let { "Тип $it" }
    ).joinToString()
}
