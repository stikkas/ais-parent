package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

class RestrictData {
    /**
     * Номер регистрации ограничения права или обременения объекта недвижимости
     */
    var number: String? = null
        @XmlElement(name = "restriction_encumbrance_number") set

    /**
     * Порядковые номера частей
     */
    var partNumbers: PartNumbers? = null
        @XmlElement(name = "part_numbers") set

    /**
     * Вид зарегистрированного ограничения права или обременения объекта недвижимости
     */
    var type: CDDict? = null
        @XmlElement(name = "restriction_encumbrance_type") set

    /**
     * Ограничиваемые права
     */
    var rights: RestrictingRightsAllNumber? = null
        @XmlElement(name = "restricting_rights") set

    override fun toString() = listOfNotNull(
        number?.let { "Номер регистрации ограничения права или обременения объекта недвижимости: $it" },
        partNumbers?.let { "Порядковые номера частей: $it" },
        type?.value?.let { "Вид зарегистрированного ограничения права или обременения объекта недвижимости: $it" },
        rights
    ).joinToString("\n")
}
