@XmlSchema(
		elementFormDefault = XmlNsForm.QUALIFIED,
		namespace = "http://rosreestr.ru/services/v0.18/TStatementRequestEGRN",
		xmlns = {
				@XmlNs(prefix = "tns", namespaceURI = "http://rosreestr.ru/services/v0.18/TStatementRequestEGRN"),
				@XmlNs(prefix = "obj", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/TObject"),
				@XmlNs(prefix = "subj", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/Subjects"),
				@XmlNs(prefix = "doc", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/Documents"),
				@XmlNs(prefix = "com", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/Commons"),
				@XmlNs(prefix = "vc", namespaceURI = "http://www.w3.org/2007/XMLSchema-versioning"),
				@XmlNs(prefix = "address", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/Address"),
				@XmlNs(prefix = "dObT", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/objectType"),
				@XmlNs(prefix = "dReRF", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/regionrf"),
				@XmlNs(prefix = "stCom", namespaceURI = "http://rosreestr.ru/services/v0.1/TStatementCommons"),
				@XmlNs(prefix = "dObP", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/objectPurpose"),
				@XmlNs(prefix = "dUsT", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/usageType"),
				@XmlNs(prefix = "dResR", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/recieveResultType"),
				@XmlNs(prefix = "dReM", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/receivingMethod"),
				@XmlNs(prefix = "dLaC", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/LandCategory"),
				@XmlNs(prefix = "dTeZ", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/terzone"),
				@XmlNs(prefix = "dBoO", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/borderObjectType"),
				@XmlNs(prefix = "dHouse", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/house"),
				@XmlNs(prefix = "dUnitType", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/unitType"),
				@XmlNs(prefix = "dRecieveResultType", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/recieveResultType"),
				@XmlNs(prefix = "DObjectType", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/regionrf"),
				@XmlNs(prefix = "dKindInfo", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/kindInfo"),
				@XmlNs(prefix = "dLandCategory", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/LandCategory"),
				@XmlNs(prefix = "dDocument", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/document"),
				@XmlNs(prefix = "dAcC", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/actionCode"),
				@XmlNs(prefix = "dSt", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/statementType"),
				@XmlNs(prefix = "dReT", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/egrnrequesttype"),
				@XmlNs(prefix = "dAgr", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/agreements"),
				@XmlNs(prefix = "Sim1", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/Commons/simple-types"),
				@XmlNs(prefix = "dHoP", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/housingPurpose"),
				@XmlNs(prefix = "dRoP", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/roomPurpose"),
				@XmlNs(prefix = "dIObT", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/interdepobjecttype"),
				@XmlNs(prefix = "dCon", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/contractor"),
				@XmlNs(prefix = "dRequestDocument", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/requestDocument"),
				@XmlNs(prefix = "dDeclarantKind", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/declarantKind"),
				@XmlNs(prefix = "dCou", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/country"),
				@XmlNs(prefix = "bCat", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/benefitCategory"),
				@XmlNs(prefix = "dDeKR", namespaceURI = "http://rosreestr.ru/services/v0.1/commons/directories/declarantKindReg"),
				@XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance")
		}
)
package ru.insoft.libs.smevs.egrn;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

