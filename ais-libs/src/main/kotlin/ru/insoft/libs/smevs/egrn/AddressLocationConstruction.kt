package ru.insoft.libs.smevs.egrn

import javax.xml.bind.annotation.XmlElement

/**
 * Адрес (местоположение) объекта недвижимости - сооружения, объекта незавершенного строительства,
 * единого недвижимого комплекса
 */
class AddressLocationConstruction {
    /**
     * Тип адреса
     */
    var type: CDDict? = null
        @XmlElement(name = "address_type") set

    /**
     * Адрес
     */
    var address: AddressMain? = null
        @XmlElement set

    /**
     * Наименования субъектов Российской Федерации, муниципальных образований, населенных пунктов (при наличии)
     */
    var locations: LocationsCity? = null
        @XmlElement set

    override fun toString() = listOfNotNull(
        type?.value?.let { "Тип адреса: $it" },
        address, locations
    ).joinToString("\n")
}
