package ru.insoft.libs

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.multipart.MultipartFile
import ru.insoft.libs.models.Address
import ru.insoft.libs.models.AddressDict
import ru.insoft.libs.models.User
import java.nio.file.Paths
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.regex.Pattern
import kotlin.math.max

fun getAddressIds(data: Map<String?, Any>): List<String>? {
    val id = data.keys.firstOrNull() ?: return null
    if (id.isBlank()) {
        return null
    }

    val res = mutableListOf(id)
    var i = 5
    var rem = id.substring(i)
    var reg = Regex("^0+$")
    val max = id.length
    while (i < max && !reg.matches(rem)) {
        res.add(id.substring(0, i) + "0".repeat(max - i))
        i += 5
        rem = id.substring(i)
    }
    return res.sorted()
}

fun getAddressId(levels: Map<ArrayList<AddressDict>?, Any>): String? = getMainAddressId(levels.keys.firstOrNull())

fun getMainAddressId(levels: List<AddressDict>?): String? = if (levels.isNullOrEmpty()) {
    null
} else {
    levels.map { it.value }.filterNot { it.isNullOrBlank() }.reduce { acc, n ->
        var sb = StringBuilder()
        for (i in acc!!.indices) {
            sb.append(if (acc[i] == '0') n!![i] else acc[i])
        }
        sb.toString()
    }
}

fun formatAddressToString(place: Address?) = place?.let { d ->
    setChilds(d.levels)
    (listOf(if (d.country.isNullOrEmpty()) d.countryText else d.country) + flatLevels(d.levels)
        .plus<String?>(d.addressText)).filterNot { it.isNullOrBlank() }.joinToString()
}

/**
 * Проставляет корпус, строение и т.д в объект child
 */
fun setChilds(levels: List<AddressDict>?) = levels?.forEach {
    if (!it.korpLabel.isNullOrBlank()) {
        it.child = AddressDict().apply {
            label = it.korpLabel
            short = it.korpShort
            type = it.korpType
            levelId = it.korpLevelId
            level = it.korpLevel
            order = it.korpOrder
        }
        it.korpLabel = null
    }
    if (!it.strLabel.isNullOrBlank()) {
        val address = AddressDict().apply {
            label = it.strLabel
            short = it.strShort
            type = it.strType
            levelId = it.strLevelId
            level = it.strLevel
            order = it.strOrder
        }
        it.child = it.child?.let { c ->
            c.child = address
            c
        } ?: address
        it.strLabel = null
    }
}

inline fun Long.offset(size: Int) = (max(this, 1) - 1) * size

/**
 * Приводит дату с начальными нулями (для дня и месяца) к виду, когда эти нули обрезаются
 */
fun normalizeDateStr(date: String?) = date?.let {
    it.replace(Regex("^(00\\.){0,2}"), "")
}

/**
 * Верно для клиентских приложений (не для админки)
 */
fun currentUser() = SecurityContextHolder.getContext().authentication?.details as User?
fun hasRole(role: String) = currentUser()?.roleId == role
fun currentOrgId() = currentUser()?.orgId ?: ""
fun currentAisId() = currentUser()?.aisId ?: 0

/**
 * Конвертирует список уровней адреса в список строк
 */
fun flatLevels(levels: List<AddressDict>) = levels.map { addressToString(it) }

fun addressToString(address: AddressDict): String = (if (address.order === 0)
    "${address.short} ${address.label}"
else
    "${address.label} ${address.short}") + (address.child?.let { " " + addressToString(it) } ?: "")

/**
 * Возвращает правильное имя файла для записи в базу
 */
fun fileNameToSave(file: MultipartFile?, fileName: String?, convert: Boolean = false) = if (file == null)
    fileName?.let { Paths.get(it).fileName.toString() }
else
    (if (convert) {
        // С клиента могут прилетать названия в формате 'id_entity:filename.ext'
        file.originalFilename?.let { it.substring(it.indexOf(':') + 1) }
    } else {
        file.originalFilename
    }) ?: "scan"

private fun String?.toIntOrMin(min: Int): Int {
    if (this == null) {
        return min
    }
    return toIntOrNull() ?: min
}

fun String?.toDay(): Int {
    return kotlin.math.min(max(toIntOrMin(1), 1), 31)
}

fun String?.toMonth(): Int {
    return kotlin.math.min(max(toIntOrMin(1), 1), 12)
}

fun String?.toYear(): Int {
    return max(toIntOrMin(1900), 1900)
}

val textDatePattern = Regex("^((?<day>\\d{1,2})\\.)?((?<month>\\d{1,2})?\\.)?(?<year>\\d{1,4})$")
val minDate = LocalDate.of(1900, 1, 1)

const val max50 = 50;
private val dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy")
private val dttf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
fun LocalDate.format(): String = dtf.format(this)
fun LocalDateTime.format(): String = dttf.format(this)
fun String.toDate(): LocalDate = LocalDate.parse(this, dtf)

/**
 * Возвращает список соответствия объектов их прикрепляемым файлам
 */
fun arrangeFiles(objs: List<Any?>?, files: List<MultipartFile>?): Map<Any, MultipartFile>? =
    if (!files.isNullOrEmpty()) {
        objs?.mapIndexedNotNull { i, d ->
            d?.let { obj ->
                files.find { it.originalFilename?.startsWith("$i:") ?: false }?.let {
                    obj to it
                }
            }
        }?.toMap()
    } else null

/**
 * Возвращает список номеров квартир
 * Исходная строка может быть одним из видов:
 * 15
 * 15a
 * 15,18, 32
 * 15-18, 32-33,19
 * 15, 14, 20-22,
 */
fun flatToFlats(flat: String?) = flat?.let { f ->
    f.split(",").flatMap {
        if (it.contains("-")) {
            val (min, max) = it.split("-").map { it.trim().toInt() }
            (min..max).map { it.toString() }
        } else {
            listOf(it.trim())
        }
    }.filterNot { it.isNullOrBlank() }.toSet().sorted()
}

val zeroEx = Pattern.compile("[^0]")
const val fieldWidth = 5
const val addressIdLength = 50

/**
 * Возвращает идентификатор адреса, годного для поиска подардесов для заданого идентификатора
 * @param paid - идентификатор адреса
 * @return строку для использования в SQL запросе like или similar to
 */
fun padAddressId(paid: String?) = paid?.let {
    if (it.isEmpty())
        null
    else {
        val m = zeroEx.matcher(paid)
        var lastNumber = 0
        while (m.find()) {
            lastNumber = m.start()
        }
        val indexStart = (lastNumber / fieldWidth + 1) * fieldWidth
        it.substring(0, indexStart) + "_".repeat(addressIdLength - indexStart)
    }
}
