### Общие куски кода для проектов "АИС Опека" и "АИС Админ"

в проекте, который использует эту библиотеку нужно дабавить в главный файл такие аннотации (через mybatis-config.xml
почему-то не работает):

```code
@MapperScan(basePackageClasses = [SearchMapper::class, SharedDictMapper::class])
@SpringBootApplication(scanBasePackages = ["ru.insoft"])
```
