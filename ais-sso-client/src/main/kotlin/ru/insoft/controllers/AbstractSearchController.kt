package ru.insoft.controllers

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import ru.insoft.filters.LdFilter
import ru.insoft.libs.controllers.AbstractController
import ru.insoft.libs.models.Response
import ru.insoft.pf.PrivateFilesService
import ru.insoft.services.SearchService

abstract class AbstractSearchController(protected open val searchService: SearchService) : AbstractController() {
    @Autowired
    private lateinit var pfs: PrivateFilesService

    @CrossOrigin(origins = ["*"])
    @GetMapping(SearchUrls.ld)
    fun searchPopulation(
        filter: LdFilter, @RequestParam(defaultValue = "20") pageSize: Int,
        @RequestParam(defaultValue = "1") page: Long
    ) = handle {
        val res = searchService.findAllLd(filter, page, pageSize)
        if (filter.snils.isNullOrEmpty()) {
            res
        } else {
            res.apply { runBlocking { data.forEach { launch { pfs.createPf(it.id!!) } } } }
        }
    }

    @GetMapping("${SearchUrls.ld}/{id}")
    fun searchFio(@PathVariable id: String) = handle {
        searchService.findFio(id)
    }

    @GetMapping(SearchUrls.isMyAddress)
    fun isMyAddress(@RequestParam id: String) = handle {
        val res = searchService.isMyAddress(id)
        if (res) {
            res
        } else {
            Response.fail("Выбранный адрес регистрации не относится к данной организации")
        }
    }

}


