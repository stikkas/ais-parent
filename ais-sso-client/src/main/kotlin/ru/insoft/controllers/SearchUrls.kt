package ru.insoft.controllers

import com.fasterxml.jackson.annotation.JsonProperty
import ru.insoft.libs.controllers.ApiUrls

open class SearchUrls {
    @JsonProperty("base")
    fun getBase() = base

    @JsonProperty("ld")
    fun getLd() = ld

    @JsonProperty("regd")
    fun getRegistered() = registered

    companion object {
        const val base = "${ApiUrls.root}/search"
        const val ld = "/ld"
        const val isMyAddress = "/is-my-address"
        const val registered = "/registered"
    }
}
