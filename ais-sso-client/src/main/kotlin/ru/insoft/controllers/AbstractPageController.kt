package ru.insoft.controllers

import org.springframework.http.MediaType
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

/**
 * Конроллер для отдачи html страниц
 */
abstract class AbstractPageController {
    abstract var context: String

    open fun pages(resp: HttpServletResponse) {
        resp.addCookie(Cookie("ctx", context))
        resp.status = HttpServletResponse.SC_OK
        resp.contentType = MediaType.TEXT_HTML_VALUE
        resp.writer.print(indexText)
    }

    // Чтобы не пересобирать angular приложение для разных контекстов
    // При старте приложения немного меняем содержимое index.html
    // Разные контексты нужны для запуска разных приложений (/admin-bddsop + /bddsop) на
    // Одном порту
    private val indexText by lazy {
        var res = ""
        val baseRegex = Regex("""(href|src)="/?(.*?)"""")
        javaClass.classLoader.getResourceAsStream("static/index.html").use {
            it.bufferedReader()
                .forEachLine {
                    res += it.replace(baseRegex, """$1="$context/$2"""")
                }
        }
        res
    }
}

