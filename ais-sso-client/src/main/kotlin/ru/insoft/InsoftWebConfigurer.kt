package ru.insoft

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.format.FormatterRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import ru.insoft.pf.ManInfoFormatter

abstract class InsoftWebConfigurer : WebMvcConfigurer {

    abstract var om: ObjectMapper

    override fun addFormatters(registry: FormatterRegistry) {
        registry.addFormatter(ManInfoFormatter(om))
    }
}
