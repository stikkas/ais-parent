package ru.insoft.json

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import ru.insoft.libs.models.DictValue

class DictValueDeserializer : JsonDeserializer<DictValue>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?) =
        p?.valueAsString?.let {
            DictValue.toDictValue(it)
        }
}
