package ru.insoft.models.reqs

import ru.insoft.libs.flatLevels
import ru.insoft.libs.models.AddressDict
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.smevs.sud.IncompleteDate
import ru.insoft.libs.smevs.sud.RegistrationInfo
import ru.insoft.libs.smevs.sud.SudimostRequest
import ru.insoft.pf.models.ManInfo

class SudimostCreatorRequest(val info: ManInfo?) : RequestCreator {
    override operator fun component1(): Any = info?.let {
        SudimostRequest().apply {
            SNILS = it.snils
            surname = it.lastName
            name = it.firstName
            patronymicName = it.middleName
            it.birthDateText?.split(".")?.let { (d, m, y) ->
                birthDate = IncompleteDate().apply {
                    day = d.toInt()
                    month = m.toInt()
                    year = y.toInt()
                }
            } ?: throw Exception("Отсутствует дата рождения")
            val places = mutableListOf<RegistrationInfo>()
            it.birthAddress?.let { a ->
                if (a.levels.isNotEmpty() || a.addressText != null) {
                    places.add(
                        RegistrationInfo().apply {
                            type = "000"
                            regionCode = regionCode(a.levels)
                            place = flatLevels(a.levels).plus<String?>(a.addressText)
                                .filterNot { it.isNullOrBlank() }.joinToString()
                        })
                }
            }
            it.regs?.filterNotNull()?.forEach { r ->
                if (r.addressText != null || r.levels.isNotEmpty()) {
                    places.add(
                        RegistrationInfo().apply {
                            type = if (r.addressType == DictValue.FACT_ADDR) "201" else "200"
                            regionCode = regionCode(r.levels)
                            place = flatLevels(r.levels).plus<String?>(r.addressText)
                                .plus<String?>(r.flat?.let { "кв. $it" })
                                .plus<String?>(r.room?.let { "к. $it" })
                                .filterNot { it.isNullOrBlank() }.joinToString()
                        })
                }
            }
            registrationPlace = if (places.isEmpty()) null else places
        }
    } ?: throw Exception("Пользователь не найден")

    override operator fun component2(): String = "MVDCONVICT"

    private fun regionCode(levels: List<AddressDict>) = levels.first()?.let { it.value!!.substring(2, 5) }
}
