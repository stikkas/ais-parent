package ru.insoft.models.reqs

import ru.insoft.libs.smevs.snils.BirthPlaceType
import ru.insoft.libs.smevs.snils.DocType
import ru.insoft.libs.smevs.snils.PassportRF
import ru.insoft.libs.smevs.snils.SnilsRequest
import ru.insoft.pf.models.ManInfo

class SnilsCreatorRequest(val info: ManInfo?) : RequestCreator {
    override operator fun component1(): Any = info?.let {
        SnilsRequest().apply {
            familyName = it.lastName
            firstName = it.firstName
            patronymic = it.middleName
            birthDate = it.birthDate ?: throw Exception("Отсутствует дата рождения")
            sex = it.sex ?: throw Exception("Отсутствует пол")
            birthPlace = it.birthAddress?.let {
                BirthPlaceType().apply {
                    country = it.country ?: it.countryText
                    region = it.levels.find { l -> l.levelId == "002" }?.label
                    district = it.levels.find { l -> l.levelId == "003" }?.label
                    settlement = it.levels.find { l -> l.levelId == "004" }?.label
//                    placeType = ""
                }
            }
            identity = DocType.build(it.docs?.firstOrNull())?.apply {
                (this as? PassportRF)?.let {
                    when {
                        series.isNullOrBlank() -> throw Exception("Отсутствует серия паспорта")
                        number.isNullOrBlank() -> throw Exception("Отсутствует номер паспорта")
                        issueDate == null -> throw Exception("Отсутствует дата выдачи паспорта")
                        issuer.isNullOrBlank() -> throw Exception("Отсутствует данные кем выдан паспорт")
                    }
                }
            }
        }
    } ?: throw Exception("Пользователь не найден")

    override operator fun component2(): String = "PFRSNILS"
}
