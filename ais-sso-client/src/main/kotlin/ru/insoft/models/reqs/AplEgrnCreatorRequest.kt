package ru.insoft.models.reqs

import ru.insoft.libs.smevs.egrn.AplEgrnRequest

class AplEgrnCreatorRequest(val details: AplEgrnRequest?) : RequestCreator {
    override operator fun component1(): Any = details?.also {
        it.header?.appliedDocument ?: throw Exception("Нет документа, удостоверяющего личность")
    } ?: throw Exception("Пользователь не найден")

    override operator fun component2(): String = "ROSREESTR"
}
