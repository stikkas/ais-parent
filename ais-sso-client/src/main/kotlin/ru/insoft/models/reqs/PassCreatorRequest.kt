package ru.insoft.models.reqs

import ru.insoft.libs.models.Document
import ru.insoft.libs.smevs.pass.PassRequest

class PassCreatorRequest(val doc: Document?) : RequestCreator {
    override operator fun component1(): Any = doc?.let {
        PassRequest().apply {
            series = it.series
            number = it.number
            issueDate = it.issueDate
        }
    } ?: throw Exception("Паспорт не найден")

    override operator fun component2(): String = "MVDPAS"
}
