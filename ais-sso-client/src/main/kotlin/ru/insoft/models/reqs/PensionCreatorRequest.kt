package ru.insoft.models.reqs

import ru.insoft.libs.smevs.pension.PensRequest

class PensionCreatorRequest(val details: PensRequest?) : RequestCreator {
    override operator fun component1(): Any = details?.also {
        it.snils ?: throw Exception("СНИЛС отсутствует")
    } ?: throw Exception("Пользователь не найден")

    override operator fun component2(): String = "PFRPENS"
}
