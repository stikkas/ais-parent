package ru.insoft.models.reqs

import ru.insoft.libs.smevs.birth.BirthRequest
import ru.insoft.libs.smevs.birth.BirthRequestDetails

class BirthCreatorRequest(val details: BirthRequestDetails?) : RequestCreator {
    override operator fun component1(): Any = details?.let {
        BirthRequest().apply { details = it }
    } ?: throw Exception("Пользователь не найден")

    override operator fun component2(): String = "ZAGSROGD"
}
