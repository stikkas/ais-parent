package ru.insoft.models.reqs

import ru.insoft.libs.smevs.divorce.DivorceRequest
import ru.insoft.libs.smevs.divorce.RequestDetails

class DivorceCreatorRequest(val info: RequestDetails?) : RequestCreator {
    override operator fun component1(): Any = info?.let {
        DivorceRequest().apply {
            details = it
//            it.agsInfo?.let {
//                if (it.nomer.isNullOrBlank()) {
//                    throw Exception("Отсутствует номер записи акта гражданского состояния")
//                }
//            }
        }
    } ?: throw Exception("Пользователь не найден")

    override operator fun component2(): String = "ZAGSRAST"
}
