package ru.insoft.models.reqs

import ru.insoft.libs.smevs.death.DeathRequest
import ru.insoft.libs.smevs.death.DeathRequestDetails

class DeathCreatorRequest(val details: DeathRequestDetails?) : RequestCreator {
    override operator fun component1(): Any = details?.let {
        DeathRequest().apply {
            details = it
//            it.ags?.let {
//                if (it.nomer.isNullOrBlank()) {
//                    throw Exception("Отсутствует номер записи акта гражданского состояния")
//                }
//            }
        }
    } ?: throw Exception("Пользователь не найден")

    override operator fun component2(): String = "ZAGSSMERT"
}
