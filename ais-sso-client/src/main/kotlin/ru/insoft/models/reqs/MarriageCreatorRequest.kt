package ru.insoft.models.reqs

import ru.insoft.libs.smevs.marry.BrakRequest
import ru.insoft.libs.smevs.marry.ReqDetails

class MarriageCreatorRequest(val info: ReqDetails?) : RequestCreator {
    override operator fun component1(): Any = info?.let {
        BrakRequest().apply {
            details = it
//            it.agsInfo?.let {
//                if (it.nomer.isNullOrBlank()) {
//                    throw Exception("Отсутствует номер записи акта гражданского состояния")
//                }
//            }
        }
    } ?: throw Exception("Пользователь не найден")

    override operator fun component2(): String = "ZAGSBRAK"
}
