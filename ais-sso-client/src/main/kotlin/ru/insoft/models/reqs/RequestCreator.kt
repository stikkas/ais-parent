package ru.insoft.models.reqs

interface RequestCreator {
    operator fun component1(): Any
    operator fun component2(): String
}
