package ru.insoft.models

import java.time.LocalDate

/**
 * Запись в Реестрах ЕГИССО
 */
class EgissoRegistryRec(val id: String, val sentDate: LocalDate?) {
    /**
     * Идентификатор законного представителя
     */
    var reprId: String? = null
}

