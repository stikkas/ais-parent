package ru.insoft.models

class Official {
    /**
     * Идентификатор должности из таблицы userlinks (userlinkid)
     */
    var id: String? = null
    var fio: String? = null
    var position: String? = null
    var organization: String? = null
}
