package ru.insoft.models

open class LdTableRow {
    var id: String? = null
    var fio: String? = null
    var sex: String? = null
    var birthDate: String? = null
    var deathDate: String? = null
    var documentType: String? = null
    var seria: String? = null
    var number: String? = null
    var actual: Int? = null

    var snils: String? = null
    var factAdr: String? = null
    var regAdr: String? = null

    /**
     * Ребенок или нет
     */
    var kind: Boolean? = null
}
