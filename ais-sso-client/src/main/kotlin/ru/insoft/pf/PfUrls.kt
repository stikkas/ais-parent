package ru.insoft.pf

import com.fasterxml.jackson.annotation.JsonProperty
import ru.insoft.libs.controllers.ApiUrls

object PfUrls {
    const val base = "${ApiUrls.root}/private-files"

    @JsonProperty("base")
    fun getBase() = base

    const val mainInfo = "/main-info"

    @JsonProperty("mainInfo")
    fun getMainInfo() = mainInfo

    const val person = "/person"

    @JsonProperty("person")
    fun getPerson() = person

    const val lastEventNotAdopt = "/last-event-not-adopt"

    @JsonProperty("lena")
    fun getLastEventNotAdopt() = lastEventNotAdopt

    const val canPrintHistory = "/can-print-history"

    @JsonProperty("cph")
    fun getCanPrintHistory() = canPrintHistory

    const val document = "/document"

    @JsonProperty("doc")
    fun getDocument() = document

    const val zagsInfo = "/zags-info"

    @JsonProperty("zags")
    fun getZagsInfo() = zagsInfo

    const val regsInfo = "/regs-info"

    @JsonProperty("regs")
    fun getRegsInfo() = regsInfo

    const val otherInfo = "/other-info"

    @JsonProperty("other")
    fun getOtherInfo() = otherInfo

    const val addedInfo = "/added-info"

    @JsonProperty("added")
    fun getAddedInfo() = addedInfo

    const val smevs = "/smevs"

    @JsonProperty("smevs")
    fun getSmevs() = smevs

    const val changesInfo = "/changes-info"

    @JsonProperty("chng")
    fun getChangesInfo() = changesInfo

    const val docs = "/docs"

    @JsonProperty("docs")
    fun getDocs() = docs

    const val rootAddress = "/root-address"

    @JsonProperty("root")
    fun getRootAddress() = rootAddress

    const val registered = "/is-registered"

    @JsonProperty("isreg")
    fun getRegistered() = registered

    const val saveParams = "/save-params"

    @JsonProperty("saveParams")
    fun getSaveParams() = saveParams
}

