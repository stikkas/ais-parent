package ru.insoft.pf

import org.springframework.web.multipart.MultipartFile
import ru.insoft.libs.models.Address
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.smevs.birth.RegInfo
import ru.insoft.models.Official
import ru.insoft.pf.models.*
import java.time.LocalDate

interface PrivateFilesService {
    fun getManInfo(id: String): ManInfo?
    fun getZagsInfo(id: String): ZagsInfo?
    fun getRegsInfo(id: String): RegisteredInfo?
    fun getOtherInfo(id: String): InvalidIncapable?
    fun getAddedInfo(id: String): AddedRequisites?

    /**
     * Возвращает список ЛД с заданным СНИЛС, у которых systemmanid отличается от заданного
     */
    fun findExistSnils(manId: String?, snils: String): List<SnilsCopy>?
    fun findExistDoc(manId: String?, typeId: String, series: String?, nomer: String?): List<SnilsCopy>?
    fun findTwins(
        manId: String?, lastName: String?, firstName: String?, birthDate: LocalDate?,
        levelId: String?, birthAddress: Address?
    ): List<SnilsCopy>?

    fun findExistPasport(manId: String?, series: String, nomer: String, typeId: String): Official?
    fun getChangesInfo(id: String): ChangeInfo?
    fun getSmevInfo(id: String): SmevsInfo?
    fun isRegistered(systemmanId: String, type: DictValue): Boolean
    fun lastEventNotAdopt(id: String): Boolean
    fun createPf(manId: String)
    fun saveManInfo(info: ManInfo, files: Map<Any, MultipartFile>?): ManInfo
    fun savePersonalData(info: ManInfo, files: Map<Any, MultipartFile>?): ManInfo
    fun saveNewDoc(info: ManInfo, files: Map<Any, MultipartFile>?): ManInfo
    fun saveZagsItem(item: ZagsItem, id: String): ZagsItem
    fun saveRegInfo(regs: List<ActualRegistration>): Boolean
    fun saveAddedReq(info: AddedRequisites): AddedRequisites
    fun saveOther(item: InvIncItem, id: String): InvIncItem
    fun saveSmevs(info: SmevsInfo): SmevsInfo
    fun remove(id: String): String?
    fun removeZags(id: String): Boolean
    fun rootAddress(orgId: String): DwellingPlace?

    /**
     * Удаляет инвалидность или недееспособность
     * @param id идентификатор инвалидности или недееспособности
     * @param dis если true - работаем с недееспособностью иначе - инвалидностью
     */
    fun removeOther(id: String, dis: Boolean?): Boolean
    fun sendRequest(req: SmevRequest, manId: String): SmevRequest?
    fun hasSameRegAddress(systemmanId: String, addressId: String, startDate: LocalDate, endDate: LocalDate?): Boolean
    fun hasActualRegWithAddress(
        manId: String, flats: List<String>?, addressId: String, flat: String?,
        addressText: String?
    ): Boolean

    fun findSameRegistration(manId: String, regType: DictValue?, startDate: LocalDate?): DwellingPlace?
    fun canPrintHistory(id: String): Boolean
}
