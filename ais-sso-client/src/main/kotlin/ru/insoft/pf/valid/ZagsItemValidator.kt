package ru.insoft.pf.valid

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.libs.models.DictValue
import ru.insoft.pf.models.ZagsItem
import ru.insoft.pf.repo.PrivateFilesMapper

@Component
class ZagsItemValidator(private val pfm: PrivateFilesMapper) : Validator {

    override fun supports(clazz: Class<*>) = ZagsItem::class.java == clazz

    override fun validate(target: Any, errors: Errors): Unit = with(target as ZagsItem) {
        typeId ?: errors.rejectValue("typeId", "Наименование документа")
        issueDate ?: errors.rejectValue("issueDate", "Дата выдачи")
        if (typeId == DictValue.DEATH_CERT.id) {
            date ?: errors.rejectValue("date", "Дата")
            pfm.birthDate(manId)?.let { bd ->
                date?.let { dd ->
                    if (dd.isBefore(bd)) {
                        errors.rejectValue("date", "Дата смерти меньше даты рождения")
                    }
                }
            }
        }
    }
}
