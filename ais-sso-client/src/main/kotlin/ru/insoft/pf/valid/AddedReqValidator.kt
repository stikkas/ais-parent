package ru.insoft.pf.valid

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.pf.models.AddedRequisites

@Component
class AddedReqValidator : Validator {

    override fun supports(clazz: Class<*>) = AddedRequisites::class.java == clazz

    override fun validate(target: Any, errors: Errors): Unit = with(target as AddedRequisites) {
        states?.forEachIndexed { i, it ->
            try {
                errors.pushNestedPath("states[$i]")
                it.statusId ?: errors.rejectValue("statusId", "Социальный статус")
                it.startDate ?: errors.rejectValue("startDate", "Дата установки")
            } finally {
                errors.popNestedPath()
            }
        }
        edus?.forEachIndexed { i, it ->
            try {
                errors.pushNestedPath("edus[$i]")
                it.typeId ?: errors.rejectValue("typeId", "Образование")
                it.infoDate ?: errors.rejectValue("infoDate", "Дата сведений")
            } finally {
                errors.popNestedPath()
            }
        }
        trains?.forEachIndexed { i, it ->
            try {
                errors.pushNestedPath("trains[$i]")
                it.placeId ?: errors.rejectValue("placeId", "Место учебы")
                it.startDate ?: errors.rejectValue("startDate", "Дата начала")
            } finally {
                errors.popNestedPath()
            }
        }
        jobs?.forEachIndexed { i, it ->
            try {
                errors.pushNestedPath("jobs[$i]")
                it.place ?: errors.rejectValue("place", "Место работы")
                it.startDate ?: errors.rejectValue("startDate", "Дата приема")
            } finally {
                errors.popNestedPath()
            }
        }
    }
}
