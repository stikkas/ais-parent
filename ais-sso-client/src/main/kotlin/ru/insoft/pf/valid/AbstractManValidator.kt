package ru.insoft.pf.valid

import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.libs.max50
import ru.insoft.pf.models.ManInfo
import java.time.LocalDate

abstract class AbstractManValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as ManInfo) {
        checkFio(errors, lastName, "lastName", "Фамилия")
        checkFio(errors, firstName, "firstName", "Имя")
        if (birthDate == null) {
            errors.rejectValue("birthDate", "Дата рождения")
        } else if (birthDate?.isAfter(LocalDate.now()) == true) {
            errors.rejectValue("birthDate", "Дата рождения больше текущей даты")
        }

//        sex ?: errors.rejectValue("sex", "Пол")
    }

    override fun supports(clazz: Class<*>) = ManInfo::class.java == clazz

    private fun checkFio(errors: Errors, value: String?, field: String, code: String) {
        if (value.isNullOrEmpty() || value.length > max50) {
            errors.rejectValue(field, code)
        }
    }
}

