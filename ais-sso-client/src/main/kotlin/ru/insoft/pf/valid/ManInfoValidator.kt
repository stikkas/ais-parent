package ru.insoft.pf.valid

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import ru.insoft.libs.AisId
import ru.insoft.libs.flatToFlats
import ru.insoft.libs.formatAddressToString
import ru.insoft.libs.getMainAddressId
import ru.insoft.pf.PrivateFilesService
import ru.insoft.pf.models.ManInfo
import java.time.LocalDate
import java.time.Period

@Component
class ManInfoValidator(
    private val docValidator: DocumentValidator,
    private val regValidator: DwellingPlaceValidator,
    private val ps: PrivateFilesService
) : AbstractManValidator() {
    @Value("\${app.ais-id}")
    var aisId: Int? = null

    override fun validate(target: Any, errors: Errors): Unit = with(target as ManInfo) {
        super.validate(target, errors)
        val date = docs?.minByOrNull { it.issueDate ?: LocalDate.MAX }?.issueDate
        if (date != null && birthDate?.isAfter(date) == true) {
            errors.rejectValue("birthDate", "Дата рождения больше даты выдачи документа")
        }
        snils?.let {
            if (it.isNotEmpty()) {
                val mr = snilsRegex.find(it)
                mr?.groups?.let { groups ->
                    val n1 = groups["p1"]?.value!!
                    val n2 = groups["p2"]?.value!!
                    val n3 = groups["p3"]?.value!!
                    val number1 = n1.toInt()
                    val number2 = n2.toInt()
                    if (number1 > 1 || (
                                number1 == 1 && (
                                        number2 > 1 ||
                                                (number2 == 1 && n3.toInt() > 998)
                                        )
                                )
                    ) {
                        if ((n1 + n2 + n3).mapIndexed { index, c ->
                                (9 - index) * c.toString().toInt()
                            }.sum() % 101 % 100 == groups["control"]?.value!!.toInt())
                            true
                        else
                            null
                    } else
                        true
                } ?: errors.rejectValue("snils", "Введите корректный СНИЛС")
            }
        }
        inn?.let {
            if (it.isNotEmpty()) {
                if (it.length != 12) {
                    errors.rejectValue("inn", "ИНН")
                }
            }
        }

        regs?.forEachIndexed { i, it ->
            try {
                errors.pushNestedPath("regs[$i]")
                it?.let { p -> ValidationUtils.invokeValidator(regValidator, p, errors) }
            } finally {
                errors.popNestedPath()
            }
            it?.startDate?.let { s ->
                birthDate?.let { b ->
                    if (b.isAfter(s)) {
                        errors.rejectValue("birthDate", "Дата рождения больше даты регистрации")
                    }
                }
            }
        }

        docs.forEachIndexed { i, d ->
            d.issueDate?.let { s ->
                birthDate?.let { b ->
                    if (b.isAfter(s)) {
                        errors.rejectValue("birthDate", "Дата рождения больше даты выдачи")
                    }
                }
            }
            try {
                errors.pushNestedPath("docs[$i]")
                docValidator.validate(d, errors, id)
            } finally {
                errors.popNestedPath()
            }
        }
        // Отдельные проверки для "Учет и регистрация населения"
        if (aisId == AisId.RUG.id) {
            if (regs.isNullOrEmpty()) {
                errors.rejectValue("regs", "Должен быть выбран адрес регистрации человека")
            }
            birthDate?.let { bd ->
                if (Period.between(bd, LocalDate.now()).years < 14) {
                    if (relatives.isNullOrEmpty()) {
                        errors.rejectValue("relatives", "Необходимо выбрать родителей/законных представителей ребенка")
                    } else if (regs?.all { reg ->
                            reg?.let { p ->
                                relatives?.any { rel ->
                                    p.startDate?.let {
                                        ps.hasSameRegAddress(rel.id!!, getMainAddressId(p.levels)!!, it, p.endDate)
                                    } == true
                                } == true
                            } == true
                        } != true) {
                        errors.rejectValue(
                            "relatives",
                            "Ребенок может быть зарегистрирован только к одному из законных представителей"
                        )
                    }
                }
            }
            id?.let { manId ->
                if (regs?.any {
                        it?.let { dp ->
                            if (dp.id == null) { // для новых регистраций проверяем что уже нет похожей
                                val addressText = dp.addressText?.trim()
                                val flats = flatToFlats(dp.flat)
                                ps.hasActualRegWithAddress(
                                    manId, flats, getMainAddressId(dp.levels)!!,
                                    flats?.firstOrNull(), addressText
                                )
                            } else {
                                false
                            }
                        } == true
                    } == true) {
                    val fio = listOf(lastName, firstName, middleName)
                        .filterNot { it.isNullOrEmpty() }
                        .joinToString(" ")
                    errors.rejectValue(
                        "id",
                        "Человек $fio не может прибыть на территорию дважды без предварительного выбытия"
                    )
                }
                var address: String? = ""
                if (regs?.any {
                        it?.let { dp ->
                            if (dp.id == null) {
                                val existsReg = ps.findSameRegistration(manId, dp.regType, dp.startDate)?.also {
                                    address = formatAddressToString(it)
                                }
                                existsReg != null
                            } else {
                                false
                            }
                        } == true
                    } == true) {
                    errors.rejectValue(
                        "id",
                        "Дата убытия с адреса $address не может быть больше даты прибытия по новому адресу"
                    )
                }
            }
        }
    }

    companion object {
        val snilsRegex = Regex("^(?<p1>\\d{3})-(?<p2>\\d{3})-(?<p3>\\d{3})-(?<control>\\d{2})$")
    }
}

