package ru.insoft.pf.valid

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import ru.insoft.pf.models.ManInfo
import ru.insoft.pf.repo.PrivateFilesMapper

@Component
class PersonalDataValidator(private val docValidator: NewDocValidator, private val pfm: PrivateFilesMapper) :
    AbstractManValidator() {

    override fun validate(target: Any, errors: Errors): Unit = with(target as ManInfo) {
        super.validate(target, errors)
        var date = pfm.minAcutalDocDate(id!!)
        if (docs.isNotEmpty()) {
            with(docs[0]) {
                issueDate?.let { ld ->
                    if (date == null || (date != null && ld.isBefore(date))) {
                        date = ld
                    }
                }
            }
        }
        if (date != null && birthDate?.isAfter(date) == true) {
            errors.rejectValue("birthDate", "Дата рождения больше даты выдачи документа")
        }
        ValidationUtils.invokeValidator(docValidator, target, errors)
    }
}

