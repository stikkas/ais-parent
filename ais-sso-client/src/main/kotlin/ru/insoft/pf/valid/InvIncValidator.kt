package ru.insoft.pf.valid

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.insoft.pf.models.InvIncItem

@Component
class InvIncValidator : Validator {

    override fun supports(clazz: Class<*>) = InvIncItem::class.java == clazz

    override fun validate(target: Any, errors: Errors): Unit = with(target as InvIncItem) {
        invalid?.let {
            errors.pushNestedPath("invalid")
            ValidationUtils.rejectIfEmpty(errors, "invalidGroupId", "Группа инвалидности")
            it.invDate ?: errors.rejectValue("invDate", "Дата установки инвалидности")
            if (it.invDoc?.isWrong() == true) {
                errors.rejectValue("invDoc", "Не выбран документ, устанавливающий инвалидность")
            }
            errors.popNestedPath()
        }
        incapable?.let {
            errors.pushNestedPath("incapable")
            it.incDate ?: errors.rejectValue("incDate", "Дата установки недееспособности")
            if (it.incDoc?.isWrong() != false) {
                errors.rejectValue("incDoc", "Не выбран документ, устанавливающий недееспособность")
            }
            errors.popNestedPath()
        }
    }
}
