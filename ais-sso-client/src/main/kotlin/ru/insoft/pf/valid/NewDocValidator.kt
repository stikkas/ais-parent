package ru.insoft.pf.valid

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.pf.models.ManInfo
import ru.insoft.pf.repo.PrivateFilesMapper

@Component
class NewDocValidator(private val docValidator: DocumentValidator, private val pfm: PrivateFilesMapper) : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as ManInfo) {
        if (docs.isNullOrEmpty()) {
            errors.rejectValue("docs", "Документ")
        } else {
            try {
                errors.pushNestedPath("docs[0]")
                docValidator.validate(docs[0], errors, id)
                docs[0].typeId?.let {
                    if (!errors.hasFieldErrors("issueDate")) {
                        pfm.existsActualDocDate(id!!, it)?.let { old ->
                            if (docs[0].issueDate?.isBefore(old) == true) {
                                errors.rejectValue(
                                    "issueDate",
                                    "Дата выдачи нового документа меньше даты выдачи существующего документа"
                                )
                            }
                        }
                    }
                }
            } finally {
                errors.popNestedPath()
            }
        }
    }

    override fun supports(clazz: Class<*>) = ManInfo::class.java == clazz
}

