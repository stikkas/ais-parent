package ru.insoft.pf.valid

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.libs.max50
import ru.insoft.libs.models.DictValue
import ru.insoft.pf.models.DwellingPlace

@Component
class DwellingPlaceValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as DwellingPlace) {
        val isReg = addressType == DictValue.REG_ADDR
        if (addressText.isNullOrBlank() && levels.isEmpty()) {
            errors.rejectValue("levels", if (isReg) "Адрес регистрации" else "Фактический адрес")
        }
        if (isReg) {
            regType ?: errors.rejectValue("regType", "Регистрация")
            startDate ?: errors.rejectValue("startDate", "Дата начала регистрации")
        }
        flat?.let {
            if (it.length > max50) {
                errors.rejectValue("flat", "Квартир(а,ы)")
            } else if (it.split(",").any {
                    it.contains("-") && it.split("-").any { it.trim().toIntOrNull() == null }
                }) {
                errors.rejectValue("flat", "Номера квартир с символами должны быть введены через запятую")
            }
        }
        room?.let {
            if (it.length > max50) {
                errors.rejectValue("room", "Комнат(а,ы)")
            }
        }
    }

    override fun supports(clazz: Class<*>) = DwellingPlace::class.java == clazz
}

