package ru.insoft.pf.valid

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.libs.smevs.SmevReqestData
import ru.insoft.pf.models.SmevRequest

@Component
class SendRequestValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as SmevRequest) {
        smevId?.let {
            SmevReqestData.toRequestData(it) ?: throw Exception("Данный тип запроса не обрабатывается")
        } ?: errors.rejectValue("smevId", "Тип запроса")
    }

    override fun supports(clazz: Class<*>) = SmevRequest::class.java == clazz
}

