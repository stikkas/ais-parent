package ru.insoft.pf.valid

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import ru.insoft.libs.formatters.DateFormatter
import ru.insoft.libs.models.Document
import ru.insoft.libs.services.SharedDictService
import ru.insoft.pf.PrivateFilesService
import java.time.LocalDate

@Component
class DocumentValidator(private val dictService: SharedDictService, private val pfs: PrivateFilesService) {

    private val passportTypeId = "9"

    fun validate(doc: Document, errors: Errors, manId: String?) {
        val types = dictService.docTypes()
        doc.typeId ?: errors.rejectValue("typeId", "Документ")

        // Дата выдачи Паспорта РФ не может быть меньше 01.07.1997.
        if (doc.issueDate == null || (doc.typeId == passportTypeId && doc.issueDate?.isBefore(
                LocalDate.parse(
                    "01.07.1997",
                    DateFormatter.format
                )
            ) == true)
        ) {
            errors.rejectValue("issueDate", "Дата выдачи документа")
        } else if (doc.issueDate?.isAfter(LocalDate.now()) == true) {
            errors.rejectValue("issueDate", "Дата выдачи документа больше текущей даты")
        }

        if (doc.typeId == passportTypeId) { // Паспорт РФ
            ValidationUtils.rejectIfEmpty(errors, "series", "Серия")
            ValidationUtils.rejectIfEmpty(errors, "number", "Номер")
//            if (!doc.series.isNullOrEmpty() && !doc.number.isNullOrEmpty()) {
//                pfs.findExistPasport(manId, doc.series!!, doc.number!!, doc.typeId!!)?.let {
//                    errors.rejectValue(
//                        "active",
//                        "Такой паспорт уже внесен в БД, принадлежит ${it.fio}, ${it.organization}"
//                    )
//                }
//            }
        }

        doc.series?.let { s ->
            if (s.isNotEmpty()) {
                if (doc.typeId != null) {
                    types.find { it.value == doc.typeId }?.let {
                        if (if (it.series.endsWith("9")) {
                                s.length != it.series.length
                            } else {
                                s.length > it.series.length
                            }
                        ) {
                            errors.rejectValue("series", "Серия документа")
                        }
                    }
                }
            }
        }
        doc.number?.let { n ->
            if (n.isNotEmpty()) {
                if (doc.typeId != null) {
                    types.find { it.value == doc.typeId }?.let {
                        if (if (it.number.endsWith("9")) {
                                n.length != it.number.length
                            } else {
                                n.length > it.number.length
                            }
                        ) {
                            errors.rejectValue("number", "Номер документа")
                        }
                    }
                }
            }
        }
        doc.depCode?.let {
            if (it.isNotEmpty()) {
                if (it.length != 7) {
                    errors.rejectValue("depCode", "Код подразделения")
                }
            }
        }
    }
}
