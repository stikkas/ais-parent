package ru.insoft.pf

import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.DigestUtils
import org.springframework.web.multipart.MultipartFile
import ru.insoft.libs.*
import ru.insoft.libs.models.Address
import ru.insoft.libs.models.Classifiers
import ru.insoft.libs.models.Classifiers.acceptDisabled
import ru.insoft.libs.models.Classifiers.acceptPartDisabled
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.models.Document
import ru.insoft.libs.repo.CommonMapper
import ru.insoft.libs.smevs.SmevReqestData
import ru.insoft.libs.smevs.birth.BirthRequest
import ru.insoft.libs.smevs.death.DeathRequest
import ru.insoft.libs.smevs.divorce.DivorceRequest
import ru.insoft.libs.smevs.egrn.AplEgrnRequest
import ru.insoft.libs.smevs.marry.BrakRequest
import ru.insoft.libs.smevs.opis.Request
import ru.insoft.libs.smevs.opis.StatementFile
import ru.insoft.libs.smevs.sud.SudimostRequest
import ru.insoft.models.reqs.*
import ru.insoft.pf.models.*
import ru.insoft.pf.repo.PrivateFilesMapper
import ru.insoft.pf.repo.RequestsMapper
import ru.insoft.repo.UtilsMapper
import ru.insoft.services.*
import java.io.InputStream
import java.io.StringWriter
import java.nio.file.*
import java.time.LocalDate
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller

private val logger = KotlinLogging.logger { }

@Service
class PrivateFilesServiceImpl(
    private val pfm: PrivateFilesMapper,
    private val us: MeService,
    private val utils: UtilsService,
    private val smevMapper: RequestsMapper,
    private val cm: CommonMapper,
    private val um: UtilsMapper
) : PrivateFilesService {

    @Value("\${app.ais-id}")
    var aisId: Int = 0

    @Value("\${app.files.pgu}")
    lateinit var rootPguPath: String

    @Value("\${app.files.root}")
    lateinit var rootFiles: String

    @Transactional(readOnly = true)
    override fun rootAddress(orgId: String) = pfm.findRootAddress(orgId)

    @Transactional(readOnly = true)
    override fun hasSameRegAddress(
        systemmanId: String,
        addressId: String,
        startDate: LocalDate,
        endDate: LocalDate?
    ) = pfm.hasSameRegAddress(systemmanId, addressId, startDate, endDate)

    @Transactional(readOnly = true)
    override fun hasActualRegWithAddress(
        manId: String,
        flats: List<String>?,
        addressId: String,
        flat: String?,
        addressText: String?
    ) = pfm.hasActualRegWithAddress(manId, flats, addressId, flat, addressText)

    @Transactional(readOnly = true)
    override fun findSameRegistration(manId: String, regType: DictValue?, startDate: LocalDate?) =
        pfm.findSameRegistration(manId, regType, startDate)

    @Transactional(readOnly = true)
    override fun getZagsInfo(manId: String) = withAddLast(manId) {
        pfm.zagsInfo(manId, currentOrgId())
    }

    @Transactional(readOnly = true)
    override fun getRegsInfo(manId: String) = withAddLast(manId) {
        pfm.regsInfo(manId, currentOrgId())
    }

    @Transactional(readOnly = true)
    override fun getOtherInfo(manId: String) = withAddLast(manId) {
        pfm.otherInfo(manId, currentOrgId())
    }

    @Transactional(readOnly = true)
    override fun getAddedInfo(manId: String) = withAddLast(manId) {
        pfm.addedRequisites(manId, currentOrgId())
    }

    @Transactional(readOnly = true)
    override fun findExistSnils(manId: String?, snils: String) = pfm.findExistSnils(manId, snils)

    @Transactional(readOnly = true)
    override fun findExistDoc(manId: String?, typeId: String, series: String?, nomer: String?) =
        pfm.findExistDocs(manId, typeId, series, nomer)

    @Transactional(readOnly = true)
    override fun findTwins(
        manId: String?, lastName: String?, firstName: String?, birthDate: LocalDate?,
        levelId: String?, birthAddress: Address?
    ) = pfm.findTwins(manId, lastName, firstName, birthDate, levelId, birthAddress, currentOrgId())

    @Transactional(readOnly = true)
    override fun findExistPasport(manId: String?, series: String, nomer: String, typeId: String) =
        pfm.findExistPasport(manId, series, nomer, typeId)

    @Transactional(readOnly = true)
    override fun getChangesInfo(manId: String) = withAddLast(manId) {
        runBlocking {
            val places = async {
                pfm.dwellingPlaces(manId)?.filterNotNull()?.map {
                    ChangeAddress(
                        if (it.addressType == DictValue.FACT_ADDR) "Фактический адрес" else it.regTypeStr ?: "",
                        formatAddressToString(it), it.startDate, it.endDate
                    )
                }
            }
            pfm.changeInfo(manId, currentOrgId())?.apply {
                fios?.let {
                    it.forEach {
                        val id = it.id!!
                        launch { it.birthPlace = utils.getBirthAddress(id) }
                    }
                }
                regs = places.await()
            }
        }
    }

    @Transactional(readOnly = true)
    override fun getSmevInfo(id: String) = smevMapper.smevsInfo(id, currentOrgId())?.apply {
        smevs?.forEach {
            it.fileName?.let { fn ->
                it.fileName = Paths.get("smev_plea", fn).toString()
            }
        }
    }

    @Transactional(readOnly = true)
    override fun getManInfo(systemManId: String): ManInfo? {
        val info = pfm.manInfo(systemManId, currentOrgId(), aisId)
        if (info != null) {
            us.addLast(systemManId, currentUser()?.id!!)
            info.regs?.forEach { it?.levels?.let { l -> setChilds(l) } }
            info.birthAddress?.levels?.let { l -> setChilds(l) }
            info.regs = info.regs?.filterNotNull()
            info.docs.forEach { d ->
                d.fileName?.let { d.fileName = Paths.get(MAN_DOC_DIR, d.id, it).toString() }
            }
            if (aisId == AisId.RUG.id && info.age?.let { it < 18 } == true) {
                info.relatives = pfm.findRelatives(systemManId)
            }
        }
        return info
    }

    @Transactional(readOnly = true)
    override fun canPrintHistory(manId: String) = pfm.hasOpenLivePlaceRegistration(manId, currentOrgId())

    @Transactional(readOnly = true)
    override fun isRegistered(systemmanId: String, type: DictValue) = when (aisId) {
        AisId.NL.id -> pfm.isRegisteredBddsop(systemmanId)
        AisId.OP.id -> pfm.isRegistered(systemmanId, type, currentOrgId())
        else -> false
    }

    @Transactional(readOnly = true)
    override fun lastEventNotAdopt(id: String) = pfm.lastEventType(id) != Classifiers.adcar

    @Transactional
    override fun createPf(manId: String) = pfm.insertOwnerPf(manId, currentOrgId())

    @Transactional
    override fun saveManInfo(info: ManInfo, files: Map<Any, MultipartFile>?): ManInfo {
        var id = info.id
        val existsIds = if (id == null) null else info.regs?.mapNotNull { it?.id }
        if (id == null) {
            pfm.insertManInfo(info, currentUser()!!.id)
            pfm.insertOwnerPf(info.id!!, currentOrgId())
            pfm.insertDetails(info)
            id = info.id ?: throw RuntimeException("inserted without id")
            info.docs.forEach {
                saveDocFile(it, files) { pfm.insertDocument(it, info.id!!) }
            }
            info.regs?.forEach { savePlace(it, id) }
        } else {
            pfm.updateManInfo(info)
            val docsIds = ArrayList<String>(info.docs.size)

            info.docs.forEach {
                saveDocFile(it, files) {
                    if (it.id != null) {
                        pfm.updateDocument(it)
                    } else {
                        pfm.insertDocument(it, id)
                    }
                }
                docsIds.add(it.id!!)
            }
            pfm.removeDocs(id, docsIds)
            pfm.removeOldPlaces(id, info.regs?.filterNotNull()?.onEach { savePlace(it, id, true) })
        }
        if (aisId == AisId.RUG.id) { // Регистрация населения
            info.id?.let { mainManId ->
                val startDate = info.regs?.firstOrNull()?.startDate
                // У всех существующих (а они должны имет уже memberId) с такой же ролью как у новых
                // нужно проставить дату окончания отношения
                val exists = info.relatives?.filter { it.memberId != null }
                val endDate = startDate?.minusDays(1)
                info.relatives?.forEach { r ->
                    if (r.memberId == null) {
                        exists?.find { it.role == r.role }?.let {
                            it.endDate = endDate
                        }
                    }
                }
                pfm.removeRelatives(mainManId, info.relatives?.map { rel ->
                    rel.memberId?.let {
                        pfm.updateRelative(rel, startDate)
                    } ?: pfm.insertRelative(rel, mainManId, startDate, currentUser()?.id)
                    rel.memberId!!
                })
                existsIds?.let { exs ->
                    info.regs?.forEach { dp ->
                        dp?.let { place ->
                            val placeId = place.id!!
                            if (!exs.contains(placeId)) { // Новый адрес для уже существующего ЛД
                                pfm.findSameAddress(placeId, mainManId, place.regType)?.let {
                                    pfm.setLeaveForRegistration(
                                        // it - id из таблицы registration
                                        it, place.startDate?.minusDays(1),
                                        getMainAddressId(place.levels)
                                    )
                                    pfm.setEventAddress(placeId, it)
                                }
                            }
                        }
                    }
                }
            }
        }
        return info
    }

    @Transactional
    override fun savePersonalData(info: ManInfo, files: Map<Any, MultipartFile>?) = runBlocking {
        val userId = currentUser()!!.id
        launch { pfm.insertPersonalData(info, userId) }
        launch { saveNewDoc(info, files) }
        info
    }

    @Transactional
    override fun saveNewDoc(info: ManInfo, files: Map<Any, MultipartFile>?): ManInfo {
        val doc = info.docs[0]
        saveDocFile(doc, files) { pfm.insertDocument(doc, info.id!!) }
        pfm.disactualOldDoc(info.docs[0].id!!, info.docs[0].typeId!!, info.id!!)
        return info
    }

    @Transactional
    override fun saveZagsItem(item: ZagsItem, id: String): ZagsItem {
        var certDate: LocalDate? = null
        item.id?.let {
            certDate = pfm.selectCertDate(it)
            pfm.updateZags(item)
            if (item.typeId == DictValue.DEATH_CERT.id && aisId == AisId.RUG.id) {
                pfm.updateEndDates(id, item.date)
            }
        } ?: run {
            item.id = pfm.insertZags(item, id)
            if (item.typeId == DictValue.DEATH_CERT.id && aisId == AisId.RUG.id) {
                pfm.setEndDates(id, item.date)
            }
        }
        if (item.typeId == DictValue.DEATH_CERT.id) {
            item.date?.let { pfm.setDeathDate(id, it) }
        }
        item.spouseId?.let {
            pfm.saveCertToSpouse(item.id, item.manId, it, certDate)
        }
        return item
    }

    @Transactional
    override fun saveRegInfo(regs: List<ActualRegistration>): Boolean {
        regs.forEach { r ->
            pfm.removeFamilies(r.id!!,
                r.people?.map { rm ->
                    if (rm.familyId == null) {
                        rm.familyId = pfm.createFamily(r.startDate, rm.value, currentUser()?.id)
                        pfm.createFamilyLinks(rm.familyId, r.id)
                    }
                    pfm.removeRegRelatives(rm.familyId!!,
                        rm.men?.map { m ->
                            m.memId?.let {
                                pfm.updateRelativeId(it, m.relativeId)
                            } ?: run {
                                m.memId = pfm.insertMember(
                                    rm.familyId!!, m.id, rm.value, r.startDate, r.endDate,
                                    m.relativeId, currentUser()?.id, r.id
                                )
                            }
                            m.memId!!
                        })
                    rm.familyId!!
                })
        }
        return true
    }

    @Transactional
    override fun saveAddedReq(info: AddedRequisites) = runBlocking {
        val manId = info.id!!
        launch {
            pfm.removeSocStatuses(manId,
                info.states?.map { s ->
                    async {
                        s.id?.let { pfm.updateSocStatus(s) } ?: pfm.insertSocStatus(s, manId)
                        s.id!!
                    }
                }?.map { it.await() })
        }
        launch {
            pfm.removeEducs(manId,
                info.edus?.map { e ->
                    async {
                        e.id?.let { pfm.updateEducation(e) } ?: pfm.insertEducation(e, manId)
                        e.id!!
                    }
                }?.map { it.await() })
        }
        launch {
            pfm.removeTrains(manId,
                info.trains?.map { t ->
                    async {
                        t.id?.let { pfm.updateTraining(t) } ?: pfm.insertTraining(t, manId)
                        t.id!!
                    }
                }?.map { it.await() })
        }
        launch {
            pfm.removeJobs(manId,
                info.jobs?.map { j ->
                    async {
                        j.id?.let { pfm.updateJob(j) } ?: pfm.insertJob(j, manId)
                        j.id!!
                    }
                }?.map { it.await() })
        }
        info
    }

    @Transactional
    override fun saveOther(item: InvIncItem, id: String): InvIncItem {
        item.invalid?.let {
            var updated = false
            utils.saveDoc(it.invDoc) {
                it.invDoc = null
                utils.updateInvalidity(it)
                updated = true
            }
            it.invalidityId?.let { _ ->
                if (!updated) {
                    utils.updateInvalidity(it)
                }
            } ?: if (it.invalidGroupId != null || it.invDate !== null || it.invDoc != null) {
                pfm.insertInvalidity(it, id)
            }
        }
        item.incapable?.let { utils.saveDisability(it, id) }
        return item
    }

    @Transactional
    override fun saveSmevs(info: SmevsInfo) = runBlocking {
        val smevId = info.id!!
        val orgId = currentOrgId()
        val userId = currentUser()!!.id
        smevMapper.removeSmevs(smevId,
            info.smevs?.map { req ->
                async {
                    req.id ?: smevMapper.insertSmevRequest(req, smevId, orgId, userId)
                    req.id!!
                }
            }?.map { it.await() }
        )
        info
    }

    @Transactional
    override fun remove(id: String) = pfm.remove(id) ?: run {
        pfm.removeManDetails(id)
        pfm.findDisabilityByIdOrManId(id)?.let {
            if (it.isNotEmpty()) {
                pfm.removeDisablility(it.map { it.id })
                it.forEach { changeEgisso(it.docId, it.docTypeId, it.manId) }
            }
        }
        us.remove(id, currentUser()?.id!!)
        null
    }

    @Transactional
    override fun removeZags(id: String) = if (pfm.hasAppeal(id)) {
        false
    } else {
        if (aisId == AisId.RUG.id) {
            pfm.manIdOfDeathCert(id)?.let {
                pfm.updateEndDates(it, null)
            }
        }
        pfm.removeZags(id)
        true
    }

    @Transactional
    override fun removeOther(id: String, dis: Boolean?): Boolean {
        if (dis == true) {
            pfm.findDisabilityByIdOrManId(null, id)?.firstOrNull()?.let {
                pfm.removeDisablility(listOf(id))
                changeEgisso(it.docId, it.docTypeId, it.manId)
            }
        } else {
            pfm.removeInvalidity(id)
        }
        return true
    }

    @Transactional
    override fun sendRequest(req: SmevRequest, manId: String) = SmevReqestData.toRequestData(req.smevId!!)?.let {
        val (obj, path) = when (it) {
            SmevReqestData.CHECK_PASS -> PassCreatorRequest(pfm.passInfo(manId))
            SmevReqestData.SNILS -> SnilsCreatorRequest(pfm.manSnilsInfo(manId))
            SmevReqestData.SUDIMOST_EXISTS -> SudimostCreatorRequest(getManInfo(manId))
            SmevReqestData.BRAK -> MarriageCreatorRequest(pfm.manMarryInfo(manId))
            SmevReqestData.DIVORCE -> DivorceCreatorRequest(pfm.manDivorceInfo(manId))
            SmevReqestData.APL_EGRN -> AplEgrnCreatorRequest(pfm.manAplEgrnReq(manId, currentOrgId()))
            SmevReqestData.BIRTH -> BirthCreatorRequest(pfm.manBirthInfo(manId))
            SmevReqestData.DEATH -> DeathCreatorRequest(pfm.manDeathInfo(manId))
            SmevReqestData.PENSION -> PensionCreatorRequest(pfm.manPensionReq(manId))
            SmevReqestData.SOCPAY -> SocPayCreatorRequest(pfm.manSocPayReq(manId))
        }
        req.request = obj.toString()
        req.reqDate = LocalDate.now()
        req.statusId = DictValue.SENT_REQ_STATUS.id
        req.id?.let { smevMapper.updateSentRequest(req) } ?: smevMapper.insertSentRequest(
            req,
            manId,
            currentOrgId(),
            currentUser()!!.id
        )
        when (obj) {
            is SudimostRequest -> obj.Id = req.code
            is BrakRequest -> obj.id = "${req.id}"
            is DivorceRequest -> obj.id = "${req.id}"
            is BirthRequest -> obj.id = "${req.id}"
            is DeathRequest -> obj.id = "${req.id}"
            is AplEgrnRequest -> obj.id = "${req.id}"
        }

        val dir = Paths.get(rootPguPath, TO_PGU, path)
        Files.createDirectories(dir)
        val fileName = "0${aisId}in${req.code}.xml"
        if (obj is AplEgrnRequest) {
            var attachemntPath: Path? = null
            val copyName = obj.header?.appliedDocument?.idDocument?.let { d ->
                d.attachment?.fileDesc?.file?.let { f ->
                    f.fileName?.apply {
                        attachemntPath = Paths.get(rootFiles, MAN_DOC_DIR, d.docId, this)
                        Files.newInputStream(attachemntPath).use { s ->
                            f.md5sum = DigestUtils.md5DigestAsHex(s).toUpperCase()
                        }
                    }
                }
            }
            ZipOutputStream(
                Files.newOutputStream(
                    dir.resolve(fileName.replaceAfterLast('.', "zip")), StandardOpenOption.CREATE
                )
            ).use { z ->
                addToZipXml(obj, z, fileName)
                addToZipXml(Request().apply {
                    statementFile = StatementFile().apply { this.fileName = fileName }
                    copyName?.let { c -> file = StatementFile().apply { this.fileName = c } }
                }, z, "request.xml")
                attachemntPath?.let { scan ->
                    z.putNextEntry(ZipEntry(copyName))
                    Files.newInputStream(scan).use { ins -> ins.copyTo(z, 1024) }
                }
            }
        } else {
            Files.newBufferedWriter(dir.resolve(fileName), StandardOpenOption.CREATE).use { s ->
                JAXBContext.newInstance(obj.javaClass).createMarshaller().apply {
                    setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true)
                }.marshal(obj, s)
            }
        }
        req
    }


    /**
     * Добавляет xml file к zip архиву
     */
    private fun addToZipXml(obj: Any, zip: ZipOutputStream, fileName: String) {
        val sw = StringWriter()
        JAXBContext.newInstance(obj.javaClass).createMarshaller().apply {
            setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true)
        }.marshal(obj, sw)
        zip.putNextEntry(ZipEntry(fileName))
        val bytes = ByteArray(1024)
        var length: Int
        val bais = sw.toString().byteInputStream()
        while (bais.read(bytes).also { length = it } >= 0) {
            zip.write(bytes, 0, length)
        }
    }

    /**
     * Удаляет, вставляет записи в ЕГИССО (Реестры) при удалении из таблицы disability
     * Так же меняет статус документа, или удаляет его
     */
    private fun changeEgisso(docId: String?, docTypeId: String?, manId: String) = docTypeId?.let { t ->
        if (t == acceptDisabled || t == acceptPartDisabled || t == acceptNoDisabled) {
            docId?.let {
                val recs = utils.findEgissoRegistrys(it)
                if (recs.isEmpty() || recs.all { it.sentDate == null }) {
                    if (recs.isNotEmpty()) {
                        utils.removeEgisso(recs)
                    }
                    cm.removeOtherDoc(it)
                } else {
                    cm.removeOtherDoc(it, true)
                    when (t) {
                        acceptDisabled -> listOf(priznanieDisabled)
                        acceptPartDisabled -> listOf(limitActivity)
                        acceptNoDisabled -> listOf(revokeLimitActivity, revokeDisabled)
                        else -> null
                    }?.let { ev ->
                        utils.findAndRemoveEgissoRegistry(
                            manId, it, ev
                        )?.forEach {
                            um.insertEgisso(
                                changedActivityReestr, removeEvent, currentOrgId(),
                                LocalDate.now(), null, manId, it.id
                            )
                        }
                    }
                }
            }
        }
    }

    /**
     * Добавляет запись о последнем просмотренном ЛД в список "Последние 10 дел"
     */
    private fun <T> withAddLast(manId: String, block: () -> T?) = block()?.apply {
        us.addLast(manId, currentUser()?.id!!)
    }

    private fun savePlace(place: DwellingPlace?, systemManId: String, forExistsMan: Boolean = false) = place?.let { p ->
        logger.debug { "Source flat is '${place.flat}'" }
        val flats = flatToFlats(place.flat)
        logger.debug { "Flats after parsing are '$flats'" }
        p.startDate = p.startDate ?: LocalDate.now()
        val leaveDate = if (p.endDate?.isBefore(LocalDate.now()) == true) p.endDate else null
        if (p.addressText.isNullOrBlank()) {
            logger.debug { "Saving classified address" }
            getMainAddressId(p.levels)?.let { aid ->
                logger.debug { "Result addres id is '$aid'" }
                var dwIds = if (flats.isNullOrEmpty()) {
                    logger.debug { "Flats are empty" }
                    listOf(pfm.findExistDwelling(null, p.room, aid) ?: pfm.insertPlaceWithAdr(place, aid))
                } else {
                    logger.debug { "Flats exist" }
                    flats.map { pfm.findExistDwelling(it, p.room, aid) ?: pfm.insertPlaceWithAdr(place, aid, it) }
                }
                var relationId = pfm.findMainRelationId(systemManId, p.id?.let {
                    if (!dwIds.contains(it)) {
                        dwIds + it
                    } else {
                        dwIds
                    }
                } ?: dwIds, p.addressType)
                logger.debug { "Found relationId is $relationId" }

                if (relationId != null) {
                    val oid = dwIds.first()
                    pfm.updateRelationship(p, relationId, oid)
                    logger.debug { "Relationship with id = $relationId updated by objectsystemid = $oid" }
                    if (p.addressType == DictValue.REG_ADDR) {
                        p.regId?.let { pfm.updateRegistration(p, relationId!!, leaveDate) } ?: pfm.insertRegistration(
                            p, relationId, leaveDate
                        )
                        logger.debug { "Registration updated" }
                    }
                    p.id = oid
                    dwIds = dwIds.subList(1, dwIds.size)
                    logger.debug { "Place's id is ${p.id} now" }
                    logger.debug { "Remained dwelling ids are '$dwIds'" }
                }

                dwIds.forEach { id ->
                    val relId = pfm.insertRelationship(p, systemManId, id, relationId)
                    logger.debug { "In relationships was created record with id = $relId and systemobjectid = $id and masterrelationid = $relationId" }
                    if (relationId == null) {
                        relationId = relId
                        p.id = id
                        if (p.addressType == DictValue.REG_ADDR) {
                            createRegistration(forExistsMan, systemManId, p, relId, leaveDate, aid)
                            logger.debug { "Registration was inserted for relationid = $relId" }
                        }
                        logger.debug { "Masterrelationid is $relationId now. SystemobjectId is ${p.id} now" }
                    }
                }
            }
        } else {
            val placeId = p.id
            if (placeId == null) {
                val flat = flats?.firstOrNull()
                val pid = pfm.insertDwellingPlace(p, flat)
                p.id = pid
                val relationId = pfm.insertRelationship(p, systemManId, pid, null)
                if (p.addressType == DictValue.REG_ADDR) {
                    createRegistration(forExistsMan, systemManId, p, relationId, leaveDate, getMainAddressId(p.levels))
                }
                flats?.let {
                    if (it.size > 1) {
                        it.subList(1, it.size).map { f -> pfm.insertDwellingPlace(p, f) }.forEach {
                            pfm.insertRelationship(p, systemManId, it, relationId)
                        }
                    }
                }
            } else {
                val exists = pfm.findExistPlaces(placeId).toMutableList()
                val relationId = exists.firstOrNull()?.second ?: pfm.insertRelationship(p, systemManId, placeId, null)
                val flat = flats?.firstOrNull()
                pfm.updateDwellingPlace(p, flat)
                exists.removeIf { it.first == placeId }
                pfm.removeRelations(relationId)

                flats?.let {
                    if (it.size > 1) {
                        it.subList(1, it.size).map { f ->
                            exists.removeFirstOrNull()?.let { eid ->
                                pfm.updateDwellingPlace(p, f, eid.first)
                                eid.first
                            } ?: pfm.insertDwellingPlace(p, f)
                        }.forEach {
                            pfm.insertRelationship(p, systemManId, it, relationId)
                        }
                    }
                }

                if (exists.isNotEmpty()) {
                    pfm.removePlaces(exists.map { it.first })
                }
                if (p.addressType == DictValue.REG_ADDR) {
                    p.regId?.let { pfm.updateRegistration(p, relationId, leaveDate) } ?: pfm.insertRegistration(
                        p,
                        relationId,
                        leaveDate
                    )
                    logger.debug { "Registration updated" }
                }
            }
        }
        p.flat = flats?.joinToString(",")
        p.ownerDate?.let { date ->
            pfm.saveOwnership(systemManId, p.id, date, p.numerator, p.denominator)
        } ?: pfm.removeOwnership(systemManId, p.id)
    }

    private fun createRegistration(
        forExistsMan: Boolean, systemManId: String, place: DwellingPlace, relationId: String,
        leaveDate: LocalDate?,
        addressId: String?
    ) {
        if (aisId == AisId.RUG.id) {
            if (forExistsMan) {
                if (pfm.hasNotRegInCurOrg(systemManId, currentOrgId())) {
                    // нет адреса регистрации под корневым адресом организации пользователя
                    pfm.insertRegistration(place, relationId, leaveDate, false, addressId)
                } else {
                    pfm.insertRegistration(place, relationId, leaveDate)
                }
            } else {
                pfm.insertRegistration(place, relationId, leaveDate, true)
            }
        } else {
            pfm.insertRegistration(place, relationId, leaveDate)
        }
    }

    private fun saveFile(stream: InputStream, vararg path: String) {
        val filePath = Paths.get(rootFiles, *path)
        Files.createDirectories(filePath.parent)
        Files.copy(stream, filePath, StandardCopyOption.REPLACE_EXISTING)
    }

    private fun saveDocFile(doc: Document, files: Map<Any, MultipartFile>?, block: () -> Unit) {
        val file = files?.get(doc)
        doc.fileName = fileNameToSave(file, doc.fileName, true)
        block()
        file?.let { f -> saveFile(f.inputStream, MAN_DOC_DIR, doc.id!!, doc.fileName!!) }
        doc.fileName?.let { n -> doc.fileName = Paths.get(MAN_DOC_DIR, doc.id, n).toString() }
    }

    companion object {
        const val TO_PGU = "EPGU/IN"
        const val MAN_DOC_DIR = "mandocuments"
    }
}

