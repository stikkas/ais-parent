package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.insoft.libs.models.Address
import ru.insoft.libs.models.DictValue
import java.time.LocalDate

/**
 * Места регистрации и проживания
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class DwellingPlace : Address() {

    /**
     * Номера квартир
     */
    var flat: String? = null

    /**
     * Номера комнат
     */
    var room: String? = null

    /**
     * Тип регистрации
     */
    var regType: DictValue? = null

    /**
     * Идентификатор регистрации
     */
    var regId: String? = null

    /**
     * Тип регистрации (для отображения в таблице)
     */
    var regTypeStr: String? = null

    /**
     * Тип адреса
     */
    var addressType: DictValue? = null

    /**
     *  Дата начала регистрации
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var startDate: LocalDate? = null

    /**
     *  Дата оканчания регистрации (для 'Регистрация по месту пребывания')
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var endDate: LocalDate? = null

    /**
     *  Дата прибытия / убытия
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var eventDate: LocalDate? = null

    /**
     * Причина прибытия
     */
    var arrivalCauseId: String? = null

    /**
     * Основание проживание
     */
    var lifeGroundId: String? = null

    /**
     * Дата собственника
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var ownerDate: LocalDate? = null

    /**
     * Доля собстенника числитель
     */
    var numerator: Int? = null

    /**
     * Доля собстенника знаменатель
     */
    var denominator: Int? = null
}



