package ru.insoft.pf.models

/**
 * Информация на вкладке "Инвалидность/Недееспособность"
 */
class InvalidIncapable : PfInitials() {
    var invalids: List<InvalidDetails>? = null
    var incaps: List<IncapableDetails>? = null
}
