package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

/**
 * Социальный статус
 */
class SocialStatus {
    var id: Long? = null

    /**
     * Название (из справочника)
     */
    var statusId: String? = null
    var statusName: String? = null

    /**
     * Дата установки
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var startDate: LocalDate? = null

    /**
     * Дата снятия
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var endDate: LocalDate? = null
}
