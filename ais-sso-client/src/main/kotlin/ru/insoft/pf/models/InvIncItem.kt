package ru.insoft.pf.models

/**
 * Данные для сохранения данных с закладки Инвалидность/Недееспособность
 */
class InvIncItem {
    var invalid: InvalidDetails? = null
    var incapable: IncapableDetails? = null
}
