package ru.insoft.pf.models

import ru.insoft.libs.models.Document

/**
 * Данные для "Изменение ФИО/ДУЛ/адреса"
 */
class ChangeInfo : PfInitials() {
    /**
     * Изменения персональных данных
     */
    var fios: List<ChangeFio>? = null

    /**
     * Документы, удостоверяющие личность
     */
    var docs: List<Document>? = null

    /**
     * Адреса
     */
    var regs: List<ChangeAddress>? = null
}
