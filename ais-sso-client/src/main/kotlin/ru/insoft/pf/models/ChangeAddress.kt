package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

/**
 * Адрес проживания
 */
class ChangeAddress(
    /**
     * Регистрация
     */
    val type: String,
    /**
     * Адрес
     */
    val address: String?,
    /**
     * Дата начала
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    val startDate: LocalDate?,
    /**
     * Дата окончания
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    val endDate: LocalDate?
)
