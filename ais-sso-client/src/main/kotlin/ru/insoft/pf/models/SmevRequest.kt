package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

/**
 * Электронный запрос СМЭВ из личного дела человека
 */
open class SmevRequest {
    /**
     * Идентификатор в таблице smev_plea
     */
    open var id: Long? = null

    /**
     * Вид сведений, запрос из справочника smev с видом взаимодействия "Электронное"
     */
    var smevId: Int? = null
    var smevName: String? = null

    /**
     * Код запроса
     */
    var code: String? = null

    /**
     * Организация
     */
    var orgName: String? = null
    var orgId: String? = null

    /**
     * Дата запроса
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var reqDate: LocalDate? = null

    /**
     * Дата ответа
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var ansDate: LocalDate? = null

    /**
     * Статус запроса
     */
    var statusId: String? = null

    /**
     * Имя файла, скана документа
     */
    open var fileName: String? = null

    /**
     * Текст запроса
     */
    var request: String? = null

    /**
     * Текст ответа
     */
    var answer: String? = null

}
