package ru.insoft.pf.models

class RegisteredInfo : PfInitials() {
    /**
     * Адреса регистрации
     */
    var regs: List<ActualRegistration>? = null
}
