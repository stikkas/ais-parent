package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import ru.insoft.libs.models.Document
import java.time.LocalDate

class InvalidDetails {
    /**
     * Дата установки
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var invDate: LocalDate? = null

    /**
     * Группа инвалидности
     */
    var invalidGroupId: String? = null

    /**
     * Причина инвалидности
     */
    var causeId: String? = null

    /**
     * ID записи о инвалидности
     */
    var invalidityId: String? = null

    /**
     * Документ, устанавливающий инвалидность
     */
    var invDoc: Document? = null
}
