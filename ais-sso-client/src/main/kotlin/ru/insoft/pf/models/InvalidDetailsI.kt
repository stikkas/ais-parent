package ru.insoft.pf.models

import ru.insoft.libs.models.Document
import java.time.LocalDate

interface InvalidDetailsI {
    /**
     * Дата установки
     */
    var invDate: LocalDate?

    /**
     * Группа инвалидности
     */
    var invalidGroupId: String?

    /**
     * ID записи о инвалидности
     */
    var invalidityId: String?

    /**
     * Документ, устанавливающий инвалидность
     */
    var invDoc: Document?

    var causeId: String?
}
