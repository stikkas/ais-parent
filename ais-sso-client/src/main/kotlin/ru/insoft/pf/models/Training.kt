package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

/**
 * Информация о обучении
 */
class Training {
    var id: String? = null

    /**
     * Место учебы
     */
    var placeId: String? = null
    var place: String? = null

    /**
     * Вид образования
     */
    var kindId: String? = null

    /**
     * Класс / курс
     */
    var course: String? = null

    /**
     * Специальность
     */
    var beruf: String? = null

    /**
     * Дата начала
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var startDate: LocalDate? = null

    /**
     * Дата окончания
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var endDate: LocalDate? = null
}
