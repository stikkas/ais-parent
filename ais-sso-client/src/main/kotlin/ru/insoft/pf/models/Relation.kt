package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import ru.insoft.json.DictValueDeserializer
import ru.insoft.libs.models.DictValue
import ru.insoft.models.LdTableRow
import java.time.LocalDate

@JsonIgnoreProperties(ignoreUnknown = true)
class Relation() : LdTableRow() {

    constructor(info: LdTableRow) : this() {
        id = info.id
        fio = info.fio
        sex = info.sex
        birthDate = info.birthDate
        deathDate = info.deathDate
        documentType = info.documentType
        seria = info.seria
        number = info.number
    }


    /**
     * идентификатор члена семьи (familymemberid из family_members)
     */
    var memberId: String? = null

    /**
     * Родственное отношение
     */
    @JsonDeserialize(using = DictValueDeserializer::class)
    var role: DictValue? = null

    // Для УРН при получении данных из БД только
    val roleId: String?
        get() = role?.id

    /**
     * Дата окончания родственного отношения
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var endDate: LocalDate? = null

    /**
     * Дата рождения
     */
    @JsonIgnore
    var dateBirth: LocalDate? = null
}
