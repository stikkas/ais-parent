package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

/**
 * Данные по существующему СНИЛС
 */
data class SnilsCopy(
    val id: String,
    val lastName: String,
    val firstName: String,
    val middleName: String?,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    val birthDate: LocalDate?
) {
    var orgId: String? = null
    var orgName: String? = null
}
