package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

/**
 * Информация о образовании
 */
class Education {
    var id: Long? = null

    /**
     * Название (из справочника)
     */
    var typeId: String? = null

    /**
     * Дата сведений
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var infoDate: LocalDate? = null
}
