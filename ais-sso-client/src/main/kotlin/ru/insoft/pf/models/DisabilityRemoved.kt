package ru.insoft.pf.models

/**
 * Записи из таблицы disability для удаления
 */
class DisabilityRemoved(val id: String, val docId: String?, val docTypeId: String?, val manId: String)
