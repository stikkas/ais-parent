package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import ru.insoft.libs.formatAddressToString
import ru.insoft.libs.models.Address
import ru.insoft.libs.models.AddressDict
import java.time.LocalDate

class ActualRegistration {
    /**
     * systemobjectid from dwellingfund
     */
    var id: String? = null

    /**
     * Тип регистрации
     */
    var type: String? = null

    /**
     * Адрес
     */
    var address: String? = null
        get() = formatAddressToString(Address().also {
            it.levels = levels
            it.addressText = addressText
        })

    /**
     * Уровни адреса, например 'Страна' - 'Россия', 'Область' - 'Земляничная'
     */
    @JsonIgnore
    var levels: List<AddressDict> = emptyList()

    /**
     * Неклассифицированный адрес
     */
    @JsonIgnore
    var addressText: String? = null

    /**
     * Дата начала
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var startDate: LocalDate? = null

    /**
     * Дата окончания
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var endDate: LocalDate? = null

    /**
     * Организация
     */
    var org: String? = null

    /**
     * Список зарегистрированных
     */
    var people: List<MainRegMan>? = null

    /**
     * Можно или нет редактировать
     */
    var canEdit: Boolean = false

    /**
     * Доля собственности для ЛД
     */
    var numerator: Int? = null
    var denominator: Int? = null
}
