package ru.insoft.pf.models

/**
 * Информация на вкладке "Дополнительные реквизиты"
 */
class AddedRequisites : PfInitials() {

    /**
     * Социальные статусы
     */
    var states: List<SocialStatus>? = null

    /**
     * Образование
     */
    var edus: List<Education>? = null

    /**
     * Обучение
     */
    var trains: List<Training>? = null

    /**
     * Сведения о работе
     */
    var jobs: List<JobInfo>? = null
}
