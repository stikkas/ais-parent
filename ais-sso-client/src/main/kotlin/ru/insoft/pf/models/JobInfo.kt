package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

/**
 * Сведения о работе
 */
class JobInfo {
    var id: String? = null

    /**
     * Место работы
     */
    var place: String? = null

    /**
     * Должность
     */
    var position: String? = null

    /**
     * Телефон 1
     */
    var phone1: String? = null

    /**
     * Телефон 2
     */
    var phone2: String? = null

    /**
     * E-mail
     */
    var email: String? = null

    /**
     * Дата приема
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var startDate: LocalDate? = null

    /**
     * Дата увольнения
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var endDate: LocalDate? = null
}
