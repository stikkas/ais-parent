package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

class ChangeFio {
    /**
     * Идентификатор из таблицы population
     */
    var id: String? = null
    var lastName: String? = null
    var firstName: String? = null
    var middleName: String? = null

    /**
     * Пол
     */
    var sex: String? = null
    var birthDate: String? = null

    /**
     * начало периода события
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var startDate: LocalDate? = null

    /**
     * актуальная запись
     */
    var actual: Short? = null

    /**
     * место рождения
     */
    var birthPlace: String? = null

}
