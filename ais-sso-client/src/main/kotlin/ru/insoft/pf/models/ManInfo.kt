package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.insoft.libs.models.Address
import ru.insoft.libs.models.Document
import java.time.LocalDate

/**
 * Данные с карточки "Основные реквизиты"
 */
class ManInfo : PfInitials() {
    /**
     * Дата рождения, нужно это поле, только для записи в базу, так пользуемся текстовой датой
     */
    @JsonIgnore
    var birthDate: LocalDate? = null

    /**
     * Пол
     */
    var sex: Int = 0

    /**
     * ИНН
     */
    var inn: String? = null

    /**
     * СНИЛС
     */
    var snils: String? = null

    /**
     * Идентификатор национальности
     */
    var nationalityId: String? = null

    /**
     * Идентификатор гражданства
     */
    var citizenshipId: String? = null

    // Адреса
    /**
     * Адрес рождения
     */
    var birthAddress: Address? = Address()

    /**
     * В опеке используются "Адрес регистрации" REG_ADDR и "Фактический адрес" FACT_ADDR.
     * Для REG_ADDR есть три типа "По месту жительства" RES_PLACE_REG, "По месту пребывания" STAY_PLACE_REG, "Без регистрации"
     * Даты тоже нужны только адресу регистрации.
     * В таблице адресов при отображении мы пишем или тип регистрации (если REG_ADDR) или  "Фактический адрес" (если FACT_ADDR).
     * В БД для всех адресов заполняем dwellingfund и relationships, для адреса регистрации еще таблицу registration.
     * При записи в БД в табл. relationships поле ownertypeid заполняем значением "нет значения" UNKNOWN_PROP
     * даты начала и окончания регистрации записываются в таблицу registration (в таблицу relationships скопировать их же, если заполнены)
     * Если дата начала не заполнена, или это фактический адрес (нет даты начала регистрации), то писать текущую дату в дату начала в relationships.
     */
    /**
     * Все адреса регистрации и фактический адрес
     */
    var regs: List<DwellingPlace?>? = emptyList()

    // Документы
    /**
     * Все документы
     */
    var docs: List<Document> = emptyList()

    /**
     * Родители / законные представители (для "Учет и регистрация населения")
     */
    var relatives: List<Relation>? = null
}

