package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

class RegMan {
    /**
     * familymemberid from family_members
     */
    var memId: String? = null

    /**
     * systemmanid from population
     */
    var id: String? = null

    /**
     * Фамилия имя отчество
     */
    var fio: String? = null

    /**
     * Дата рождения
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var birthDate: LocalDate? = null

    /**
     * Дата начала
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var startDate: LocalDate? = null

    /**
     * Дата окончания
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var endDate: LocalDate? = null

    /**
     * Тип регистрации
     */
    var regType: String? = null

    /**
     * Родственное отношение
     */
    var relativeId: String? = null

    /**
     * Доля собственности
     */
    var numerator: Int? = null
    var denominator: Int? = null
}
