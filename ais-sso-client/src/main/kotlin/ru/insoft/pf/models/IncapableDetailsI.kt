package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import ru.insoft.libs.models.Document
import java.time.LocalDate

interface IncapableDetailsI {

    var disabilityId: String?

    /**
     * Дата установки
     */
    var incDate: LocalDate?

    /**
     *  Документ устанавливающий недееспособность
     */
    var incDoc: Document?

    /**
     * Описание
     */
    var incDesc: String?
}
