package ru.insoft.pf.models

import ru.insoft.libs.models.Dict

class MainRegMan : Dict<String>() {
    var familyId: String? = null
    var men: List<RegMan>? = null
}
