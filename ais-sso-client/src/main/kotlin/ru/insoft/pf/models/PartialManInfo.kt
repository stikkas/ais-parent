package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
 * Частничные данные для создания нового дела
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class PartialManInfo {
    /**
     * Фамилия
     */
    var lastName: String? = null

    /**
     * Имя
     */
    var firstName: String? = null

    /**
     * Отчество
     */
    var middleName: String? = null

    /**
     * Дата рождения
     */
    var birthDate: String? = null

    /**
     * Номер документа
     */
    var docNumber: String? = null

    /**
     * Снилс
     */
    var snils: String? = null
}


