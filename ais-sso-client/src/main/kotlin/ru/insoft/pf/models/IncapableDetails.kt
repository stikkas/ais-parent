package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import ru.insoft.libs.models.Document
import java.time.LocalDate

class IncapableDetails : IncapableDetailsI {
    /**
     * ID записи о недееспособности
     */
    override var disabilityId: String? = null

    /**
     * Дата установки
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    override var incDate: LocalDate? = null

    /**
     *  Документ устанавливающий недееспособность
     */
    override var incDoc: Document? = null

    /**
     * Описание
     */
    override var incDesc: String? = null
}
