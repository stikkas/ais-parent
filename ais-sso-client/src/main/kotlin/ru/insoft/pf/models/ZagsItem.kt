package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDate

/**
 * Данные из таблицы zags_information
 */
class ZagsItem {
    var id: String? = null

    /**
     * Идентификатор пользователя
     * нужен только для проверки при сохранении,
     * и для создания копии сертификата о браке
     */
    @JsonIgnore
    var manId: String = ""

    /**
     * Тип документа (свидетельство о браке или свидетельство о смерти)
     */
    var type: String? = null
    var typeId: String? = null

    /**
     * Дата
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var date: LocalDate? = null

    /**
     * Дата регистрации
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var regDate: LocalDate? = null

    /**
     * Дата выдачи
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var issueDate: LocalDate? = null

    /**
     * Серия
     */
    var series: String? = null

    /**
     * Номер
     */
    var nomer: String? = null

    /**
     * Отдел ЗАГС
     * поле registryofficetext
     */
    var whogive: String? = null

    /**
     * Запись акта гражданского состояния №
     */
    var actNomer: String? = null

    /**
     * Супруг, исплользуется в "Свидетельстве о браке"
     */
    var spouse: String? = null
    var spouseId: String? = null

    /**
     * Код субъекта зарегистрировавшего АГС
     */
    var orgId: String? = null
}
