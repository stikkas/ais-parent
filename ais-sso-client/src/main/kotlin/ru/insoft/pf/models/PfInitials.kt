package ru.insoft.pf.models

import com.fasterxml.jackson.annotation.JsonFormat
import ru.insoft.libs.models.DictValue
import java.time.LocalDate

abstract class PfInitials {
    var id: String? = null

    /**
     * Фамилия
     */
    var lastName: String? = null
        set(value) {
            field = value?.trim()
        }

    /**
     * Имя
     */
    var firstName: String? = null
        set(value) {
            field = value?.trim()
        }

    /**
     * Отчество
     */
    var middleName: String? = null
        set(value) {
            field = value?.trim()
        }

    /**
     * Фамилия в дательном падеже
     */
    var datLastName: String? = null
        set(value) {
            field = value?.trim()
        }

    /**
     * Имя в дательном падеже
     */
    var datFirstName: String? = null
        set(value) {
            field = value?.trim()
        }

    /**
     * Отчество в дательном падеже
     */
    var datMiddleName: String? = null
        set(value) {
            field = value?.trim()
        }

    /**
     * Дата рождения, текстовая, в случае, когда неизвестна точная дата
     */
    var birthDateText: String? = null

    /**
     * Дата смерти
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var deathDate: LocalDate? = null

    /**
     * Актуальна ли запись
     * 1 - актуальна
     */
    var actual: Int? = null

    /**
     * Имеется или нет карточка учета
     * nul - нет
     * UNDERAGE_REG - Учет несовершеннолетних
     * PATRON_REG Учет - Учет нуждающихся в патронаже
     * INCAPABLE_REG - Учет совершеннолетних недееспособных
     */
    var registered: DictValue? = null

    /**
     * Имеется или нет запись в таблице disability
     */
    var disability: Boolean? = null

    /**
     * ID актуальной семьи опеки
     */
    var familyId: String? = null

    /**
     * ID семейного устройства
     */
    var suId: Long? = null

    /**
     * Кол-во полных лет
     */
    var age: Int? = null
}
