package ru.insoft.pf

import mu.KotlinLogging
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import ru.insoft.libs.*
import ru.insoft.libs.controllers.AbstractController
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.models.Document
import ru.insoft.pf.models.*
import ru.insoft.pf.valid.*
import java.time.DateTimeException
import java.time.LocalDate
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong

/**
 * Контроллер для ввода и отображения личных дел
 */
private val logger = KotlinLogging.logger { }

@RestController
@RequestMapping(PfUrls.base)
class PrivateFilesController(
    private val pfs: PrivateFilesService,
    private val manInfoValidator: ManInfoValidator,
    private val personalValidator: PersonalDataValidator,
    private val newDocValidator: NewDocValidator,
    private val iiValidator: InvIncValidator,
    private val addedReqValidator: AddedReqValidator,
    private val zagsValidator: ZagsItemValidator,
    private val sendReqValidator: SendRequestValidator
) : AbstractController() {
    @GetMapping("${PfUrls.registered}/{id}")
    fun isRegistered(@PathVariable id: String, @RequestParam(required = false) type: DictValue?) = pfs.isRegistered(
        id, type ?: DictValue.UNDERAGE_REG
    )

    @GetMapping(PfUrls.rootAddress)
    fun rootAddress() = handle { pfs.rootAddress(currentOrgId()) }

    @GetMapping(PfUrls.mainInfo)
    fun manInfo(@RequestParam(required = false) id: String?, @RequestParam(required = false) chid: Long?) = handle {
        when {
            id != null -> pfs.getManInfo(id)
            chid != null -> {
                store.remove(chid)?.let {
                    ManInfo().apply {
                        lastName = it.lastName
                        firstName = it.firstName
                        middleName = it.middleName
                        snils = it.snils
                        it.docNumber?.let { n ->
                            if (n.isNotEmpty()) {
                                docs = listOf(Document().apply { number = it.docNumber })
                            }
                        }
                        it.birthDate?.let {
                            if (it.isNotEmpty()) {
                                textDatePattern.find(it)?.let { r ->
                                    var day = r.groups["day"]?.value
                                    var month = r.groups["month"]?.value
                                    val year = r.groups["year"]?.value
                                    birthDateText = listOf(day ?: "00", month ?: "00", year ?: "1900").joinToString(".")
                                }
                            }
                        }
                    }
                }
            }
            else -> null
        }
    }

    @GetMapping("/{id}${PfUrls.zagsInfo}")
    fun zagsInfo(@PathVariable id: String) = handle {
        pfs.getZagsInfo(id)
    }

    @GetMapping("/{id}${PfUrls.regsInfo}")
    fun regsInfo(@PathVariable id: String) = handle {
        pfs.getRegsInfo(id)
    }

    @GetMapping("/{id}${PfUrls.otherInfo}")
    fun otherInfo(@PathVariable id: String) = handle {
        pfs.getOtherInfo(id)
    }

    @GetMapping("/{id}${PfUrls.changesInfo}")
    fun changesInfo(@PathVariable id: String) = handle {
        pfs.getChangesInfo(id)
    }

    @GetMapping("/{id}${PfUrls.addedInfo}")
    fun addedInfo(@PathVariable id: String) = handle {
        pfs.getAddedInfo(id)
    }

    @GetMapping("/{id}${PfUrls.smevs}")
    fun smevsInfo(@PathVariable id: String) = handle {
        pfs.getSmevInfo(id)
    }

    @GetMapping("${PfUrls.lastEventNotAdopt}/{id}")
    fun lastEventNotAdopt(@PathVariable id: String) = handle {
        pfs.lastEventNotAdopt(id)
    }

    @GetMapping("${PfUrls.canPrintHistory}/{id}")
    fun canPrintHistory(@PathVariable id: String) = handle {
        pfs.canPrintHistory(id)
    }

    /**
     * Сохраняет частичные данные, на основе которых будет создаваться личное дело
     */
    @PostMapping(PfUrls.saveParams)
    fun saveParams(@RequestBody data: PartialManInfo) = handle {
        val key = id.addAndGet(1)
        store[key] = data
        key
    }

    @PostMapping(PfUrls.mainInfo)
    fun saveManInfo(
        @RequestParam info: ManInfo,
        @RequestParam(required = false) files: List<MultipartFile>?,
        @RequestParam(required = false) force: Boolean?
    ) =
        check(info.apply { birthDate = toDate(birthDateText) }, manInfoValidator) {
            val existSnils = if (!info.snils.isNullOrBlank()) {
                pfs.findExistSnils(info.id, info.snils!!)
            } else null
            if (existSnils?.isNotEmpty() == true) {
                checkCopies(existSnils, info) { f ->
                    "Введенный СНИЛС существует в базе данных " + f.mapNotNull { it.orgName }.joinToString()
                }
            } else {
                // Левая часть file:///project_root_dir/search_exist_ld.jpg [Личное дело не найдено] ------------------------
                info.docs?.firstOrNull()?.let { d ->
                    d.typeId?.let { t ->
                        pfs.findExistDoc(info.id, t, d.series, d.number)?.let { c ->
                            if (c.isNotEmpty()) {
                                checkCopies(c, info) { f ->
                                    "Введенный документ существует для " +
                                            f.groupBy {
                                                listOfNotNull(
                                                    it.lastName,
                                                    it.firstName,
                                                    it.middleName
                                                ).joinToString(" ")
                                            }.map { (k, v) ->
                                                k + " в базе данных " + v.mapNotNull { it.orgName }.joinToString()
                                            }.joinToString()
                                }
                            } else null
                        }
                    }
                } ?: if (force == true) {
                    null
                } else {
                    pfs.findTwins(
                        // [Не заполнен документ, удостоверяющий личность] или [Такие люди не найдены для заданного документа]
                        info.id, convertNameToCompare(info.lastName), convertNameToCompare(info.firstName),
                        info.birthDate, info.birthAddress?.levels?.lastOrNull()?.value, info.birthAddress
                    )?.let { t -> t.ifEmpty { null } }
                } ?: pfs.saveManInfo(info, arrangeFiles(info.docs, files))
            }
        }


    @PostMapping(PfUrls.person)
    fun savePersonalData(@RequestParam info: ManInfo, @RequestParam(required = false) files: List<MultipartFile>?) =
        check(info.apply { birthDate = toDate(birthDateText) }, personalValidator) {
            pfs.savePersonalData(info, arrangeFiles(info.docs, files))
        }

    @PostMapping(PfUrls.document)
    fun saveNewDoc(@RequestParam info: ManInfo, @RequestParam(required = false) files: List<MultipartFile>?) =
        check(info, newDocValidator) {
            pfs.saveNewDoc(info, arrangeFiles(info.docs, files))
        }

    @PostMapping(PfUrls.addedInfo)
    fun saveAddReq(@RequestBody info: AddedRequisites) = check(info, addedReqValidator) {
        pfs.saveAddedReq(info)
    }

    @PostMapping(PfUrls.smevs)
    fun savePleaSmevs(@RequestBody info: SmevsInfo) = handle {
        pfs.saveSmevs(info)
    }

    @PostMapping("/{id}${PfUrls.smevs}")
    fun sendRequest(@RequestBody req: SmevRequest, @PathVariable id: String) = check(req, sendReqValidator) {
        pfs.sendRequest(req, id)
    }

    @PostMapping("/{id}${PfUrls.zagsInfo}")
    fun saveZagsItem(@PathVariable id: String, @RequestBody item: ZagsItem) =
        check(item.apply { manId = id }, zagsValidator) {
            pfs.saveZagsItem(item, id)
        }

    @PostMapping(PfUrls.regsInfo)
    fun saveRegs(@RequestBody regs: List<ActualRegistration>) = handle {
        pfs.saveRegInfo(regs)
    }

    @PostMapping("/{id}${PfUrls.otherInfo}")
    fun saveOther(@PathVariable id: String, @RequestBody item: InvIncItem) = check(item, iiValidator) {
        pfs.saveOther(item, id)
    }

    @DeleteMapping("/{id}${PfUrls.zagsInfo}")
    fun removeZags(@PathVariable id: String) = handle {
        pfs.removeZags(id)
    }

    @DeleteMapping("/{id}${PfUrls.otherInfo}")
    fun removeOther(@PathVariable id: String, @RequestParam(required = false) dis: Boolean?) = handle {
        pfs.removeOther(id, dis)
    }

    @DeleteMapping("/{id}")
    fun remove(@PathVariable id: String) = handle {
        pfs.remove(id)
    }

    fun toDate(value: String?): LocalDate? {
        val date = value ?: return null
        return textDatePattern.find(date)?.let {
            var day = it.groups["day"]?.value
            var month = it.groups["month"]?.value
            val year = it.groups["year"]?.value
            if (day != null && month == null && year != null) {
                month = day
                day = null
            }
            try {
                LocalDate.of(year.toYear(), month.toMonth(), day.toDay())
            } catch (ex: DateTimeException) {
                logger.error(ex) { "Wrong date" }
                null
            }
        }
    }

    private fun equalNames(name1: String?, name2: String?) = convertNameToCompare(name1) == convertNameToCompare(name2)
    private fun convertNameToCompare(name: String?) = name?.trim()?.toLowerCase()?.replace("ё", "е")

    /**
     * Проверка алгоритма file:///project_root_dir/search_exist_ld.jpg
     * [Личное дело найдено] и [Заполнен док. удостоверающий личность -> Такие люди найдены]
     */
    private fun checkCopies(copies: List<SnilsCopy>, info: ManInfo, msg: (found: List<SnilsCopy>) -> String) =
        copies.filter { s ->
            !(equalNames(s.lastName, info.lastName) && equalNames(s.firstName, info.firstName)
                    && equalNames(s.middleName, info.middleName) && s.birthDate == info.birthDate)
        }.let {
            if (it.isEmpty()) {
                null
            } else {
                val uniq = mutableListOf<SnilsCopy>()
                it.forEach { c ->
                    if (!uniq.any {
                            c.lastName == it.lastName && c.firstName == it.firstName
                                    && c.middleName == it.middleName && c.orgId == it.orgId
                        }) {
                        uniq.add(c)
                    }
                }
                msg(uniq)
            }
        } ?: run {
            // Все данные совпадают
            val manId = copies.first().id
            pfs.createPf(manId)
            pfs.getManInfo(manId)
        }

    companion object {
        private val store = ConcurrentHashMap<Long, PartialManInfo>()
        private val id = AtomicLong()
    }
}

