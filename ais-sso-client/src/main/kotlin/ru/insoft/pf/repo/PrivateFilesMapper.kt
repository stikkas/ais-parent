package ru.insoft.pf.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.libs.models.Address
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.models.Document
import ru.insoft.libs.smevs.birth.BirthRequestDetails
import ru.insoft.libs.smevs.death.DeathRequestDetails
import ru.insoft.libs.smevs.divorce.RequestDetails
import ru.insoft.libs.smevs.egrn.AplEgrnRequest
import ru.insoft.libs.smevs.marry.ReqDetails
import ru.insoft.libs.smevs.pension.PensRequest
import ru.insoft.libs.smevs.socpay.SocPayRequest
import ru.insoft.models.Official
import ru.insoft.pf.models.*
import java.time.LocalDate

@Mapper
interface PrivateFilesMapper {
    fun manInfo(@Param("id") manId: String, @Param("orgId") orgId: String, @Param("aisId") aisId: Int): ManInfo?
    fun manSnilsInfo(@Param("id") manId: String): ManInfo?
    fun zagsInfo(@Param("id") manId: String, @Param("orgId") orgId: String): ZagsInfo?
    fun regsInfo(@Param("id") manId: String, @Param("orgId") orgId: String): RegisteredInfo?
    fun otherInfo(@Param("id") manId: String, @Param("orgId") orgId: String): InvalidIncapable?
    fun dwellingPlaces(@Param("manId") manId: String, @Param("aisId") aisId: Int? = null): List<DwellingPlace?>?
    fun addedRequisites(@Param("id") manId: String, @Param("orgId") orgId: String): AddedRequisites?
    fun changeInfo(@Param("id") manId: String, @Param("orgId") orgId: String): ChangeInfo?
    fun findDocsByManId(id: String): List<Document>?
    fun findExistSnils(@Param("manId") id: String?, @Param("snils") snils: String): List<SnilsCopy>?
    fun findExistDocs(
        @Param("manId") id: String?,
        @Param("typeId") typeId: String,
        @Param("series") series: String?,
        @Param("nomer") nomer: String?
    ): List<SnilsCopy>?

    fun findTwins(
        @Param("manId") manId: String?, @Param("lastName") lastName: String?, @Param("firstName") firstName: String?,
        @Param("birthDate") birthDate: LocalDate?, @Param("addressId") levelId: String?,
        @Param("address") birthAddress: Address?, @Param("orgId") orgId: String
    ): List<SnilsCopy>?

    fun findExistPasport(
        @Param("manId") id: String?,
        @Param("series") series: String,
        @Param("nomer") nomer: String,
        @Param("typeId") typeId: String
    ): Official?

    fun findTrainings(id: String): List<Training>
    fun findDisabilityByIdOrManId(
        @Param("manId") manId: String?,
        @Param("id") id: String? = null
    ): List<DisabilityRemoved>?

    fun isRegisteredBddsop(@Param("id") id: String): Boolean
    fun isRegistered(
        @Param("id") id: String, @Param("regType") regType: DictValue,
        @Param("orgId") orgId: String
    ): Boolean

    fun insertManInfo(@Param("info") info: ManInfo, @Param("userId") userId: String)
    fun insertOwnerPf(@Param("systemManId") systemManId: String, @Param("orgId") orgId: String)
    fun updateManInfo(info: ManInfo)
    fun remove(id: String): String?
    fun removeManDetails(id: String)
    fun insertDetails(info: ManInfo)
    fun insertDocument(@Param("doc") doc: Document, @Param("id") systemmanId: String)
    fun insertDwellingPlace(@Param("place") place: DwellingPlace, @Param("flat") flat: String? = null): String
    fun insertPlaceWithAdr(
        @Param("place") place: DwellingPlace,
        @Param("aid") addressId: String,
        @Param("flat") flat: String? = null
    ): String

    fun insertRelationship(
        @Param("place") place: DwellingPlace,
        @Param("mid") mid: String,
        @Param("oid") oid: String,
        @Param("masterId") masterId: String?
    ): String

    /**
     * @param newOrFirst когда null - никакие поля не дублируем,
     * когда true -  дублируем поле даты
     * когда false - дублиреум поля даты и адреса
     */
    fun insertRegistration(
        @Param("dp") it: DwellingPlace, @Param("rid") relationId: String,
        @Param("leaveDate") leaveDate: LocalDate?,
        @Param("newOrFirst") newOrFirst: Boolean? = null,
        @Param("aid") addressId: String? = null
    )

    fun updateDocument(doc: Document)
    fun removeDocs(@Param("id") systemmanId: String, @Param("excluded") docsIds: ArrayList<String>)
    fun disactualOldDoc(@Param("docId") id: String, @Param("typeId") typeId: String, @Param("manId") manId: String)
    fun updateDwellingPlace(
        @Param("place") place: DwellingPlace,
        @Param("flat") flat: String?,
        @Param("id") id: String? = null
    )

    /**
     * Возвращает список пар systemobjectd -> masterrelationid
     */
    fun findExistPlaces(id: String): List<Pair<String, String>>
    fun updateRelationship(
        @Param("place") place: DwellingPlace,
        @Param("id") id: String,
        @Param("oid") sysObjId: String
    )

    fun findMainRelationId(
        @Param("mid") systemmanId: String, @Param("oids") sysObjIds: List<String>,
        @Param("atype") adressType: DictValue?
    ): String?

    fun updateRegistration(
        @Param("place") place: DwellingPlace, @Param("relId") relationId: String,
        @Param("leaveDate") leaveDate: LocalDate?
    )

    fun findRootAddress(orgId: String): DwellingPlace?

    fun setDeathDate(@Param("id") id: String, @Param("date") date: LocalDate)
    fun insertZags(@Param("it") item: ZagsItem, @Param("id") id: String): String
    fun updateZags(item: ZagsItem)
    fun selectCertDate(id: String): LocalDate?
    fun removeZags(id: String)
    fun manIdOfDeathCert(id: String): String?
    fun hasAppeal(id: String): Boolean

    /**
     * получить информацию для паспорта
     */
    fun passInfo(manId: String): Document?
    fun minAcutalDocDate(id: String): LocalDate?
    fun existsActualDocDate(@Param("manId") id: String, @Param("typeId") typeId: String): LocalDate?
    fun birthDate(manId: String): LocalDate?

    fun insertInvalidity(@Param("inv") inv: InvalidDetails, @Param("id") id: String)
    fun removeDisablility(@Param("ids") ids: List<String>)
    fun removeInvalidity(id: String)

    fun updateSocStatus(s: SocialStatus)
    fun insertSocStatus(@Param("status") status: SocialStatus, @Param("id") manId: String)
    fun removeSocStatuses(@Param("id") manId: String, @Param("excluded") excluded: List<Long>?)

    fun updateEducation(e: Education)
    fun insertEducation(@Param("edu") education: Education, @Param("id") manId: String)
    fun removeEducs(@Param("id") manId: String, @Param("excluded") excluded: List<Long>?)

    fun updateTraining(t: Training)
    fun insertTraining(@Param("train") training: Training, @Param("id") manId: String)
    fun removeTrains(@Param("id") manId: String, @Param("excluded") excluded: List<String>?)

    fun updateJob(job: JobInfo)
    fun insertJob(@Param("job") job: JobInfo, @Param("id") manId: String)
    fun removeJobs(@Param("id") manId: String, @Param("excluded") excluded: List<String>?)
    fun insertPersonalData(@Param("info") info: ManInfo, @Param("userId") userId: String)

    /**
     * Добавляет запись о браке к супругу, если этой записи еще нет
     * @param id идентификатор записи о браке текущего человека
     * @param manId идентификатор человека, чья запись
     * @param spouseId идентификатор человека, которому нужно добавить (обновить) запись о браке
     */
    fun saveCertToSpouse(
        @Param("itemId") id: String?,
        @Param("manId") manId: String,
        @Param("spouseId") spouseId: String,
        @Param("oldDate") oldCertDate: LocalDate?
    )

    fun manMarryInfo(manId: String): ReqDetails?
    fun manDivorceInfo(manId: String): RequestDetails?
    fun manBirthInfo(manId: String): BirthRequestDetails?
    fun manDeathInfo(manId: String): DeathRequestDetails?
    fun manPensionReq(manId: String): PensRequest?
    fun manSocPayReq(manId: String): SocPayRequest?
    fun manAplEgrnReq(@Param("manId") manId: String, @Param("orgId") orgId: String): AplEgrnRequest?
    fun lastEventType(id: String): String?

    /**
     * Возвращает идентификатор если в таблице dwellingfund есть запись с заданными параметрами
     */
    fun findExistDwelling(
        @Param("flat") flat: String?,
        @Param("room") room: String?,
        @Param("aid") aid: String
    ): String?

    fun findDwelling(
        @Param("id") id: String,
        @Param("flat") flat: String
    ): String?

    fun removePlaces(@Param("ids") ids: List<String>)
    fun removeOldPlaces(@Param("manId") manId: String, @Param("excluded") ids: List<DwellingPlace>?)
    fun removeRelations(id: String)
    fun hasSameRegAddress(
        @Param("manId") systemmanId: String,
        @Param("adrId") addressId: String,
        @Param("startDate") startDate: LocalDate,
        @Param("endDate") endDate: LocalDate?
    ): Boolean

    fun hasActualRegWithAddress(
        @Param("manId") manId: String,
        @Param("flats") flats: List<String>?,
        @Param("adrId") addressId: String,
        @Param("flat") flat: String?,
        @Param("adrText") addressText: String?
    ): Boolean

    fun removeRelatives(@Param("id") mainManId: String, @Param("ids") ids: List<String>?)
    fun updateRelative(@Param("rel") rel: Relation, @Param("startDate") startDate: LocalDate?)
    fun insertRelative(
        @Param("rel") rel: Relation,
        @Param("id") mainManId: String,
        @Param("startDate") startDate: LocalDate?,
        @Param("userId") userId: String?
    )

    fun saveOwnership(
        @Param("manId") systemManId: String,
        @Param("pId") id: String?,
        @Param("date") date: LocalDate,
        @Param("num") num: Int?,
        @Param("denum") denum: Int?
    )

    fun removeOwnership(@Param("manId") systemManId: String, @Param("pId") id: String?)

    fun findRelatives(systemManId: String): List<Relation>?
    fun hasNotRegInCurOrg(@Param("manId") systemManId: String, @Param("orgId") currentOrgId: String): Boolean
    fun findSameRegistration(
        @Param("id") manId: String,
        @Param("regTypeId") regType: DictValue?,
        @Param("date") startDate: LocalDate?
    ): DwellingPlace?

    fun findSameAddress(
        @Param("pid") placeId: String,
        @Param("manId") mainManId: String,
        @Param("regTypeId") regType: DictValue?
    ): String?

    fun setEventAddress(@Param("placeId") placeId: String, @Param("regId") regId: String)
    fun setLeaveForRegistration(
        @Param("regId") regId: String, @Param("date") date: LocalDate?,
        @Param("adrId") addressId: String?
    )

    fun hasOpenLivePlaceRegistration(@Param("manId") manId: String, @Param("orgId") currentOrgId: String): Boolean
    fun setEndDates(@Param("manId") id: String, @Param("date") date: LocalDate?)
    fun updateEndDates(@Param("manId") id: String, @Param("date") date: LocalDate?)
    fun createFamily(
        @Param("date") startDate: LocalDate?,
        @Param("manId") id: String?,
        @Param("userId") userId: String?
    ): String?

    fun createFamilyLinks(@Param("fid") familyId: String?, @Param("sid") systemobjectId: String?)
    fun updateRelativeId(@Param("mId") memId: String, @Param("rId") relativeId: String?)
    fun insertMember(
        @Param("fId") familyId: String,
        @Param("manId") manId: String?,
        @Param("mmanId") mainManId: String?,
        @Param("startDate") startDate: LocalDate?,
        @Param("endDate") endDate: LocalDate?,
        @Param("rId") relativeId: String?,
        @Param("userId") userId: String?,
        @Param("sId") systemobjectId: String?
    ): String?

    fun removeRegRelatives(@Param("fId") familyId: String, @Param("excluded") ids: List<String>?)
    fun removeFamilies(@Param("sId") systemobjectId: String, @Param("excluded") familyIds: List<String>?)
}
