package ru.insoft.pf.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.pf.models.SmevRequest
import ru.insoft.pf.models.SmevsInfo

@Mapper
interface RequestsMapper {
    fun smevsInfo(@Param("id") manId: String, @Param("orgId") orgId: String): SmevsInfo?

    fun insertSentRequest(
        @Param("req") req: SmevRequest, @Param("manId") manId: String,
        @Param("orgId") orgId: String, @Param("userId") userId: String
    )

    fun updateSentRequest(req: SmevRequest)

    fun removeSmevs(@Param("manId") id: String, @Param("excluded") ids: List<Long>?)
    fun insertSmevRequest(
        @Param("req") req: SmevRequest, @Param("manId") id: String, @Param("orgId") orgId: String,
        @Param("userId") userId: String
    )
}
