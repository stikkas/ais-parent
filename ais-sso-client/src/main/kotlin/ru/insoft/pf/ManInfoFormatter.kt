package ru.insoft.pf

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.format.Formatter
import ru.insoft.pf.models.ManInfo
import java.util.*


open class ManInfoFormatter(private val om: ObjectMapper) : Formatter<ManInfo> {

    override fun print(obj: ManInfo, locale: Locale): String = obj.toString()

    override fun parse(json: String, locale: Locale) = om.readValue(json, ManInfo::class.java)
}

