package ru.insoft.filters

import ru.insoft.libs.filters.AbstractFilter
import ru.insoft.libs.models.DictValue
import java.time.LocalDate

/**
 * Критерии поиска по личным делам
 */
class LdFilter : AbstractFilter() {

    /**
     * Дата рождения 'с'
     */
    var birthDate: LocalDate? = null
        get() = if (child == true && field != null) { // для детей корректируем минимальную дату рождения
            minBirthDate = minBirthDate ?: LocalDate.now().minusYears(18).plusDays(1)
            if (field?.isBefore(minBirthDate) == true) {
                LocalDate.MIN
            } else {
                field
            }
        } else if (child == false && field != null) {
            maxBirthDate = maxBirthDate ?: LocalDate.now().minusYears(18)
            if (field?.isAfter(maxBirthDate) == true) {
                LocalDate.MIN
            } else {
                field
            }
        } else {
            field
        }
        set(value) {
            field = if (value == LocalDate.MIN) null else value
        }

    /**
     * Фамилия
     */
    var lastName: String? = null
        set(value) {
            field = decodeAndTrim(value)?.toLowerCase()
        }

    /**
     * Имя
     */
    var firstName: String? = null
        set(value) {
            field = decodeAndTrim(value)?.toLowerCase()
        }

    /**
     * Отчество
     */
    var middleName: String? = null
        set(value) {
            field = decodeAndTrim(value)?.toLowerCase()
        }

    /**
     * Номер удостоверяющего документа
     */
    var docNumber: String? = null
        set(value) {
            field = decodeAndTrim(value)?.toLowerCase()
        }

    /**
     * Родственные отношения
     */
    var relativeId: DictValue? = null
        set(value) = when (value) {
            DictValue.FATHER, DictValue.HUSBAND, DictValue.UNCLE, DictValue.GRANDPA, DictValue.BROTHER -> sex = 0
            DictValue.MOTHER, DictValue.WIFE, DictValue.AUNT, DictValue.GRANDMA, DictValue.SISTER -> sex = 1
            else -> {
            }
        }

    /**
     * Используется когда родственное отношение выражено id а не константой
     */
    var relationId: String? = null
        set(value) = value?.let {
            relativeId = DictValue.toDictValue(it)
        } ?: Unit

    /**
     * Пол проставляем в зависимости от родственных отношений
     * Иногда получаем в запросе
     * Должно использоваться либо то либо это
     */
    var sex: Int? = null

    /**
     * Ищем детей если - true, взрослых если - false, всех если - null
     */
    var child: Boolean? = null

    /**
     * СНИЛС
     */
    var snils: String? = null
        set(value) {
            field = decodeAndTrim(value)?.toLowerCase()
        }

    /**
     * Ищем только актуальных
     */
    var actual: Boolean? = null

    // Для Опеки
    /**
     * Тип учета (используется в Обращении)
     */
    var registered: DictValue? = null

    /**
     * Не стоит на учете
     */
    var noreg = false

    /**
     * Поиск из обращения при подборе
     */
    var plea: Boolean? = null

    /**
     * Поиск опекунов для опекаемого
     */
    var caredId: String? = null

    /**
     *  Исключить недееспособных
     */
    var capable: Boolean? = null

    // Для БДДСОП
    /**
     * Ищем только среди правонарушетелей
     */
    var breaker: Boolean? = null


    private var minBirthDate: LocalDate? = null
    private var maxBirthDate: LocalDate? = null
}

