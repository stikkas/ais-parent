package ru.insoft.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.filters.LdFilter
import ru.insoft.models.LdTableRow

@Mapper
interface ShareSearchMapper {
    fun findAllLd(
        @Param("filter") filter: LdFilter, @Param("offset") offset: Long, @Param("size") size: Int,
        @Param("orgId") orgId: String, @Param("aisId") aisId: Int?
    ): List<LdTableRow>

    fun countLd(@Param("filter") filter: LdFilter, @Param("orgId") orgId: String, @Param("aisId") aisId: Int?): Long
    fun findFioById(id: String): String?

    fun findMyChildrenOrgs(orgId: String): List<String>
    fun isMyAddress(@Param("addressId") addressId: String, @Param("orgId") orgId: String): Boolean
}
