package ru.insoft.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.libs.models.Address
import ru.insoft.libs.models.DictValue
import ru.insoft.models.EgissoRegistryRec
import ru.insoft.pf.models.IncapableDetailsI
import ru.insoft.pf.models.InvalidDetails
import ru.insoft.pf.models.LastPf
import java.time.LocalDate

@Mapper
interface UtilsMapper {
    /**
     * Находит по id данные для отображения последних 10-и
     */
    fun findPfByIds(@Param("ids") ids: List<String>): List<LastPf>

    /**
     * Находит последнию запись по дате в таблице disability для человека
     * и возвращает тип документа (идентификатор классификатора)
     */
    fun findLastDisability(@Param("manId") systemmanId: String?, @Param("date") limitDate: LocalDate? = null): String?

    /**
     * Обновление инвалидности
     */
    fun updateInvalidity(info: InvalidDetails)

    /**
     * Добавление новой недееспособности
     */
    fun insertDisability(@Param("inc") inc: IncapableDetailsI, @Param("id") id: String)
    fun insertEgisso(
        @Param("reestrId") reestrId: String,
        @Param("eventId") eventId: String,
        @Param("orgId") orgId: String?,
        @Param("date") date: LocalDate?,
        @Param("docId") docId: String?,
        @Param("subjId") subjectId: String,
        @Param("linkId") linkId: String? = null
    )

    /**
     * Обновление недееспособности
     */
    fun updateDisability(info: IncapableDetailsI)

    /**
     * Возвращает адрес рождения для заданного человека
     */
    fun getBirthAddress(id: String): Address?

    /**
     * Возвращает адрес регистрации
     * @param systemManId идентификатор человека
     * @param type тип адреса (фактический или регистрации)
     */
    fun getRegAddress(@Param("id") systemManId: String, @Param("type") type: DictValue): Address?
    fun findEgissoRegistryByDocId(
        @Param("docId") docId: String,
        @Param("events") events: List<String>? = null
    ): List<EgissoRegistryRec>

    /**
     * Ищет записи в egisso_registry, удаляет те, у которых sent_date is null и возвращает те, у которых
     * sent_date is not null
     * При этом поле sentDate объектов будет null (оно не используется вызывающей стороной)
     */
    fun findAndRemoveEgissoRegistry(
        @Param("subjId") subjId: String, @Param("docId") docId: String, @Param("events") events: List<String>? = null,
        @Param("repId") manId: String? = null
    ): List<EgissoRegistryRec>?

    fun removeEgisso(@Param("ids") ids: List<String>)
}
