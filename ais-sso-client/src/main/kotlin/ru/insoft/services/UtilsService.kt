package ru.insoft.services

import ru.insoft.libs.models.DictValue
import ru.insoft.libs.models.Document
import ru.insoft.models.EgissoRegistryRec
import ru.insoft.pf.models.IncapableDetailsI
import ru.insoft.pf.models.InvalidDetails
import ru.insoft.pf.models.LastPf

interface UtilsService {

    fun saveDoc(doc: Document?, removeOwner: () -> Unit): Document?
    fun findPfByIds(ids: List<String>): List<LastPf>
    fun updateInvalidity(info: InvalidDetails)
    fun saveDisability(info: IncapableDetailsI, systemManId: String)
    fun getBirthAddress(systemManId: String): String?
    fun getRegAddress(systemManId: String, type: DictValue): String?
    fun findEgissoRegistrys(docId: String, events: List<String>? = null): List<EgissoRegistryRec>
    fun removeEgisso(recs: List<EgissoRegistryRec>)
    fun findAndRemoveEgissoRegistry(
        subjId: String, docId: String, events: List<String>? = null, manId: String? = null
    ): List<EgissoRegistryRec>?
}
