package ru.insoft.services

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.libs.currentOrgId
import ru.insoft.libs.formatAddressToString
import ru.insoft.libs.models.Classifiers.acceptDisabled
import ru.insoft.libs.models.Classifiers.acceptPartDisabled
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.models.Document
import ru.insoft.libs.repo.CommonMapper
import ru.insoft.models.EgissoRegistryRec
import ru.insoft.pf.models.IncapableDetailsI
import ru.insoft.pf.models.InvalidDetails
import ru.insoft.repo.UtilsMapper


const val acceptNoDisabled = "e6bdb614-2b29-11ea-a399-00155d00050e" // Решение суда о признании дееспособным
const val limitActivity = "a28c8016-e620-11eb-acb9-00155d02221d" // Ограничение дееспособности гражданина
const val changedActivityReestr = "1ffe7d82-e622-11eb-bb25-00155d02221d" // Реестр лиц с измененной дееспособностью
const val priznanieDisabled = "87e0cef2-e620-11eb-acb9-00155d02221d" // Признание гражданина недееспособным
const val revokeDisabled = "956c30e8-e620-11eb-acb9-00155d02221d" // Отмена признания гражданина недееспособным
const val revokeLimitActivity = "b2383316-e620-11eb-acb9-00155d02221d" // Отмена ограничения дееспособности гражданина
const val removeEvent = "055bdc74-e620-11eb-8435-00155d02221d" // Событие "Удаление"

@Transactional
@Service
class UtilsServiceImpl(private val cm: CommonMapper, private val um: UtilsMapper) : UtilsService {

    override fun saveDoc(doc: Document?, removeOwner: () -> Unit) = doc?.let { d ->
        // Если документ пустой но с id то нужно его удалить, но перед этим нужно его вычистить из владельца
        if (d.isEmpty()) {
            d.id?.let {
                removeOwner()
                cm.removeOtherDoc(it)
            }
            null
        } else {
            d.id?.let { cm.updateOtherDoc(d) } ?: cm.insertOtherDoc(d)
            d
        }
    }

    @Transactional(readOnly = true)
    override fun findPfByIds(ids: List<String>) = um.findPfByIds(ids)

    override fun updateInvalidity(info: InvalidDetails) = um.updateInvalidity(info)

    override fun saveDisability(info: IncapableDetailsI, systemManId: String) {
        var updated = false
        saveDoc(info.incDoc) {
            info.incDoc = null
            um.updateDisability(info)
            updated = true
        }
        info.disabilityId?.let {
            if (!updated) {
                um.updateDisability(info)
            }
        } ?: run {
            info.incDoc?.typeId?.let { type ->
                when {
                    type == acceptDisabled -> priznanieDisabled
                    type == acceptNoDisabled
                            && um.findLastDisability(systemManId) == acceptDisabled -> revokeDisabled
                    type == acceptPartDisabled -> limitActivity
                    type == acceptNoDisabled
                            && um.findLastDisability(systemManId) == acceptPartDisabled -> revokeLimitActivity
                    else -> null
                }?.let { eid ->
                    um.insertEgisso(
                        changedActivityReestr,
                        eid,
                        currentOrgId(),
                        info.incDate,
                        info.incDoc?.id,
                        systemManId
                    )
                }
            }
            um.insertDisability(info, systemManId)
        }
    }

    @Transactional(readOnly = true)
    override fun getBirthAddress(systemManId: String) = formatAddressToString(um.getBirthAddress(systemManId))

    @Transactional(readOnly = true)
    override fun getRegAddress(systemManId: String, type: DictValue) =
        formatAddressToString(um.getRegAddress(systemManId, type))

    override fun findEgissoRegistrys(docId: String, events: List<String>?) = um.findEgissoRegistryByDocId(docId, events)

    override fun removeEgisso(recs: List<EgissoRegistryRec>) = um.removeEgisso(recs.map { it.id })
    override fun findAndRemoveEgissoRegistry(
        subjId: String,
        docId: String,
        events: List<String>?,
        manId: String?
    ): List<EgissoRegistryRec>? = um.findAndRemoveEgissoRegistry(subjId, docId, events, manId)
}
