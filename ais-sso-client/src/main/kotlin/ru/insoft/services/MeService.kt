package ru.insoft.services

import kotlinx.coroutines.Job
import ru.insoft.pf.models.LastPf

interface MeService {
    fun lastTens(): List<LastPf>
    fun addLast(id: String, userId: String): Job
    fun remove(id: String, userId: String): Job
}
