package ru.insoft.services

import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Value
import ru.insoft.filters.LdFilter
import ru.insoft.libs.currentOrgId
import ru.insoft.libs.models.DictValue
import ru.insoft.libs.models.Page
import ru.insoft.libs.offset
import ru.insoft.repo.ShareSearchMapper

abstract class SearchServiceImpl(private val ssm: ShareSearchMapper, private val us: UtilsService) : SearchService {
    @Value("\${app.ais-id}")
    var aisId: Int? = null

    override fun findAllLd(filter: LdFilter, page: Long, size: Int) =
        findAll(size, page, { ssm.countLd(filter, it, aisId) }) { f, o ->
            ssm.findAllLd(filter, f, size, o, aisId).also {
                coroutineScope {
                    it.forEach { r ->
                        val id = r.id!!
                        launch { r.factAdr = us.getRegAddress(id, DictValue.FACT_ADDR) }
                        launch { r.regAdr = us.getRegAddress(id, DictValue.REG_ADDR) }
                    }
                }
            }
        }

    override fun findFio(id: String) = ssm.findFioById(id)

    override fun findMyChildrenOrgs(orgId: String) = ssm.findMyChildrenOrgs(orgId)
    override fun isMyAddress(addressId: String) = ssm.isMyAddress(addressId, currentOrgId())

    protected fun <T> findAll(
        size: Int, page: Long, countBlock: (orgId: String) -> Long,
        dataBlock: suspend (offset: Long, orgId: String) -> List<T>
    ) = runBlocking {
        val orgId = currentOrgId()
        val data = async { dataBlock(page.offset(size), orgId) }
        val count = async { countBlock(orgId) }
        Page(data.await(), size, count.await(), page)
    }
}
