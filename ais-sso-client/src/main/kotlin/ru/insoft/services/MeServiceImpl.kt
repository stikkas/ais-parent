package ru.insoft.services

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.libs.currentUser
import ru.insoft.pf.models.LastPf
import ru.insoft.repo.UtilsMapper
import java.util.concurrent.ConcurrentHashMap

@Service
class MeServiceImpl(private val um: UtilsMapper) : CoroutineScope by CoroutineScope(Dispatchers.Default), MeService {

    @Transactional(readOnly = true)
    override fun lastTens(): List<LastPf> {
        return store[currentUser()?.id!!]?.let { i ->
            if (i.isNotEmpty()) {
                val ids = i.toTypedArray()
                ids.reverse()
                val res = Array<LastPf?>(ids.size) { null }
                um.findPfByIds(i).forEach { l ->
                    res[ids.indexOf(l.id)] = l
                }
                res.filterNotNull()
            } else {
                emptyList()
            }
        } ?: emptyList()
    }

    override fun addLast(id: String, userId: String) = launch {
        val data = store.getOrPut(userId) { ArrayList<String>(10) }
        synchronized(data) {
            if (!data.contains(id)) {
                if (data.size > 9) {
                    val last = data.subList(1, data.size)
                    data.clear()
                    data.addAll(last)
                }
                data.add(id)
            }
        }
    }

    override fun remove(id: String, userId: String) = launch {
        store[userId]?.let {
            synchronized(it) {
                it.remove(id)
            }
        }
    }

    companion object {
        private val store = ConcurrentHashMap<String, MutableList<String>>()
    }
}

