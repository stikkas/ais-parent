package ru.insoft.services

import ru.insoft.filters.LdFilter
import ru.insoft.libs.models.Page
import ru.insoft.models.LdTableRow

interface SearchService {
    fun findAllLd(filter: LdFilter, page: Long, size: Int): Page<LdTableRow>
    fun findFio(id: String): String?
    fun findMyChildrenOrgs(orgId: String): List<String>
    fun isMyAddress(addressId: String): Boolean
}
