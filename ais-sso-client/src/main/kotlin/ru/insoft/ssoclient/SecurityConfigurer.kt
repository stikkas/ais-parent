package ru.insoft.ssoclient

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.servlet.ServletContextInitializer
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import ru.insoft.libs.models.User
import java.net.URLEncoder
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSessionEvent
import javax.servlet.http.HttpSessionListener

@EnableWebSecurity
class SecurityConfigurer : WebSecurityConfigurerAdapter() {
    @Value("\${app.sso.cookie-name:ssoauth}")
    private lateinit var cookieName: String

    @Value("\${server.port}")
    private lateinit var localPort: String

    @Value("\${app.root-url}")
    private lateinit var myurl: String

    @Value("\${app.sso.auth-url}")
    private lateinit var authUrl: String

    @Value("\${app.sso.public-files:/noXRAfaye.txt}")
    private lateinit var publicFiles: Array<String>

    // Фейковы проверяльщик пароля, т.к. всю информацию мы получаем с сервера авторизации,
    // который уже сделал всю необходимую проверку
    @Bean
    fun passwordEncoder() = object : PasswordEncoder {
        override fun encode(rawPassword: CharSequence?) = rawPassword?.toString()
        override fun matches(rawPassword: CharSequence?, encodedPassword: String?) = true
    }

    /**
     * Устанавливаем для каждого приложения свое название для session cookie
     * и время 4 часа, на сервере авторизации установим время 8 часов,
     * чтобы когда здесь кука протухает, то она должна пойти на сервер авторизации
     * там залогинится (автологин), обновить время куки сервера авторизации и время
     * коки разлогинивания
     */
    @Bean
    fun servletContextInitializer() = ServletContextInitializer { servletContext ->
        val fourHour = 4 * 60 * 60
        servletContext.sessionCookieConfig.name = "SOFTS$localPort"
        servletContext.addListener(object : HttpSessionListener {
            override fun sessionCreated(se: HttpSessionEvent?) {
                se?.session?.maxInactiveInterval = fourHour
            }
        })
    }

    @Bean
    fun authManager() = authenticationManagerBean()

    override fun configure(web: WebSecurity) {
        web.ignoring().antMatchers(*publicFiles)
    }

    override fun configure(http: HttpSecurity) {
        http.authorizeRequests()
            // Используется GET т.к. нужно обязательно редиректить.
            .antMatchers(HttpMethod.GET, "/sso/login").permitAll()
            .anyRequest().authenticated()
            .and().cors().disable().csrf().disable()
            .httpBasic()
            .authenticationEntryPoint(object : LoginUrlAuthenticationEntryPoint("$authUrl?callback=$myurl/sso/login") {
                override fun determineUrlToUseForThisRequest(
                    request: HttpServletRequest,
                    response: HttpServletResponse,
                    exception: AuthenticationException
                ): String {
                    var next = request.requestURI
                    if (!request.queryString.isNullOrBlank()) {
                        next += "?${request.queryString}"
                    }
                    return "$loginFormUrl&next=${URLEncoder.encode(next, "UTF-8")}"
                }
            }).and().userDetailsService {
                User().apply {
                    val (id, login, orgId, roleId) = it.split("|")
                    this.id = id
                    this.login = login
                    this.orgId = orgId
                    this.roleId = roleId
                    pass = "xxx"
                }
            }
            .addFilterAfter({ request, response, chain ->
                if (SecurityContextHolder.getContext().authentication?.isAuthenticated == true) {
                    (request as HttpServletRequest).cookies.find { it.name == cookieName }
                        ?: SecurityContextLogoutHandler().logout(
                            request,
                            response as HttpServletResponse,
                            SecurityContextHolder.getContext().authentication
                        )
                }
                chain.doFilter(request, response)
            }, BasicAuthenticationFilter::class.java)
    }
}
