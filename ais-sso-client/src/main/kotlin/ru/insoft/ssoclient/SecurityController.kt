package ru.insoft.ssoclient

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import ru.insoft.libs.models.Role
import ru.insoft.libs.models.User
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RequestMapping("/sso")
@RestController
class SecurityController(private val authManager: AuthenticationManager) {
    @Value("\${app.sso.user-info-url}")
    private lateinit var userInfoUrl: String

    @Value("\${app.root-url}")
    private lateinit var myurl: String

    @GetMapping("/login")
    fun login(@RequestParam next: String?, user: Authentication?, resp: HttpServletResponse, req: HttpServletRequest) {
        if (user?.isAuthenticated != true) {
            try {
                val headers = HttpHeaders()
                req.cookies.forEach {
                    headers.add("Cookie", "${it.name}=${it.value}")
                }
                RestTemplate().exchange(
                    userInfoUrl, HttpMethod.GET,
                    HttpEntity<Authentication>(headers), User::class.java
                ).body?.let {
                    SecurityContextHolder.getContext().authentication =
                        authManager.authenticate(
                            UsernamePasswordAuthenticationToken(
                                "${it.id}|${it.login}|${it.orgId}|${it.roleId}",
                                "xxx",
                                listOf(Role(it.roleId))
                            ).apply { details = it }
                        )
                }
            } catch (err: Exception) {
                err.printStackTrace()
            }
        }
        if (SecurityContextHolder.getContext().authentication?.isAuthenticated == true) {
            resp.sendRedirect("""$myurl${next ?: "/"}""")
        } else {
            resp.status = 403
            resp.writer.print("Доступ запрещен")
        }
    }

    @GetMapping("/user-info")
    fun userInfo(auth: Authentication) = auth.details
}
